# About

Hyrax Middleware: a middleware for mobile edge clouds.

Copyright (C) 2019 INESC TEC. 

This software is authored by João Filipe Rodrigues. Its development was financially supported by HYRAX project (Hyrax: Crowd-Sourcing Mobile Devices to Develop Edge Clouds - Ref: CMUP-ERI/FIA/0048/2013) and was part of João's PhD work at the Computer Science Department, Faculty of Sciences, University of Porto,  supervised by Luís Lopes and Fernando Silva, and with contributions by Eduardo Marques, Rolando Martins, and Joaquim Silva. 

If you use the Hyrax middleware in a work that leads to a scientific publication, we would appreciate if you would kindly cite the following document:

João Rodrigues, A Middleware for Mobile Edge-Cloud Applications, PhD Thesis, Computer Science Department, Faculty of Sciences, University of Porto, 2019

The document can be found at https://hdl.handle.net/10216/118307

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

This work makes use and is distributed with the following libraries:
- Jdeferred (http://jdeferred.org)
- Little Proxy (https://github.com/adamfisk/LittleProxy)
- Google Protocol Buffers (https://developers.google.com/protocol-buffers/)
- Google Protocol Buffers - Java Format (https://github.com/bivas/protobuf-java-format)

You can reach INESC TEC at info@inesctec.pt, or

Campus da Faculdade de Engenharia da Universidade do Porto
Rua Dr. Roberto Frias
4200-465 Porto
Portugal

A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.


# Hyrax Network Layer
The Hyrax network layer is an Android API that allows to build ad-hoc networks
using the Android Link Layer. Besides of that, it also is responsible for routing
the messages through the distinct existing network interface. At this level there
is a complete abstraction of wireless technologies by interconnecting the
different networks. This layer provides a minimal interface for developers
interact e.g. send/receive.

The project [HyraxNetworkLayerApp](git@bitbucket.org:hyrax_dcc/android-network-layer-library.git),
hosts an Android App used to test the network layer and also may serve as a developer
guide because it contains a few examples of use.

## How to install
The code in this project is an Android studio library, thus we assume that the
reader knows what it is. If not check [Android Library](https://developer.android.com/studio/projects/android-library.html).
As this project is a library it cannot be installed direct on the Android devices, thus
it must be first added to an Android project.

### Add as git submodule
The advantages of submodules is essentially the ability to add other projects
to your own project. These submodules projects are independent which means that
can be have it's own commits/updates. See [Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
for more information. Assuming that developer has already a git project, then inside the project
folder (root folder) do:
```git
git submodule add git@bitbucket.org:hyrax_dcc/android-network-layer-library.git
git status
cat .gitmodules
```
* The command on the first line means to clone a git project and adding it as
a current project submodule.
* In the second line the developer must see at least two changes on the project
**.gitmodules** and **android-network-layer-library**. Means that a submodule was
successfully added.
* The last command the developer can see the references to the submodule added.

*These commands are just needed for git older versions*. For newer versions the
above commands are enough.
```git
git submodule init
git submodule update
```

In case the developer does not have the current project under git, then clone
is just enough.
```git
git clone git@bitbucket.org:hyrax_dcc/android-network-layer-library.git
```

From now on, the developer should have the Android library added to the project. To
finalize the process two more things must be done, the library compile instruction
and add the library to gradle settings. Regarding the first process, on app folder gradle
(*build.gradle*) add in the dependencies section the compile instruction
*compile project(':android-network-layer-library')*. It should look like this:
```
dependencies {
    ...
    compile project(':android-network-layer-library')
}
```
Now the developer must open the *settings.gladle* and add ':android-network-layer-library'.
The file must look like:
```
include ':app', ':android-network-layer-library'
```
If the line is not added the project won't know that the library exists.
### Dependencies
Note this library depend of another which is the [Link Layer](https://bitbucket.org/hyrax_dcc/android-link-layer-library/).
This means that developers need to add the link layer library to the root folder.
The process is very similar as before, yet the reader may check to README file of
[Link Layer](https://bitbucket.org/hyrax_dcc/android-link-layer-library/),
for instructions. A slightly configuration modification is needed, as the network
layer depends of the link layer (not the root App), thus the network
layer must compile the link layer project. Then network layer(*build.gradle*), under
dependencies will look like:
```
dependencies {
    ...
    compile project(':android-link-layer-library')
}
```
The (*build.gradle*) of the root app project will stay exactly in the same way.
Once more do not forget to include the reference to the link layer module on the
root app file *settings.gladle*.
```
include ':app', ':android-network-layer-library', ':android-link-layer-library'
```

Notice that at the end *settings.gladle* have the reference to two android libraries
modules. From now on the network layer tools are at developers disposal, as well as
the link layer tools can now be used at the network level. **So Have Fun**

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.NonNull;

import java.util.UUID;

/**
 * This class represents a logic address that is understood by the network layer, thus can be used
 * by any wireless interface.
 */
public final class Address implements Comparable<Address> {
    @NonNull
    private UUID address;

    /**
     * Constructor.
     *
     * @param address universal identifier.
     */
    private Address(@NonNull UUID address) {
        this.address = address;
    }

    /**
     * Instantiates and address object from an UUID object
     *
     * @param uuid universal identifier
     * @return address class object
     */
    @NonNull
    public static Address AddressFromUUID(@NonNull UUID uuid) {
        return new Address(uuid);
    }

    /**
     * Returns an universal identifier with a logic address.
     *
     * @return universal identifier
     */
    @NonNull
    public UUID getLogicAddress() {
        return address;
    }

    @Override
    public int compareTo(@NonNull Address o) {
        return address.compareTo(o.address);
    }

    @Override
    public boolean equals(Object o) {
        return (o != null) && (o.getClass() == Address.class)
                && this.address.equals(((Address) o).address);
    }

    @Override
    public int hashCode() {
        return address.hashCode();
    }

    @Override
    public String toString() {
        return address.toString();
    }
}

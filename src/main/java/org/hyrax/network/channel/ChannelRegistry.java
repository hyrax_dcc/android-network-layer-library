/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.channel;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.network.Address;
import org.hyrax.network.NetworkService;
import org.hyrax.network.Routing;
import org.hyrax.network.misc.Channel;
import org.hyrax.network.misc.ChannelServer;
import org.hyrax.network.misc.Error;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRegistry;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.hyrax.network.routing.LibRouting;
import org.hyrax.network.routing.packet.Packet;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class responsible for channels management.
 */
public final class ChannelRegistry {
    private static final Lock LOCK = new ReentrantLock();
    private static final String TAG = "ChannelReg";
    @Nullable
    private static ChannelRegistry instance;
    private final NetworkRegistry networkRegistry;
    private final Routing routing;
    private final ChannelMap channelMap;
    private Packet.Header logicAddressPacket;

    /**
     * Singleton Constructor.
     *
     * @param networkRegistry network registry object
     * @param routing         routing algorithm
     */
    private ChannelRegistry(@NonNull NetworkRegistry networkRegistry, @NonNull Routing routing) {
        this.networkRegistry = networkRegistry;
        this.routing = routing;
        this.channelMap = new ChannelMap();
    }

    /**
     * Initializes the channel registry.
     *
     * @param networkRegistry network registry object
     * @param routing         routing algorithm
     * @return a channel registry object
     */
    public static ChannelRegistry boot(@NonNull NetworkRegistry networkRegistry, @NonNull Routing routing) {
        if (instance == null) {
            instance = new ChannelRegistry(networkRegistry, routing);
            return instance;
        }
        throw new NetworkRuntimeException("Channel Registry already booted");
    }

    /**
     * Closes and cleans the channel registry.
     */
    public void shutdown() {
        LOCK.lock();
        try {
            if (instance != null) {
                instance = null;
            }
            //channelMap.clean();
        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Starts a new channel.
     *
     * @param channel channel object
     * @param defer   a defer object to notify when the channel has started
     */
    private void startNewChannel(@NonNull final Channel channel,
                                 @NonNull DeferredObject<Channel, Exception, Void> defer) {
        if (logicAddressPacket == null)
            // local logic address messages
            logicAddressPacket = Packet.Header.newBuilder()
                    .setTag(LibRouting.LOGIC_ADDRESS)
                    .setBodyLength(0)
                    .setSource(LibRouting.addressToProtoAddress(NetworkService.getLocalAddress()))
                    .build();

        channel.setStatusListener(new Channel.StatusListener() {
            @Override
            public void onStart() {
                LOCK.lock();
                try {
                    if (instance != null) {
                        defer.resolve(channel);
                        // listen for packets
                        listenPackets(channel);
                        // sends the logic address
                        channel.writePacket(instance.logicAddressPacket, new byte[]{});
                    }
                } finally {
                    LOCK.unlock();
                }
            }

            @Override
            public void onStop(@NonNull Error error) {
                if (!defer.isResolved()) {
                    defer.reject(new Exception((error.hasError()) ?
                            error.getError() : "No defined error"));
                }
                // remove channel from cache
                removeChannel(channel);
            }
        });
        channel.start();
    }

    /**
     * Removes a channel from cache and notifies.
     *
     * @param channel a channel object
     */
    private void removeChannel(@NonNull Channel channel) {
        LOCK.lock();
        try {
            if (instance != null) {
                Address address = channelMap.removeChannel(channel);
                // if channel exists - then notifies
                if (address != null) {
                    // notify the routing a left peer
                    instance.routing.onPeerLeave(address, channel);
                    // notify a new peer left
                    instance.networkRegistry.notifyPeerLeave(address);
                }
            }
        } finally {
            LOCK.unlock();
        }
    }

    private void listenPackets(@NonNull Channel channel) {
        channel.setPacketListener((header, bytes) -> {
            LOCK.lock();
            try {
                if (instance != null) {
                    switch (header.getTag()) {
                        case LibRouting.LOGIC_ADDRESS:
                            Address remote = LibRouting.protoAddressToAddress(header.getSource());
                            // if channel successfully added - then notifies - otherwise stops it
                            if (channelMap.addNewChannel(channel, remote)) {
                                // notify the routing a new peer
                                instance.routing.onPeerJoin(remote, channel);
                                // notify a new peer join
                                instance.networkRegistry.notifyPeerJoin(remote);
                            } else {
                                channel.stop();
                            }
                            break;
                        default:
                            Address address = instance.channelMap.channelToAddress.get(channel);
                            if (address == null) return;
                            instance.routing.onPeerPacket(address, channel, header, bytes);
                    }
                }
            } finally {
                LOCK.unlock();
            }
        });
    }

    /**
     * Adds and starts a new server channel.
     *
     * @param channelServer channel server object
     * @param defer         a defer object to notify when the channel has started
     */
    private void startNewServerChannel(@NonNull ChannelServer channelServer,
                                       @NonNull DeferredObject<ChannelServer, Exception, Void> defer) {
        LOCK.lock();
        try {
            if (instance != null && instance.channelMap.channelServers.add(channelServer)) {
                channelServer.setStatusListener(new ChannelServer.StatusListener() {
                    @Override
                    public void onStart() {
                        defer.resolve(channelServer);
                    }

                    @Override
                    public void onStop(@NonNull Error error) {
                        if (!defer.isResolved())
                            if (!defer.isResolved()) {
                                defer.reject(new Exception((error.hasError()) ?
                                        error.getError() : "No defined error"));
                            }
                        //remove server channel from cache
                        removeServerChannel(channelServer);
                    }

                    @Override
                    public void onNewClient(@NonNull Channel channel) {
                        // starts the channel that comes from the client
                        startNewChannel(channel, new DeferredObject<>());
                    }
                });
                channelServer.start();
            }
        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Removes a server channel from cache.
     *
     * @param channelServer channel server object
     */
    private void removeServerChannel(@NonNull ChannelServer channelServer) {
        LOCK.lock();
        try {
            if (instance != null && !instance.channelMap.channelServers.remove(channelServer)) {
                NetLog.w(TAG, "Channel Server remove fail!");
            }
        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Verifies if the current device has a channel connection to a remote physical address.
     *
     * @param phyAddress physical address to verify e.g. ip address, bluetooth mac address
     * @return <tt>true</tt> if connected, <tt>false</tt> otherwise
     */
    public boolean isConnected(@NonNull String phyAddress) {
        LOCK.lock();
        try {
            return (instance != null) && instance.channelMap.phyAddressesSet.contains(phyAddress);
        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Create a server socket if none exists. Otherwise it return an existing one.
     *
     * @param port       integer port to listen, or 0 to listen on an any available port
     * @param technology the technology used
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    public Promise<ChannelServer, Exception, Void>
    createSocketChannelServerIfNotExists(int port, @NonNull Technology technology) {
        LOCK.lock();
        try {
            for (ChannelServer cs : channelMap.channelServers) {
                if (cs instanceof SocketChannelServer)
                    return new DeferredObject<ChannelServer, Exception, Void>().resolve(cs);
            }
            return createSocketChannelServer(port, technology);

        } finally {
            LOCK.unlock();
        }
    }

    /**
     * Creates a server socket.
     *
     * @param port       integer port to listen, or 0 to listen on an any available port
     * @param technology the technology used
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    private Promise<ChannelServer, Exception, Void>
    createSocketChannelServer(int port, @NonNull Technology technology) {
        final DeferredObject<ChannelServer, Exception, Void> defer = new DeferredObject<>();
        if (instance != null) {
            SocketChannelServer channelServer = new SocketChannelServer(port, technology);
            instance.startNewServerChannel(channelServer, defer);
        } else {
            defer.reject(new Exception("Channel Server Wifi instance null"));
        }
        return defer.promise();
    }

    /**
     * Creates a socket channel object.
     *
     * @param ipAddress  remote device ip address
     * @param port       remote device listen port
     * @param technology the technology used
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    public Promise<Channel, Exception, Void>
    createSocketChannel(@NonNull String ipAddress, int port,
                        @NonNull Technology technology) {
        final DeferredObject<Channel, Exception, Void> defer = new DeferredObject<>();
        if (instance != null) {
            final SocketChannel socketChannel = new SocketChannel(ipAddress, port, technology);
            instance.startNewChannel(socketChannel, defer);
        } else {
            defer.reject(new Exception("Channel Wifi instance null"));
        }
        return defer.promise();
    }

    /**
     * Creates a bluetooth channel object.
     *
     * @param device        a bluetooth device object
     * @param bluetoothComm a bluetooth communication object
     * @return a <a href="http://jdeferred.org/" target="_blank">Promise</a> object
     */
    public Promise<Channel, Exception, Void>
    createBluetoothChannel(@NonNull Device device,
                           @NonNull BluetoothComm bluetoothComm) {

        final DeferredObject<Channel, Exception, Void> defer = new DeferredObject<>();
        if (instance != null) {
            final BluetoothChannel bthChannel = new BluetoothChannel(device, bluetoothComm);
            instance.startNewChannel(bthChannel, defer);
        } else {
            defer.reject(new Exception("Channel Bluetooth instance null"));
        }
        return defer.promise();
    }

    /**
     * A channel map class.
     */
    private class ChannelMap {
        private final Set<ChannelServer> channelServers;
        private final Set<String> phyAddressesSet;
        private final Map<Channel, Address> channelToAddress;

        /**
         * Constructor.
         */
        private ChannelMap() {
            this.channelServers = new HashSet<>();
            this.phyAddressesSet = new HashSet<>();
            this.channelToAddress = new HashMap<>();
        }

        /**
         * Adds a new channel a logic address to the map.
         *
         * @param channel channel object to add
         * @param address channel logic address
         * @return <tt>true</tt> if the map successfully added, <tt>false</tt> otherwise
         */
        private boolean addNewChannel(@NonNull Channel channel, @NonNull Address address) {
            if (!channelToAddress.containsKey(channel) && phyAddressesSet.add(channel.getAddress())) {
                channelToAddress.put(channel, address);
                return true;
            }
            return false;
        }

        /**
         * Removes a channel from the map.
         *
         * @param channel channel object to be remove
         * @return address associated to the channel or NULL if no map found
         */
        @Nullable
        private Address removeChannel(@NonNull Channel channel) {
            phyAddressesSet.remove(channel.getAddress());
            return channelToAddress.remove(channel);
        }

        /**
         * Stops all channels and cleans the hash_maps
         */
        private void clean() {
            for (ChannelServer s : channelServers)
                s.stop();
            channelServers.clear();
            for (Channel c : channelToAddress.keySet())
                c.stop();
            phyAddressesSet.clear();
            channelToAddress.clear();
        }
    }
}

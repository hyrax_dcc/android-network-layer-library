/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.channel;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Technology;
import org.hyrax.network.misc.Channel;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.Error;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.hyrax.network.routing.packet.Packet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Client socket implementation.
 */
class SocketChannel implements Channel, Runnable {
    private static final int TRIES = 10; //5 times
    private static final int TIMEOUT = 500; //half second

    private final String ipAddress;
    private final int port;
    private final AtomicBoolean running;
    private final Technology technology;

    @Nullable
    private Socket socket;
    @Nullable
    private InputStream inputStream;
    @Nullable
    private OutputStream outputStream;
    @NonNull
    private StatusListener statusListener;
    @NonNull
    private PacketListener packetListener;

    /**
     * Constructor.
     *
     * @param ipAddress target ipAddress
     * @param port      target port
     */
    SocketChannel(@NonNull String ipAddress, int port, @NonNull Technology technology) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.running = new AtomicBoolean(false);
        this.technology = technology;

        this.statusListener = StatusListener.NO_LISTENER;
        this.packetListener = PacketListener.NO_LISTENER;
    }

    /**
     * Constructor.
     *
     * @param socket remote socket
     */
    SocketChannel(@NonNull Socket socket, @NonNull Technology technology) {
        this(socket.getInetAddress().getHostAddress(), 0, technology);
        this.socket = socket;
    }

    /**
     * Attempts to connect to a remote server
     *
     * @throws IOException throws an exception if something expected happens
     */
    private void connect() throws IOException {
        int t = 0;
        while (socket == null && t++ < TRIES) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(ipAddress, port), TIMEOUT);

            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                socket = null;
                if (t == TRIES)
                    throw e;

            } catch (IOException e) {
                e.printStackTrace();
                try {
                    Thread.sleep(TIMEOUT);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                socket = null;
                if (t == TRIES)
                    throw e;
            }
        }
    }

    @Override
    public void run() {
        if (running.compareAndSet(false, true)) {
            Error error = Error.NO_ERROR;
            try {
                // attempts to connect
                connect();
                if (socket == null)
                    throw new IOException("Socket is Null");
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();

                // notify start
                statusListener.onStart();

                Packet.Header header;
                byte[] buffer = new byte[Constants.MAX_BUFFER_SIZE];
                while (running.get() && socket.isConnected()) {
                    if (inputStream == null)
                        throw new IOException("InputStream is Null");

                    // unpack header
                    header = Packet.Header.parseDelimitedFrom(inputStream);
                    if (header != null) {
                        int remain = header.getBodyLength();
                        int read = 0;
                        // reads the message body
                        while (remain > 0 && (read = inputStream.read(buffer, read, remain)) != -1)
                            remain -= read;

                        if (remain == 0) {
                            packetListener.onPacket(header, buffer);
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
                if (running.get())
                    error = new Error(e.getMessage());

            } finally {
                try {
                    if (inputStream != null) inputStream.close();
                    if (outputStream != null) outputStream.close();
                    if (socket != null) socket.close();
                } catch (IOException | NullPointerException e) {
                    e.printStackTrace();
                }
                running.set(false);
                //notify stop
                statusListener.onStop(error);
            }
            return;
        }
        throw new NetworkRuntimeException("Wifi Channel already started");
    }

    @Override
    public void start() {
        new Thread(this).start();
    }

    @Override
    public void stop() {
        if (running.compareAndSet(true, false)) {
            if (socket != null) {
                try {
                    statusListener.onStop(Error.NO_ERROR);
                    //reset listeners
                    statusListener = StatusListener.NO_LISTENER;
                    packetListener = PacketListener.NO_LISTENER;
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean isRunning() {
        return running.get();
    }

    @NonNull
    @Override
    public String getAddress() {
        return ipAddress;
    }

    @NonNull
    @Override
    public Technology getTechnology() {
        return technology;
    }

    @Override
    public synchronized boolean writePacket(@NonNull Packet.Header header, @NonNull byte[] body) {
        if (outputStream != null) {
            try {
                // writes packet header
                header.writeDelimitedTo(outputStream);
                // write packet body
                outputStream.write(body, 0, header.getBodyLength());
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void setStatusListener(@NonNull StatusListener statusListener) {
        this.statusListener = statusListener;
    }

    @Override
    public void setPacketListener(@NonNull PacketListener packetListener) {
        this.packetListener = packetListener;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof SocketChannel) && channelEquals((SocketChannel) o);
    }

    @Override
    public int hashCode() {
        return channelHash();
    }
}

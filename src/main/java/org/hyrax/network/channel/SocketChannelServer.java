/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.channel;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Technology;
import org.hyrax.network.misc.ChannelServer;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.Error;
import org.hyrax.network.misc.NetworkRuntimeException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Wifi server socket implementation.
 */
class SocketChannelServer implements ChannelServer, Runnable {
    private final AtomicBoolean running;
    private final int userDefinedPort;
    private final Technology technology;
    private StatusListener statusListener;
    @Nullable
    private ServerSocket serverSocket;

    /**
     * Constructor.
     *
     * @param port port to listen to. Zero means that the system will choose an available port randomly.
     */
    SocketChannelServer(int port, @NonNull Technology technology) {
        this.running = new AtomicBoolean(false);
        this.technology = technology;
        this.userDefinedPort = port;
        this.statusListener = StatusListener.NO_LISTENER;
    }


    @Override
    public void start() {
        new Thread(this).start();
    }

    @Override
    public void stop() {
        if (running.compareAndSet(true, false)) {
            try {
                statusListener.onStop(Error.NO_ERROR);
                statusListener = StatusListener.NO_LISTENER;

                if (serverSocket != null)
                    serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean isRunning() {
        return running.get();
    }

    @Override
    public int getPort() {
        if (serverSocket == null)
            return 0;
        int tries = 10;
        while (tries-- > 0 && serverSocket.getLocalPort() <= 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return serverSocket.getLocalPort();
    }

    @Override
    public String getAddress() {
        return (serverSocket != null) ?
                serverSocket.getInetAddress().getHostAddress() :
                Constants.UNDEFINED;
    }

    @Override
    public void setStatusListener(@NonNull StatusListener statusListener) {
        this.statusListener = statusListener;
    }

    @Override
    public void run() {
        if (running.compareAndSet(false, true)) {
            Error error = Error.NO_ERROR;
            try {
                serverSocket = new ServerSocket(userDefinedPort);
                //notify start
                statusListener.onStart();

                while (running.get()) {
                    Socket socket = serverSocket.accept();
                    SocketChannel clientChannel = new SocketChannel(socket, technology);
                    //notify new client
                    statusListener.onNewClient(clientChannel);
                }

            } catch (IOException e) {
                e.printStackTrace();
                if (running.get())
                    error = new Error(e.getMessage());

            } finally {
                running.set(false);
                //notify stop
                statusListener.onStop(error);
            }
            return;
        }
        throw new NetworkRuntimeException("Wifi Server Channel already started");
    }
}

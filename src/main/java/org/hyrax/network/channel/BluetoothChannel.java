/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.channel;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.Technology;
import org.hyrax.link.bluetooth.legacy.BluetoothNode;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.network.misc.Channel;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.Error;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.hyrax.network.routing.packet.Packet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Bluetooth client socket wrapper implementation.
 */
class BluetoothChannel implements Channel {
    private final Device device;
    private final BluetoothComm bluetoothComm;
    private final AtomicBoolean running;

    private StatusListener statusListener;
    private PacketListener packetListener;

    /**
     * Constructor
     *
     * @param device        a bluetooth device object
     * @param bluetoothComm a bluetooth communication socket
     */
    BluetoothChannel(@NonNull Device device, @NonNull BluetoothComm bluetoothComm) {
        NetLog.assertInstance(device, BluetoothNode.class, "Device type must be BluetoothNode");
        this.device = device;
        this.bluetoothComm = bluetoothComm;
        this.running = new AtomicBoolean(false);
        this.statusListener = StatusListener.NO_LISTENER;
        this.packetListener = PacketListener.NO_LISTENER;
    }

    @Override
    public void start() {
        if (running.compareAndSet(false, true)) {
            bluetoothComm.setCommListener(new BluetoothComm.CommListener() {
                @Override
                public void onInputStreamReady(@NonNull InputStream inputStream) throws IOException {
                    //notify start
                    statusListener.onStart();

                    Packet.Header header;
                    byte[] buffer = new byte[Constants.MAX_BUFFER_SIZE];
                    //loop through input stream to get messages
                    while (bluetoothComm.isRunning()) {
                        // unpack header
                        header = Packet.Header.parseDelimitedFrom(inputStream);
                        int remain = header.getBodyLength();
                        int read = 0;
                        // reads the message body
                        while (remain > 0 && (read = inputStream.read(buffer, read, remain)) != -1)
                            remain -= read;

                        if (remain == 0)
                            packetListener.onPacket(header, buffer);
                    }
                }

                @Override
                public void onStop() {
                    //notify stop
                    statusListener.onStop(Error.NO_ERROR);
                }
            });
            return;
        }
        throw new NetworkRuntimeException("Bluetooth Channel already started");
    }

    @Override
    public void stop() {
        if (running.compareAndSet(true, false)) {
            statusListener.onStop(Error.NO_ERROR);
            //reset listeners
            statusListener = StatusListener.NO_LISTENER;
            packetListener = PacketListener.NO_LISTENER;

            bluetoothComm.forceStop();
        }
    }

    @Override
    public boolean isRunning() {
        return running.get();
    }

    @NonNull
    @Override
    public String getAddress() {
        return device.getUniqueAddress();
    }

    @NonNull
    @Override
    public Technology getTechnology() {
        return Technology.BLUETOOTH;
    }

    @Override
    public synchronized boolean writePacket(@NonNull Packet.Header header, @NonNull byte[] body) {
        OutputStream out = bluetoothComm.getOutputStream();
        if (out != null) {
            try {
                // writes packet header
                header.writeDelimitedTo(out);
                // write packet body
                out.write(body, 0, header.getBodyLength());
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void setStatusListener(@NonNull StatusListener statusListener) {
        this.statusListener = statusListener;
    }

    @Override
    public void setPacketListener(@NonNull PacketListener packetListener) {
        this.packetListener = packetListener;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof BluetoothChannel) && channelEquals((BluetoothChannel) o);
    }

    @Override
    public int hashCode() {
        return channelHash();
    }
}

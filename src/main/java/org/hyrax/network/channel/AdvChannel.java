/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.channel;

import android.support.annotation.NonNull;

import org.hyrax.network.misc.NetLog;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * UDP advertiser class.
 */
public class AdvChannel {
    private static final int UDP_MAX_SIZE = 1024;
    private static final String TAG = "AdvChannel";
    private final int listenPort;
    private final AtomicBoolean running;
    private final Receiver receiver;

    private final List<PacketListener> listeners;

    /**
     * Upd packet listener.
     */
    public interface PacketListener {
        /**
         * Triggered when a packet has been received.
         *
         * @param srcIpAddress source ip address
         * @param jsonMessage  string json message object
         */
        void onPacket(@NonNull String srcIpAddress, @NonNull String jsonMessage);
    }

    /**
     * Constructor.
     *
     * @param listenPort port to listen
     */
    public AdvChannel(int listenPort) {
        this.listenPort = listenPort;
        this.running = new AtomicBoolean(false);
        this.receiver = new Receiver();
        this.listeners = new ArrayList<>();
    }

    /**
     * Starts the advertiser server.
     */
    public void start() {
        if (running.compareAndSet(false, true)) {
            new Thread(receiver).start();
        }

    }

    /**
     * Stops the advertiser.
     */
    public void stop() {
        if (running.compareAndSet(true, false)) {
            receiver.serverSocket.close();
            listeners.clear();
        }
    }

    /**
     * Adds a packet listener.
     *
     * @param packetListener listener object
     */
    public void addPacketListener(@NonNull PacketListener packetListener) {
        synchronized (listeners) {
            listeners.add(packetListener);
        }
    }

    /**
     * Removes a packet listener.
     *
     * @param packetListener packet listener object
     */
    public void removePacketListener(@NonNull PacketListener packetListener) {
        synchronized (listeners) {
            listeners.remove(packetListener);
        }
    }

    /**
     * Send a message to a set of addresses broadcast.
     *
     * @param jsonMessage json message
     * @param addresses   designation addresses or broadcast addresses
     */
    public synchronized void sendMessage(@NonNull String jsonMessage, @NonNull String... addresses) {
        try {
            byte[] bytes = jsonMessage.getBytes();

            if (bytes.length >= UDP_MAX_SIZE) {
                throw new Exception(String.format(Locale.ENGLISH,
                        "Overflow packet size. Limit: %d || Current size: %d",
                        UDP_MAX_SIZE, bytes.length));
            }

            if (addresses.length == 0)
                return;

            DatagramSocket clientSocket = new DatagramSocket();
            clientSocket.setBroadcast(true);

            for (String adr : addresses) {
                DatagramPacket packet = new DatagramPacket(bytes, bytes.length,
                        InetAddress.getByName(adr), listenPort);

                clientSocket.send(packet);
            }
            clientSocket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Listens for incoming messages.
     */
    class Receiver implements Runnable {
        private DatagramSocket serverSocket;

        Receiver() {

        }

        @Override
        public void run() {
            try {
                serverSocket = new DatagramSocket(listenPort);
                byte[] data = new byte[UDP_MAX_SIZE];
                while (running.get()) {
                    try {
                        DatagramPacket receivePacket = new DatagramPacket(data, data.length);
                        serverSocket.receive(receivePacket);
                        byte[] bytes = Arrays.copyOf(receivePacket.getData(), receivePacket.getLength());

                        String hostAddress = receivePacket.getAddress().getHostAddress();
                        String jsonMessage = new String(bytes);

                        synchronized (listeners) {
                            for (PacketListener listener : listeners)
                                listener.onPacket(hostAddress, jsonMessage);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        NetLog.e(TAG, e.getMessage());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                NetLog.e("Adv", "Is Closed");
            }
        }
    }

    /**
     * Returns a collection of broadcast addresses. Wifi , Wifi Direct
     *
     * @return a collection of string addresses
     */
    @NonNull
    public static List<String> getBroadcastAddresses() {
        System.setProperty("java.net.preferIPv4Stack", "true");
        List<String> addresses = new ArrayList<>();
        try {
            for (Enumeration<NetworkInterface> niEnum = NetworkInterface.getNetworkInterfaces(); niEnum.hasMoreElements(); ) {
                NetworkInterface ni = niEnum.nextElement();
                if (!ni.isLoopback()) {
                    for (InterfaceAddress interfaceAddress : ni.getInterfaceAddresses()) {
                        if (interfaceAddress.getBroadcast() != null) {
                            addresses.add(interfaceAddress.getBroadcast().toString().substring(1));
                        }
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return addresses;
    }
}

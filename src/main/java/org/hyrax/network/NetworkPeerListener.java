/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.NonNull;

import org.hyrax.network.misc.NetworkListener;

/**
 * Interface that listens for general network events.
 */
public interface NetworkPeerListener extends NetworkListener {

    /**
     * Method triggered when a peers has joined the network.
     *
     * @param address remote peer logic address
     */
    void onPeerJoin(@NonNull Address address);

    /**
     * Method triggered when a peers has left the network.
     *
     * @param address remote peer logic address
     */
    void onPeerLeave(@NonNull Address address);
}

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.IntRange;

import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.NetLog;

/**
 * This class is responsible for tuning the behaviour of a sendToAll message.
 * <br/>
 * This object will be used internally by the network.
 */

public class RoutingProperties {
    private final int maxNumberOfHops;
    private final int tag;

    /**
     * Constructor.
     *
     * @param builder a {@link RoutingProperties} builder object
     */
    private RoutingProperties(Builder builder) {
        this.maxNumberOfHops = builder.maxNumberOfHops;
        this.tag = builder.tag;
    }

    /**
     * Returns a maximum number of network hops allowed.
     *
     * @return integer with the number of hops, or -1 meaning unlimited hops.
     */
    public int getMaxNumberOfHops() {
        return maxNumberOfHops;
    }

    /**
     * Returns the routing tag.
     *
     * @return Integer routing tag
     */
    @IntRange(from = Constants.ROUTING_TAG_MIN)
    public int getTag() {
        return tag;
    }

    /**
     * Instantiates a builder object in order to defined the intended parameters.
     *
     * @return a {@link Builder} object
     */
    public static Builder newBuilder(@IntRange(from = Constants.ROUTING_TAG_MIN) int tag) {
        return new Builder(tag);
    }

    /**
     * Class responsible for building and defining the {@link RoutingProperties} parameters
     */
    public static final class Builder {
        private int maxNumberOfHops;
        private int tag;

        /**
         * Constructor.
         */
        private Builder(@IntRange(from = Constants.ROUTING_TAG_MIN) int tag) {
            this.maxNumberOfHops = Constants.DEFAULT_ROUTING_HOPS;
            this.tag = tag;
        }

        /**
         * Changes the maximum numbers of hops allowed.
         *
         * @param nHops a new_def maximum hops number, -1 to unlimited hops
         * @return the builder object
         */
        public Builder setMaxNumberOfHops(int nHops) {
            this.maxNumberOfHops = nHops;
            return this;
        }

        /**
         * Changes the routing tag.
         *
         * @param tag routing tag
         * @return the builder object
         */
        public Builder setTag(@IntRange(from = Constants.ROUTING_TAG_MIN) int tag) {
            this.tag = tag;
            return this;
        }

        /**
         * Returns a {@link RoutingProperties} object.
         *
         * @return {@link RoutingProperties} object
         */
        public RoutingProperties build() {
            NetLog.assertEquals(false, tag < Constants.ROUTING_TAG_MIN,
                    "Invalid tag :" + tag + " Only [130,inf] supported");

            return new RoutingProperties(this);
        }
    }
}

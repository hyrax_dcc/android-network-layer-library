/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.NetworkListener;

/**
 * Interface that listens for incoming network packets.
 */
public interface NetworkPacketListener extends NetworkListener {

    /**
     * Method triggered when a message from a specific address has arrived.
     *
     * @param bytes      body message in bytes
     * @param len        body message size
     * @param srcAddress device source address
     * @param tag        type of messages
     */
    void onNewPacket(@NonNull byte[] bytes, int len, @NonNull Address srcAddress,
                     @IntRange(from = Constants.ROUTING_ALL_TAG) int tag);
}

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.LinkService;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.hyrax.network.misc.PropertiesManager;
import org.hyrax.network.routing.LibRouting;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class is a gateway between the network layer and the users.
 * <br/>
 * It initializes all the object necessary to the network properly works.
 * <p/>
 */
public final class NetworkService {
    private static NetworkService instance;
    private static final AtomicBoolean started = new AtomicBoolean(false);
    private final Address localAddress;
    private final Address floodAddress;
    private final Network network;


    /**
     * Constructor.
     *
     * @param context context android application context
     * @param routing routing algorithm
     */
    private NetworkService(@NonNull Context context, @NonNull Routing routing) {
        // boots the link layer
        LinkService.boot(context);
        // loads the addresses
        localAddress = loadLocalAddress(context.getApplicationInfo().dataDir);
        floodAddress = loadFloodAddress();
        // initializes the network class
        network = new Network(routing);
    }

    /**
     * Boots and initializes the network.
     *
     * @param context context android application context
     * @param type    routing type
     * @return {@link Network} instance
     * @throws NetworkRuntimeException if network service has been already started
     */
    public static Network boot(@NonNull Context context, @NonNull Routing.Type type) {
        return boot(context, LibRouting.factory(type));
    }

    /**
     * Boots and initializes the network.
     *
     * @param context context android application context
     * @param routing the routing algorithm class
     * @return {@link Network} instance
     * @throws NetworkRuntimeException if network service has been already started
     */
    public static Network boot(@NonNull Context context, @NonNull Routing routing) {
        if (started.compareAndSet(false, true)) {
            instance = new NetworkService(context, routing);
            //boots the network
            instance.network.start();
            return instance.network;
        }
        throw new NetworkRuntimeException("Network Service already started");
    }

    /**
     * Initializes the local address in save it in configuration files.
     * <p/>
     * The first time the network is initialized there is no address assigned, thus it create
     * a new address
     *
     * @param appDir the application base directory
     * @return an Address object
     */
    @NonNull
    private Address loadLocalAddress(@NonNull String appDir) {
        PropertiesManager.loadPropertiesPath(appDir + File.separator + Constants.CONFIG_FILENAME);
        String localAddress = PropertiesManager.getProperty(PropertiesManager.LOCAL_ADDRESS_KEY);
        // if address not exists - just creates a new one
        if (localAddress == null)
            PropertiesManager.setProperty(PropertiesManager.LOCAL_ADDRESS_KEY,
                    UUID.randomUUID().toString());
        // validation and instantiation
        localAddress = PropertiesManager.getProperty(PropertiesManager.LOCAL_ADDRESS_KEY);
        return Address.AddressFromUUID(UUID.fromString(localAddress));
    }

    /**
     * Initializes the flood address. Note that the flood address is statically defined.
     *
     * @return an Address object
     */
    @NonNull
    private Address loadFloodAddress() {
        return Address.AddressFromUUID(
                UUID.fromString(Constants.FLOOD_ADDRESS_STRING)
        );
    }

    /**
     * Returns the network local address.
     *
     * @return local {@link Address} object
     * @throws NetworkRuntimeException if network service has not been already started
     */
    public static Address getLocalAddress() {
        if (started.get()) {
            return instance.localAddress;
        }
        throw new NetworkRuntimeException("Network service not started yet");
    }

    /**
     * Returns the network flooding address.
     *
     * @return flooding {@link Address} object
     * @throws NetworkRuntimeException if network service has not been already started
     */
    public static Address getFloodAddress() {
        if (started.get()) {
            return instance.floodAddress;
        }
        throw new NetworkRuntimeException("Network service not started yet");
    }

    /**
     * Shutdowns the networks and intents to free all allocated objects.
     */
    public static void shutdown() {
        if (started.compareAndSet(true, false)) {
            //Stops network
            Log.e("Stop", "Here");
            instance.network.stop();
            //Stops the link
            Log.e("Stop", "Here2");
            LinkService.shutdown();
            Log.e("Stop", "Here3");
        }
    }

    /**
     * Verifies if the network service is booted or not.
     *
     * @return <tt>true</tt> if booted, <tt>false</tt> otherwise
     */
    public static boolean isRunning() {
        return started.get();
    }
}

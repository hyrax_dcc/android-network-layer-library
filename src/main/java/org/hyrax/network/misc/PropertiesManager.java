/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * It manages and save some network properties in a file, because the network layer needs some
 * persistent properties.
 * <p/>
 */
public final class PropertiesManager {
    //key for local address
    public static final String LOCAL_ADDRESS_KEY = "local_address";

    private static String configFilePath;

    /**
     * Loads the configuration properties file path.
     *
     * @param propertiesPath properties file absolute path
     */
    public static void loadPropertiesPath(@NonNull String propertiesPath) {
        configFilePath = propertiesPath;
    }

    /**
     * Changes or add a property to configuration properties file.
     *
     * @param key   property key
     * @param value property value
     */
    public static void setProperty(String key, String value) {
        Properties prop = new Properties();
        OutputStream out = null;

        try {
            out = new FileOutputStream(configFilePath);

            prop.setProperty(key, value);
            prop.store(out, null);

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Returns a value of a specific property or <i>null</i> if not available.
     *
     * @param key property key
     * @return property value or <i>null</i>
     */
    @Nullable
    public static String getProperty(String key) {
        Properties prop = new Properties();
        InputStream in = null;
        try {
            in = new FileInputStream(configFilePath);
            prop.load(in);

            return prop.getProperty(key);

        } catch (IOException e) {
            e.printStackTrace();
            return null;

        } finally {
            if (in != null)
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}

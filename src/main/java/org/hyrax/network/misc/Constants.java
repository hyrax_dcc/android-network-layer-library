/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

/**
 * Default network constants.
 * Created on 02-05-2017.
 */
public final class Constants {
    /**
     * Undefined string.
     */
    public static final String UNDEFINED = "UNDEFINED";

    /**
     * Maximum buffer size.
     */
    public static final int MAX_BUFFER_SIZE = 1024 * 8; // 8K

    /**
     * Configuration file name.
     */
    public static final String CONFIG_FILENAME = "config.properties";

    /**
     * String the represents the network broadcast address.
     */
    public static final String FLOOD_ADDRESS_STRING = "4177a34f-8e08-42eb-b18b-552375d830e8";

    /**
     * Default number of minimum connections per device
     */
    public static final int DEFAULT_MIN_CONNECTIONS_DEVICE = 3;

    /**
     * Default number of maximum connections per device
     */
    public static final int DEFAULT_MAX_CONNECTIONS_DEVICE = 5;

    /**
     * Time that a none can be idle, doing nothing
     */
    public static final int DEFAULT_IDLE_TIME = 60000;

    /**
     * Default number of routing hops.
     */
    public static final int DEFAULT_ROUTING_HOPS = 5;

    /**
     * Default routing tag. This tags means to listen all messages.
     */
    public static final int ROUTING_ALL_TAG = 129;

    /**
     * Minimum routing tag allowed to the user.
     */
    public static final int ROUTING_TAG_MIN = 130;
}

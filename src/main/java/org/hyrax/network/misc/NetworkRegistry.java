/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import org.hyrax.network.Address;
import org.hyrax.network.NetworkPacketListener;
import org.hyrax.network.NetworkPeerListener;
import org.hyrax.network.NetworkService;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class holds information and objects useful that need to be shared among, mainly, networks
 * interfaces instances.
 */
//TODO - complete later ans make it thread safe
public class NetworkRegistry {
    private static final String TAG = "NetRegistry";
    private final CopyOnWriteArrayList<NetworkPeerListener> networkPeerListeners;
    private final CopyOnWriteArrayList<PacketList> networkPacketListeners;

    /**
     * Class that maps a tag to a packet listener.
     */
    private class PacketList {
        private final int tag;
        private final NetworkPacketListener packetListener;

        PacketList(int tag, NetworkPacketListener packetListener) {
            this.tag = tag;
            this.packetListener = packetListener;
        }

        @Override
        public int hashCode() {
            return packetListener.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof PacketList && (((PacketList) o)).packetListener.equals(packetListener);
        }
    }

    /**
     * Constructor.
     */
    public NetworkRegistry() {
        this.networkPeerListeners = new CopyOnWriteArrayList<>();
        this.networkPacketListeners = new CopyOnWriteArrayList<>();
    }

    /**
     * Adds a network listener.
     *
     * @param networkListener listener object
     */
    public void addPeerListener(@NonNull NetworkPeerListener networkListener) {
        networkPeerListeners.add(networkListener);
    }

    /**
     * Adds a network packet listener.
     * <br/>
     * It listen the tag {@value Constants#ROUTING_ALL_TAG} as well, which is a special tag to listen all messages.
     *
     * @param tag                   message tag - filter
     * @param networkPacketListener packet listener object
     */
    public void addPacketListener(int tag, @NonNull NetworkPacketListener networkPacketListener) {
        networkPacketListeners.add(new PacketList(tag, networkPacketListener));
    }

    /**
     * Removes a network event listener.
     *
     * @param networkListener listener object
     */
    public void removeNetworkListener(@NonNull NetworkListener networkListener) {
        if (networkListener instanceof NetworkPeerListener) {
            if (!networkPeerListeners.remove(networkListener))
                NetLog.w(TAG, "NetworkPeerListener not on list");
        } else if (networkListener instanceof NetworkPacketListener) {
            if (!networkPacketListeners.remove(new PacketList(0, (NetworkPacketListener) networkListener))) {
                NetLog.w(TAG, "NetworkPacketListener not on list");
            }
        }
    }

    /**
     * Notifies a new peer has arrived.
     *
     * @param address peer logic address
     */
    public void notifyPeerJoin(@NonNull Address address) {
        // if network service is not booted - just do nothing
        if (!NetworkService.isRunning())
            return;

        for (NetworkPeerListener packetListener : networkPeerListeners)
            packetListener.onPeerJoin(address);
    }

    /**
     * Notifies a peer has left the network.
     *
     * @param address peer logic address
     */
    public void notifyPeerLeave(@NonNull Address address) {
        // if network service is not booted - just do nothing
        if (!NetworkService.isRunning())
            return;

        for (NetworkPeerListener packetListener : networkPeerListeners)
            packetListener.onPeerLeave(address);
    }

    /**
     * Notifies a new peer arrival packet message.
     *
     * @param bytes   message bytes
     * @param len     body message size
     * @param address source message logical address
     * @param tag     message tag
     */
    public void notifyPeerPacket(@NonNull byte[] bytes, int len, @NonNull Address address,
                                 @IntRange(from = Constants.ROUTING_TAG_MIN) int tag) {
        // if network service is not booted - just do nothing
        if (!NetworkService.isRunning())
            return;
        for (PacketList listener : networkPacketListeners) {
            if (listener.tag == Constants.ROUTING_ALL_TAG || listener.tag == tag)
                listener.packetListener.onNewPacket(bytes, len, address, tag);
        }
    }

    /**
     * Destroy and cleans all the registries.
     */
    public void destroy() {
        networkPeerListeners.clear();
        networkPacketListeners.clear();
    }
}

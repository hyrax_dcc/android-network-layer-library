/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * An error class definition.
 */
public class Error {
    public static final Error NO_ERROR = new Error();
    @Nullable
    private final String messageError;

    /**
     * Constructor.
     */
    private Error() {
        this.messageError = null;
    }

    /**
     * Constructor.
     *
     * @param messageError message error string
     */
    public Error(@NonNull String messageError) {
        this.messageError = messageError;
    }

    /**
     * Check if has error.
     *
     * @return <tt>true</tt> if has error, <tt>false</tt> otherwise
     */
    public boolean hasError() {
        return messageError != null;
    }

    /**
     * Returns the error message.
     * <br/>
     * Before getting the error message, please check it it has an error first.
     *
     * @return string message error
     * @throws NullPointerException if message error is NULL.
     */
    public String getError() {
        NetLog.assertNotNull(messageError, "Message error cannot be NULL. Please check first");
        return messageError;
    }
}

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

import android.support.annotation.NonNull;

import org.hyrax.link.Technology;
import org.hyrax.network.routing.packet.Packet;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Objects;

/**
 * This interface represents the client communication channel.
 */
public interface Channel extends Comparable<Channel> {
    HashMap<Technology, Integer> techMapOrder = new HashMap<Technology, Integer>() {{
        put(Technology.WIFI_DIRECT, 1);
        put(Technology.WIFI, 2);
        put(Technology.BLUETOOTH, 3);
        put(Technology.BLUETOOTH_LE, 4);
        put(Technology.MOBILE, 5);
    }};

    /**
     * Listener interface that listen events from the channel.
     */
    interface StatusListener {
        /**
         * Client has started.
         */
        void onStart();

        /**
         * Client has stopped.
         *
         * @param error error object. Check if stopped with errors or not
         */
        void onStop(@NonNull Error error);

        /**
         * Default status listener.
         */
        StatusListener NO_LISTENER = new StatusListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onStop(@NonNull Error error) {

            }
        };
    }

    /**
     * Interface that listens for incoming packets.
     */
    interface PacketListener {
        /**
         * Method triggered when a packet from a specific channel has arrived.
         *
         * @param header packet object (protocol buffers)
         * @param bytes  packet body bytes
         */
        void onPacket(@NonNull Packet.Header header, @NonNull byte[] bytes);

        /**
         * Default packet listener.
         */
        PacketListener NO_LISTENER = (header, bufferBody) -> {
        };
    }

    /**
     * Starts the channel.
     */
    void start();

    /**
     * Stops the channel.
     */
    void stop();

    /**
     * Verifies if the channel is running.
     *
     * @return <tt>true</tt> if running, <tt>false</tt> otherwise
     */
    boolean isRunning();

    /**
     * Returns the remote device address
     * <br/>
     * E.g ipAddress, macAddress
     *
     * @return string remote device address
     */
    @NonNull
    String getAddress();

    /**
     * Returns the technology that is being used.
     *
     * @return technology enum object
     */
    @NonNull
    Technology getTechnology();

    /**
     * Writes a packet header and body to the channel
     *
     * @param header packet header object (protocol buffers)
     * @param body   packet body
     * @return <tt>true</tt> if packet send, <tt>false</tt> otherwise
     */
    boolean writePacket(@NonNull Packet.Header header, @NonNull byte[] body);

    /**
     * Sets a channel status listener.
     *
     * @param statusListener channel status listener object
     */
    void setStatusListener(@NonNull StatusListener statusListener);

    /**
     * Sets a channel packet listener.
     *
     * @param packetListener packet listener object
     */
    void setPacketListener(@NonNull PacketListener packetListener);

    @Override
    default int compareTo(@NonNull Channel o) {
        if (this.channelEquals(o))
            return 0;
        return techMapOrder.get(getTechnology()) - techMapOrder.get(o.getTechnology());
    }

    /**
     * Check a channel is the same as the current.
     *
     * @param channel channel to compare with
     * @return <tt>true</tt> if two channel objects are the same, <tt>false</tt> otherwise
     */
    default boolean channelEquals(@NonNull Channel channel) {
        return (this.getTechnology() == channel.getTechnology()) &&
                (this.getAddress().equals(channel.getAddress()));
    }

    /**
     * Returns an hash to the current channel object
     *
     * @return integer representing an hash
     */
    default int channelHash() {
        return Objects.hash(getTechnology(), getAddress());
    }
}

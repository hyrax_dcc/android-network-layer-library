/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import junit.framework.Assert;

import org.hyrax.link.Technology;
import org.hyrax.network.NetworkService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SocketHandler;

/**
 * Debugger class.
 * <p/>
 * Debugs actions of the network layer as well as the link layer events.
 */
public final class NetLog {
    private static final AtomicBoolean active = new AtomicBoolean(false);
    private static List<NetLogListener> logListener;
    private static LogHandler logHandler;

    /**
     * Debug messages severity level.
     */
    public enum Level {
        INFO,
        DEBUG,
        WARNING,
        ERROR,
    }

    /**
     * Interface that listen for logs produced by the network layer.
     */
    public interface NetLogListener {
        /**
         * Method triggered when a method is available.
         *
         * @param level    severity log level
         * @param logEntry log entry object
         */
        void onLog(@NonNull Level level, @NonNull LogEntry logEntry);
    }

    /**
     * Log class entry.
     */
    public static class LogEntry {
        public final String tag;
        public final String message;

        /**
         * Constructor.
         *
         * @param tag     log tag
         * @param message log message
         */
        LogEntry(@NonNull String tag, @NonNull String message) {
            this.tag = tag;
            this.message = message;
        }
    }

    /**
     * Enables the debugger.
     */
    public static void enable() {
        if (active.compareAndSet(false, true)) {
            logListener = new ArrayList<>();
            return;
        }
        throw new RuntimeException("NetLog: Already active");
    }

    /**
     * Activates the log to a file.
     *
     * @param context  application context
     * @param filePath file location
     * @param port     If port > 0 sends logs to a remote server, otherwise does not send
     */
    public static void enableLogHandler(@NonNull Context context, @NonNull String filePath, int port) {
        if (logHandler == null) {
            logHandler = new LogHandler(filePath, "127.0.0.1", port);
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                logHandler.logStats(context, 1000);
            }*/
            return;
        }
        throw new RuntimeException("NetLog: HandlerLog already active.");
    }

    /**
     * Adds a network log listener.
     *
     * @param listener listener object
     */
    public static void addNetLogListener(@NonNull NetLogListener listener) {
        assertNotNull(logListener);
        logListener.add(listener);
    }

    /**
     * Disables the debug messages.
     */
    public static void disable() {
        if (active.compareAndSet(true, false)) {
            logListener.clear();
        }
        if (logHandler != null) {
            logHandler.destroy();
            logHandler = null;
        }
    }

    /**
     * Publish event logs.
     *
     * @param level    log event level
     * @param logEntry log entry object
     */
    private static void publish(@NonNull Level level, @NonNull LogEntry logEntry) {
        if (active.get()) {
            for (NetLogListener listener : logListener)
                listener.onLog(level, logEntry);
        }
    }

    /**
     * Logs info level activity.
     *
     * @param tag     log tag
     * @param message log message
     */
    public static void i(@NonNull String tag, @NonNull String message) {
        publish(Level.INFO, new LogEntry(tag, message));
        Log.i(tag, message);
    }

    /**
     * Logs debug level activity.
     *
     * @param tag     log tag
     * @param message log message
     */
    public static void d(@NonNull String tag, @NonNull String message) {
        publish(Level.DEBUG, new LogEntry(tag, message));
        Log.d(tag, message);
    }

    /**
     * Logs warning level activity.
     *
     * @param tag     log tag
     * @param message log message
     */
    public static void w(@NonNull String tag, @NonNull String message) {
        publish(Level.WARNING, new LogEntry(tag, message));
        Log.w(tag, message);
    }

    /**
     * Logs error level activity.
     *
     * @param tag     log tag
     * @param message log message
     */
    public static void e(@NonNull String tag, @NonNull String message) {
        publish(Level.ERROR, new LogEntry(tag, message));
        Log.e(tag, message);
    }

    /**
     * Asserts if two objects are equals.
     *
     * @param expected expected object
     * @param actual   action object
     * @param message  log message
     */
    public static <A, B> void assertEquals(@NonNull A expected, @NonNull B actual, @NonNull String message) {
        if (active.get()) {
            Assert.assertEquals(message, expected, actual);
        }
    }

    /**
     * Asserts if an object instance belongs to a particular class.
     *
     * @param obj     object to te checked
     * @param c       expected class instance
     * @param message log message
     */
    public static <T> void assertInstance(@NonNull T obj, @NonNull Class c, @NonNull String message) {
        if (active.get()) {
            Assert.assertTrue(message, c.isAssignableFrom(obj.getClass()));
        }
    }

    /**
     * Asserts if an object is not null.
     *
     * @param obj actual object
     */
    public static void assertNotNull(@Nullable Object obj) {
        if (active.get()) {
            Assert.assertNotNull(obj);
        }
    }

    /**
     * Asserts if an object is not null.
     *
     * @param obj     actual object
     * @param message assert error message
     */
    public static void assertNotNull(@Nullable Object obj, @NonNull String message) {
        if (active.get()) {
            Assert.assertNotNull(message, obj);
        }
    }

    /**
     * Log taxonomy.
     */
    public enum LogTag {
        BATTERY(false),
        SYSTEM_STATS(false),
        //MEMORY(false),
        //CPU_PID_STAT(false),
        //CPU_STAT(false),
        FORMATION(false),
        PACKET_SEND(false),
        PACKET_RCV(false),
        PACKET_VALID(false),
        PACKET_DROP(false),
        DATAGRAM_ROUTE(false),

        TEST_PACKET_BEGIN(false),
        TEST_PACKET_SEND(false),
        TEST_PACKET_RCV(false),
        TEST_PACKET_ACK(false),
        TEST_PACKET_END(false),

        PEER_ONLINE(true),
        PEER_OFFLINE(true),
        PEER_NEW(true),
        PEER_LOST(true),

        DISCOVERY_ON(false),
        DISCOVERY_OFF(false),
        VISIBLE_ON(false),
        VISIBLE_OFF(false),
        SERVER_ON(false),
        SERVER_OFF(false),
        CONNECTION_ATTEMPT(false),
        CONNECTION_NEW(false),
        CONNECTION_LOST(false),;
        // loggable for the network viewer
        private boolean netViewLog;

        /**
         * Constructor
         *
         * @param netViewLog <tt>true</tt> to log to the network viewer, <tt>false</tt> otherwise
         */
        LogTag(boolean netViewLog) {
            this.netViewLog = netViewLog;
        }

        /**
         * Returns true if to log the network viewer, false otherwise.
         *
         * @return <tt>true</tt> if to log, <tt>false</tt> otherwise
         */
        boolean isNetViewLoggable() {
            return netViewLog;
        }
    }

    /**
     * Logs the online peer.
     *
     * @param thisDev     current device logic address
     * @param routingType routing algorithm type
     */
    public static void logPeerOnline(@NonNull String thisDev, @NonNull String routingType) {
        if (logHandler != null)
            logHandler.log(LogTag.PEER_ONLINE, thisDev, routingType);
    }

    /**
     * Logs the offline peer.
     *
     * @param thisDev     current device logic address
     * @param routingType routing algorithm type
     */
    public static void logPeerOffline(@NonNull String thisDev, @NonNull String routingType) {
        if (logHandler != null)
            logHandler.log(LogTag.PEER_OFFLINE, thisDev, routingType);
    }

    /**
     * Logs a new peer connection.
     *
     * @param rmtDev     remote device logic address
     * @param technology channel technology
     */
    public static void logPeerNew(@NonNull String rmtDev, @NonNull Technology technology) {
        if (logHandler != null)
            logHandler.log(LogTag.PEER_NEW, rmtDev, technology);
    }

    /**
     * Logs a lost peer connection.
     *
     * @param rmtDev     remote device logic address
     * @param technology channel technology
     */
    public static void logPeerLost(@NonNull String rmtDev, @NonNull Technology technology) {
        if (logHandler != null)
            logHandler.log(LogTag.PEER_LOST, rmtDev, technology);
    }

    /**
     * Logs when a network formation is enabled or disabled.
     *
     * @param formationType formation algorithm type
     * @param status        formation algorithm status
     */
    public static void logFormation(@NonNull String formationType, String status) {
        if (logHandler != null)
            logHandler.log(LogTag.FORMATION, formationType, status);
    }

    /**
     * Logs a send message intention.
     *
     * @param destDev   destination device logic address
     * @param msgId     message identifier
     * @param msgTag    message tag
     * @param msgLength message (payload) length
     */
    public static void logPacketSend(@NonNull String destDev, int msgId, int msgTag, int msgLength) {
        if (logHandler != null)
            logHandler.log(LogTag.PACKET_SEND, destDev, msgId, msgTag, msgLength);
    }

    /**
     * Logs a received packet.
     *
     * @param srcDev      source device (packet) logic address
     * @param msgId       message identifier
     * @param fromChannel receive channel logic address
     * @param technology  receive channel technology
     */
    public static void logPacketRcv(String srcDev, int msgId, @NonNull String fromChannel,
                                    @NonNull String technology) {
        if (logHandler != null)
            logHandler.log(LogTag.PACKET_RCV, srcDev, msgId, fromChannel, technology);
    }

    /**
     * Logs a datagram message route.
     *
     * @param srcDev     source device logic address
     * @param msgId      message identifier
     * @param toChannel  exit channel address
     * @param technology exit channel technology
     * @param success    <tt>true</tt> if write success, <tt>false</tt> otherwise
     */
    public static void logDatagramRoute(@NonNull String srcDev, int msgId, int datagramLength,
                                        @NonNull String toChannel, @NonNull String technology, boolean success) {
        if (logHandler != null)
            logHandler.log(LogTag.DATAGRAM_ROUTE, srcDev, msgId, datagramLength, toChannel, technology, success);
    }

    /**
     * Logs a valid packet.
     *
     * @param msgId message identifier
     */
    public static void logPacketValid(@NonNull String srcDev, int msgId) {
        if (logHandler != null)
            logHandler.log(LogTag.PACKET_VALID, srcDev, msgId);
    }

    /**
     * Logs a dropped packet
     *
     * @param msgId message identifier
     */
    public static void logPacketDrop(@NonNull String srcDev, int msgId) {
        if (logHandler != null)
            logHandler.log(LogTag.PACKET_DROP, srcDev, msgId);
    }

    /**
     * Logs the begging of a packets exchange test.
     *
     * @param nPack     number of packets to be exchanged
     * @param pktLength length of each packet
     * @param pktRate   rate of packets per second
     * @param nHops     number of messages hops
     */
    public static void logTestPacketBegin(int nPack, int pktLength, int pktRate, int nHops) {
        if (logHandler != null)
            logHandler.log(LogTag.TEST_PACKET_BEGIN, nPack, pktLength, pktRate, nHops);
    }

    /**
     * Logs the end of a packets exchange test.
     */
    public static void logTestPacketEnd() {
        if (logHandler != null)
            logHandler.log(LogTag.TEST_PACKET_END);
    }

    /**
     * Logs a send packet message (Application level).
     *
     * @param destDev  destination logic address
     * @param pktMsgId app message id
     */
    public static void logTestPacketSend(@NonNull String destDev, int pktMsgId) {
        if (logHandler != null)
            logHandler.log(LogTag.TEST_PACKET_SEND, destDev, pktMsgId);
    }

    /**
     * Logs a received packet message (Application level).
     *
     * @param srcDev   logic source address
     * @param pktMsgId app message id
     */
    public static void logTestPacketRcv(@NonNull String srcDev, int pktMsgId) {
        if (logHandler != null)
            logHandler.log(LogTag.TEST_PACKET_RCV, srcDev, pktMsgId);
    }

    /**
     * Logs a ack of a previously sent message (Application level).
     *
     * @param destDev  logic destination address.
     * @param pktMsgId app message id
     */
    public static void logTestPacketAck(@NonNull String destDev, int pktMsgId) {
        if (logHandler != null)
            logHandler.log(LogTag.TEST_PACKET_ACK, destDev, pktMsgId);
    }

    /**
     * Just do logs.
     *
     * @param logTag log tag - taxonomy
     * @param data   log extra data
     */
    public static void log(@NonNull LogTag logTag, @NonNull Object... data) {
        if (logHandler != null)
            logHandler.log(logTag, data);
    }

    /**
     * Logger class.
     */
    public static class LogHandler {
        private final Logger logger;
        private FileHandler fileHandler;
        @Nullable
        private SocketHandler netViewerHandler;
        //executor thread that takes care of log stuff burden
        private final ExecutorService executor;
        private final ScheduledExecutorService scheduledExecutor;
        private final Map<String, LogTag> tagMap;

        /**
         * Constructor.
         *
         * @param filePath log file path
         * @param host     server host
         * @param port     server port
         */
        private LogHandler(@NonNull String filePath, @NonNull String host, int port) {
            this.logger = Logger.getLogger("NetworkLogger");
            try {
                this.fileHandler = new FileHandler(filePath);
                this.fileHandler.setFormatter(new Formatter() {
                    @Override
                    public String format(LogRecord record) {
                        String params = paramsToString(record.getParameters());
                        return String.format(Locale.ENGLISH, "%d %s, %s\n",
                                record.getMillis(), record.getMessage(), params);
                    }
                });
                logger.addHandler(fileHandler);
            } catch (IOException e) {
                e.printStackTrace();
                NetLog.e("LogHandler", e.getMessage());
            }

            this.tagMap = new HashMap<>();
            for (LogTag tag : LogTag.values())
                tagMap.put(tag.name(), tag);

            this.executor = Executors.newSingleThreadExecutor();
            this.scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
            if (port == 0)
                this.netViewerHandler = null;
            else {
                executor.execute(() -> {
                    try {
                        this.netViewerHandler = new SocketHandler(host, port);
                        netViewerHandler.setFilter(record -> tagMap.containsKey(record.getMessage()) &&
                                tagMap.get(record.getMessage()).isNetViewLoggable());
                        netViewerHandler.setFormatter(new Formatter() {
                            @Override
                            public String format(LogRecord record) {
                                return String.format(Locale.ENGLISH, "%s, %s\n",
                                        record.getMessage(),
                                        paramsToString(addLocalAdrToParams(record.getParameters())));
                            }

                            /**
                             * Adds local address to the params list.
                             * @param oldParams old params list
                             * @return new params list
                             */
                            private Object[] addLocalAdrToParams(@Nullable Object[] oldParams) {
                                String local = NetworkService.getLocalAddress().toString();
                                if (oldParams == null)
                                    return new Object[]{local};
                                Object[] params = new Object[oldParams.length + 1];
                                params[0] = local;
                                System.arraycopy(oldParams, 0, params, 1, oldParams.length);
                                return params;
                            }
                        });
                        logger.addHandler(netViewerHandler);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });
            }
        }


        /**
         * Transforms an array of objects to string.
         *
         * @param params objects array
         * @return string format
         */
        @NonNull
        private String paramsToString(@Nullable Object[] params) {
            StringBuilder argsBuilder = new StringBuilder();
            if (params != null && params.length > 0) {
                argsBuilder.append(params[0].toString());
                for (int i = 1; i < params.length; i++) {
                    argsBuilder.append(", ");
                    argsBuilder.append(params[i].toString());
                }
            }
            return argsBuilder.toString();
        }

        /**
         * Sends the log to a queue to be written when possible on a file.
         *
         * @param tag  log tag - taxonomy
         * @param data log extra data
         */
        private void log(@NonNull LogTag tag, Object... data) {
            final long nanoTime = System.nanoTime();
            executor.execute(() -> doLog(tag, nanoTime, data));
        }

        /**
         * Logs a message of a certain type with specific arguments
         *
         * @param tag      log tag - taxonomy
         * @param nanoTime timestamp in nano seconds
         * @param data     log extra data
         */
        private void doLog(@NonNull LogTag tag, long nanoTime, Object... data) {
            LogRecord lr = new LogRecord(java.util.logging.Level.INFO, tag.name());
            lr.setParameters(data);
            lr.setMillis(nanoTime);
            logger.log(lr);
        }

        /**
         * Closes the logger and forces to write everything from memory to file disk.
         */
        private void destroy() {
            //closes the executor service and waits for all work to be done
            executor.shutdown();
            scheduledExecutor.shutdown();
            //flushes the logs to file
            fileHandler.flush();
            fileHandler.close();
            logger.removeHandler(fileHandler);
            //flusher the network socket handler
            if (netViewerHandler != null) {
                netViewerHandler.flush();
                netViewerHandler.close();
                logger.removeHandler(netViewerHandler);
            }
        }

        /**
         * Logs all meaningful statistics about the current process, cpu, memory and
         * also battery consumptions, every {@param period} fixed time interval.
         *
         * @param context application context
         * @param period  fixed delay interval period in milliseconds.
         */
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public void logStats(@NonNull Context context, long period) {
            //get current process identifier
            final int processPid = android.os.Process.myPid();

            scheduledExecutor.scheduleWithFixedDelay(
                    () -> {
                        //logs battery
                        //logBatteryStats(context);
                        //logs cpu
                        //logCpuStats(processPid);
                        //logs memory
                        //logMemoryStats(processPid);
                        //logs system resources
                        logResourcesStats(processPid);
                    }, 0, period, TimeUnit.MILLISECONDS);
        }

        /**
         * Reads the battery stats.
         *
         * @param context application context
         * @link https://source.android.com/devices/tech/power/device
         */
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        private void logBatteryStats(@NonNull Context context) {
            BatteryManager batteryManager = (BatteryManager) context.getSystemService(
                    Context.BATTERY_SERVICE);
            Objects.requireNonNull(batteryManager);
            //current in microamperes,
            int currentNow = batteryManager
                    .getIntProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
            //capacity in microampere-hours
            int chargeCounter = batteryManager
                    .getIntProperty(BatteryManager.BATTERY_PROPERTY_CHARGE_COUNTER);
            //energy in nanowatt-hours
            long energyCounter = batteryManager
                    .getLongProperty(BatteryManager.BATTERY_PROPERTY_ENERGY_COUNTER);
            //gets battery voltage
            Intent intent = context.registerReceiver(
                    null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            Objects.requireNonNull(intent);
            long voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
            //log
            doLog(LogTag.BATTERY, currentNow, chargeCounter, energyCounter, voltage);
        }

        /**
         * Reads the cpu usage.
         * More details in @link http://man7.org/linux/man-pages/man5/proc.5.html
         *
         * @param pid process identifier
         */
        private void logCpuStats(int pid) {
            try (BufferedReader r1 = new BufferedReader(new FileReader("/proc/" + pid + "/stat"));
                 BufferedReader r2 = new BufferedReader(new FileReader("/proc/stat"))) {
                //doLog(LogTag.CPU_PID_STAT, System.nanoTime(), r1.readLine());
                //doLog(LogTag.CPU_STAT, System.nanoTime(), r2.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /**
         * Reads the memory usage.
         * More details in @link http://man7.org/linux/man-pages/man5/proc.5.html
         *
         * @param pid process identifier
         */
        private void logMemoryStats(int pid) {
            try (BufferedReader r = new BufferedReader(new FileReader("/proc/" + pid + "/statm"))) {
                //doLog(LogTag.MEMORY, System.nanoTime(), r.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        /**
         * Logs the system resources CPU and Memory
         *
         * @param pid process identifier
         */
        private void logResourcesStats(int pid) {
            try (BufferedReader pidCpu = new BufferedReader(new FileReader("/proc/" + pid + "/stat"));
                 BufferedReader totalCpu = new BufferedReader(new FileReader("/proc/stat"));
                 BufferedReader mem = new BufferedReader(new FileReader("/proc/" + pid + "/statm"))) {

                long[] cpuStats = processCpuStats(pidCpu.readLine(), totalCpu.readLine());
                long[] memStats = processMemoryStats(mem.readLine());
                doLog(LogTag.SYSTEM_STATS, System.nanoTime(),
                        cpuStats[0], cpuStats[1], cpuStats[2], // cpu stats log
                        memStats[0], memStats[1], memStats[2]); // memory stats log
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private long[] processCpuStats(@NonNull String pidCpu, @NonNull String totalCpu) {
            String[] pidCpuParts = pidCpu.split("\\s+", -1);
            // pid utime
            long utime = getPidUTime(pidCpuParts);
            String[] totalCpuParts = totalCpu.split("\\s+", -1);
            // total cpu idle time
            long idle = getCpuIdleTime(totalCpuParts);
            // total cpu non idle time
            long non_idle = getCpuNonIdleTime(totalCpuParts);
            return new long[]{utime, idle, non_idle};
        }

        private long getCpuIdleTime(String[] line) {
            long idle = Long.valueOf(line[4]);
            long iowait = Long.valueOf(line[5]);
            return idle + iowait;
        }

        private long getCpuNonIdleTime(String[] line) {
            long user = Long.valueOf(line[1]);
            long nice = Long.valueOf(line[2]);
            long system = Long.valueOf(line[3]);
            long irq = Long.valueOf(line[6]);
            long softirq = Long.valueOf(line[7]);
            long steal = Long.valueOf(line[8]);
            return user + nice + system + irq + softirq + steal;
        }

        private long getPidUTime(String[] parts) {
            return Long.valueOf(parts[13]);
        }

        private long[] processMemoryStats(@NonNull String strLine) {
            String[] parts = strLine.split(" ");
            int page_size = 4096;
            long total = Long.valueOf(parts[0]) * page_size;
            long rss = Long.valueOf(parts[1]) * page_size;
            long shared = Long.valueOf(parts[2]) * page_size;
            return new long[]{total, rss, shared};
        }
    }
}

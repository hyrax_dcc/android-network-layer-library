/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.misc;

import android.support.annotation.NonNull;

/**
 * This interface represents the server communication channel.
 */
public interface ChannelServer {
    /**
     * Listener interface that listen events from channel server.
     */
    interface StatusListener {
        /**
         * Server has started.
         */
        void onStart();

        /**
         * Server has stopped.
         *
         * @param error object. Check if stopped with errors or not
         */
        void onStop(@NonNull Error error);

        /**
         * A new client available.
         *
         * @param channel client channel object
         */
        void onNewClient(@NonNull Channel channel);

        //default status listener
        StatusListener NO_LISTENER = new StatusListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onStop(@NonNull Error error) {

            }

            @Override
            public void onNewClient(@NonNull Channel channel) {

            }
        };
    }

    /**
     * Starts the server.
     */
    void start();

    /**
     * Stops the server.
     */
    void stop();

    /**
     * Verifies if the channel server is running.
     *
     * @return <tt>true</tt> if running, <tt>false</tt> otherwise
     */
    boolean isRunning();

    /**
     * Returns the port that is listen to.
     *
     * @return Integer port
     */
    int getPort();

    /**
     * Returns the physical address of the server.
     *
     * @return String address
     */
    String getAddress();

    /**
     * Sets a status server listener.
     *
     * @param statusListener server status listener object
     */
    void setStatusListener(@NonNull StatusListener statusListener);
}

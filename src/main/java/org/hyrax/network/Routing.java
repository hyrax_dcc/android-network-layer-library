/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.NonNull;

import org.hyrax.network.misc.Channel;
import org.hyrax.network.misc.NetworkRegistry;
import org.hyrax.network.routing.packet.Packet;

/**
 * Interface that defines a routing algorithm.
 */
public interface Routing {
    /**
     * Enum class that represents the routing algorithm type.
     */
    enum Type {
        /**
         * Flood routing algorithm.
         */
        FLOOD,
        /**
         * Controlled flood routing algorithm.
         */
        FLOOD_CONTROL,
        /**
         * A custom routing.
         */
        CUSTOM
    }

    /**
     * Returns the routing algorithm name.
     *
     * @return string routing name
     */
    @NonNull
    String getName();

    /**
     * Returns the routing type.
     *
     * @return enum routing type
     */
    @NonNull
    Type getType();

    /**
     * Changes the network registry object.
     *
     * @param networkRegistry network registry object
     */
    void setNetworkRegistry(@NonNull NetworkRegistry networkRegistry);

    /**
     * Method triggered when a new peer has been connected and the logical address resolved.
     *
     * @param address remote logical address
     * @param channel channel object
     */
    void onPeerJoin(@NonNull Address address, @NonNull Channel channel);

    /**
     * Method triggered when a peers has lost the connection.
     *
     * @param address remote logical address
     * @param channel channel object
     */
    void onPeerLeave(@NonNull Address address, @NonNull Channel channel);

    /**
     * Method triggered when a new packet has arrived.
     *
     * @param address direct peer logical address
     * @param channel channel address
     * @param header  packet header object (protocol buffers)
     * @param body    packet body
     */
    void onPeerPacket(@NonNull Address address, @NonNull Channel channel,
                      @NonNull Packet.Header header, @NonNull byte[] body);

    /**
     * Sends a message to a specific destination.
     *
     * @param bytes             message bytes
     * @param destination       destination address
     * @param routingProperties send definition properties
     */
    void send(@NonNull byte[] bytes, @NonNull Address destination,
              @NonNull RoutingProperties routingProperties);

    /**
     * Propagates a message through the network while the properties are satisfied.
     * </p>
     * The send properties allows to control the network the message propagation radius.
     *
     * @param bytes             message bytes
     * @param routingProperties send definition properties
     */
    void sendToAll(@NonNull byte[] bytes, @NonNull RoutingProperties routingProperties);

    /**
     * Starts the routing algorithm.
     */
    void start();

    /**
     * Stop the routing process and cleans the allocated data structures.
     */
    void destroy();
}

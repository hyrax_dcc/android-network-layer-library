/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation;

import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.link.Device;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiServerSettings;
import org.hyrax.network.Address;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.Network;
import org.hyrax.network.NetworkPeerListener;
import org.hyrax.network.NetworkService;
import org.hyrax.network.Routing;
import org.hyrax.network.RoutingProperties;

/**
 * Created on 9/17/18.
 */
public class Test {

    Test() {
        String SERVICE_NAME = "WD_GROUP";
        int PORT = 8080;
        // server
        LinkPromise<WifiDirectLink> wdLink = LinkService.getLinkPromise(Technology.WIFI_DIRECT);
        ServerProperties<WifiServerSettings> serverProperties = wdLink
                .getFeatures().newServerBuilder()
                .setSettings(WifiServerSettings.newBuilder()
                        .enableProxy(true).build())
                .build();
        VisibilityProperties visProperties = wdLink
                .getFeatures().newVisibilityBuilder()
                .setAdvertiseId("_id")
                .setAdvertiserSettings(WifiAdvertiseSettings.newBuilder(SERVICE_NAME)
                        .setPort(PORT)
                        .build())
                .setAdvertiserData(new WifiAdvertiseData() {{
                    addData("KEY_1", "VALUE_1");
                    addData("KEY_2", "VALUE_2");
                }})
                .setTimeout(5000)
                .build();

        wdLink.enable()
                .done(enabled -> wdLink.accept(serverProperties))
                .done(accept -> wdLink.setVisible(visProperties))
                .done(visible -> Log.d("GroupOwner", "Device is group owner and is visible"))
                .fail(error -> Log.e("GroupOwner", "Group creation failed, please try later"));


        // client
        DiscoveryProperties<Void, WifiScanFilter> disProperties = wdLink
                .getFeatures().newDiscoveryBuilder()
                .setScanId("_id")
                .setScannerFilter(WifiScanFilter.newBuilder()
                        .setServiceName(SERVICE_NAME)
                        .setFilter(device -> device.getName().startsWith("prefix"))
                        .build()
                )
                .setStopRule(devices -> devices.size() > 1)
                .setTimeout(5000)
                .stopAfterTimeoutExpiration(true)
                .build();
        wdLink.enable()
                .done(enabled -> wdLink.discover(disProperties))
                .done(devices -> {
                    Device dev = pickOne(devices);
                    ConnectionProperties<WifiConnectionSettings> connProperties = wdLink
                            .getFeatures().newConnectionBuilder(dev)
                            .setTimeout(10000)
                            .build();
                    return wdLink.connect(dev, connProperties);
                })
                .done(devConnected -> Log.d("GroupClient", "Device connected to group"))
                .fail(error -> Log.e("GroupClient", "Group connection failed"));


        Network network = NetworkService.boot(context, Routing.Type.FLOOD_CONTROL);
        // listens network events
        network.addNetworkListener(new NetworkPeerListener() {
            @Override
            public void onPeerJoin(@NonNull Address address) {
                // peer join notification
            }

            @Override
            public void onPeerLeave(@NonNull Address address) {
                // peer leave notification
            }
        });
        // listen network messages
        network.addPacketListener((bytes, len, srcAddress, tag) -> {
            // network messages
        });
        network.enableFormation(Formation.Type.WIFI_DIRECT_STAR);
        network.enableFormation(Formation.Type.BLUETOOTH_MESH_RANDOM,
                                FormationProperties.newBuilder().build());
        // sends message to everyone
        network.sendToAll("Hello World to All!".getBytes(), ALL);
        // sends message to a specific device
        network.send("Hello World!".getBytes(), destAddress, ONE);
        network.send("Hi!".getBytes(), destAdr, RoutingProperties
                .newBuilder(HI)
                .setMaxNumberOfHops(5).build());

        network.disableFormation(Formation.Type.WIFI_DIRECT_STAR);
        network.disableFormation(Formation.Type.BLUETOOTH_MESH_RANDOM);

        NetworkService.shutdown();
    }
}

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation.wifi;

import android.support.annotation.NonNull;

import org.hyrax.network.misc.Constants;

/**
 * Class responsible for holding the wifi credentials.
 */
class Credential {
    @NonNull
    final String ssid;
    @NonNull
    final String password;
    @NonNull
    final String proxyHost;
    final int proxyPort;
    final int serverListenPort;

    /**
     * Constructor.
     *
     * @param ssid             wifi network name
     * @param password         wifi network password
     * @param serverListenPort socket server listen port
     */
    Credential(@NonNull String ssid, @NonNull String password, int serverListenPort) {
        this(ssid, password, serverListenPort, Constants.UNDEFINED, 0);
    }

    /**
     * Constructor.
     *
     * @param ssid             wifi network name
     * @param password         wifi network password
     * @param serverListenPort socket server listen port
     * @param proxyHost        proxy server host address
     * @param proxyPort        proxy server listen port
     */
    private Credential(@NonNull String ssid, @NonNull String password, int serverListenPort,
                       @NonNull String proxyHost, int proxyPort) {
        this.ssid = ssid;
        this.password = password;
        this.serverListenPort = serverListenPort;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
    }

    /**
     * Verifies if proxy is defined
     *
     * @return <tt>true</tt> if proxy is defined,<tt>false</tt> otherwise
     */
    protected boolean hasProxy() {
        return !Constants.UNDEFINED.equals(proxyHost) && proxyPort > 0;
    }
}

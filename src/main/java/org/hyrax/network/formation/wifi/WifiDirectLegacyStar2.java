/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation.wifi;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkEvent;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.direct.WifiDirectCredentials;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.NetworkService;
import org.hyrax.network.channel.AdvChannel;
import org.hyrax.network.channel.ChannelRegistry;
import org.hyrax.network.formation.FormationInit;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.misc.ChannelServer;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.NetLog;
import org.jdeferred2.AlwaysCallback;
import org.jdeferred2.DoneCallback;
import org.jdeferred2.Promise;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Wifi Direct Legacy star, version 2, topology implementation.
 * </p>
 * This algorithm is similar to Wifi Direct Legacy star, but the devices, must be
 * connected to a Wifi network in order to perform a discovery.
 * </p>
 * Only the group owner is able to participate on a normal wifi network and wifi p2p group,
 * the remaining devices (clients) can only be at the group.
 * <p/>
 * The algorithm implements a simple star network, where one device will dynamically act as
 * a master(group owner) of the network and the other will connect to it.
 * <p/>
 * Note, this algorithm only creates isolated groups, then communication is
 * just performed inside the groups.
 */
public class WifiDirectLegacyStar2 implements Formation {
    private static final String TAG = "WDLegStar2";
    private static final String NAME = "WifiDirectLegacyStar2";

    private FormationInit formationInit;
    // wifi direct link
    private LinkPromise<WifiDirectLink> wdLink;
    // wifi link
    private final LinkPromise<WifiLink> wifiLink;
    @Nullable
    private Topology topology;

    // wifi hardware listener
    private boolean hwRegistered;

    private final LinkListener<Boolean> hwListener = ((eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_HARDWARE_ON:
                start();
                break;
            case LINK_HARDWARE_OFF:
                lightStop();
                break;
        }
    });

    // wifi direct connections listener
    private final LinkListener<Device> connectionListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        Device device = outcome.getOutcome();
        switch (eventType) {
            case LINK_CONNECTION_NEW:
                if (topology == null)
                    return;

                Log.w("Connect", "To " + device);

                boolean isOnGroup = topology.isConnectedToWifiDirectGroup();
                boolean isGo = wdLink.isAccepting(topology.serverId);
                NetLog.i(TAG, String.format(Locale.ENGLISH,
                        "New Connection %s -> %s || OnGroup: %s || isGo: %s", device.getName(),
                        device.getUniqueAddress(), Boolean.toString(isOnGroup), Boolean.toString(isGo)));
                // if is a group peer - then establish a socket to the group owner
                if (isOnGroup && !isGo) {
                    // connects socket to group owner
                    topology.connectToGo(device);
                }
                break;
            case LINK_CONNECTION_LOST:
                NetLog.w(TAG, "Lost Connection " + device.getUniqueAddress());
                break;
        }
    };

    public WifiDirectLegacyStar2() {
        this.wdLink = LinkService.getLinkPromise(Technology.WIFI_DIRECT);
        this.wifiLink = LinkService.getLinkPromise(Technology.WIFI);
        LibFormation.listenAndRecordEvents(wdLink);
        LibFormation.listenAndRecordEvents(wifiLink);
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.WIFI_DIRECT_LEGACY_STAR_2;
    }

    @Override
    public void setFormationInit(@NonNull FormationInit formationInit) {
        this.formationInit = formationInit;
    }

    @NonNull
    @Override
    public FormationProperties getDefaultFormationProperties() {
        return FormationProperties.newBuilder()
                .setDiscoveryTimeLimits(4000, 6000)
                .setVisibilityTimeLimits(15000, 20000)
                .setConnectionLimits(3, 5)
                .setAcceptProbability((double) 1 / 4)
                .setIdleTime(40000)
                .setEnableHardwareOnStart(true)
                .build();
    }

    @Override
    public void start() {
        assert formationInit != null;
        if (!hwRegistered) {
            wdLink.listen(EventCategory.LINK_HARDWARE, hwListener);
            hwRegistered = true;
        }

        boolean toEnableHw = formationInit.formationProperties.isToEnableHardwareOnStart();
        if (toEnableHw && !wdLink.isEnabled()) {
            wdLink.enable()
                    .done(result -> NetLog.d(TAG, "Wifi hardware enabled"))
                    .fail(result -> NetLog.e(TAG, "Enable Wifi hardware failure"));

        } else if (wdLink.isEnabled() && (topology == null || !topology.isRunning())) {
            formationInit.channelRegistry.createSocketChannelServerIfNotExists(0, Technology.WIFI_DIRECT)
                    .done(server -> {
                        wdLink.listen(EventCategory.LINK_CONNECTION, connectionListener);
                        wifiLink.listen(EventCategory.LINK_CONNECTION, connectionListener);

                        topology = new Topology(wdLink, wifiLink, formationInit, server);
                        topology.start();
                    })
                    .fail(error -> NetLog.e(TAG, "Network Start failure. " + error.getMessage()));
        }
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    /**
     * Stops the network formation but keeps some service running.
     */
    private void lightStop() {
        wdLink.removeListener(connectionListener);
        wifiLink.removeListener(connectionListener);

        if (topology != null)
            topology.stop();
        topology = null;
    }

    @Override
    public synchronized void stop() {
        lightStop();
        wdLink.removeListener(hwListener);
        hwRegistered = false;
        LinkService.shutdownLink(Technology.WIFI);

        try {
            Thread.sleep(50);
            if (formationInit.formationProperties.isToEnableHardwareOnStart()) {
                wdLink.disable();
                LinkService.shutdownLink(Technology.WIFI_DIRECT);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (formationInit.formationProperties.isToEnableHardwareOnStart())
            wdLink.disable();
        LibFormation.stopListenEvents(wdLink);
        LibFormation.stopListenEvents(wifiLink);
    }

    @Override
    public boolean isRunning() {
        return topology != null && topology.isRunning();
    }

    private static class Topology {
        private static final int ADV_CHANNEL_PORT = 9500;

        private LinkPromise<WifiDirectLink> wdLink;
        private LinkPromise<WifiLink> wifiLink;
        private final FormationProperties formationProperties;
        private final ChannelRegistry channelRegistry;
        private final ScheduledExecutorService executorService;
        private ChannelServer channelServer;

        private final Random random;
        private String serverId;

        private AdvChannel advChannel;
        private AtomicBoolean running;
        private int magicNumber;
        private final Map<String, Credential> gos;
        private final Map<String, Device> networks;

        /**
         * Constructor.
         *
         * @param wdLink        wifi direct link object
         * @param wifiLink      wifi link object
         * @param formationInit initialization object
         * @param channelServer socket server object
         */
        private Topology(@NonNull LinkPromise<WifiDirectLink> wdLink,
                         @NonNull LinkPromise<WifiLink> wifiLink,
                         @NonNull FormationInit formationInit,
                         @NonNull ChannelServer channelServer) {
            this.wdLink = wdLink;
            this.wifiLink = wifiLink;
            this.formationProperties = formationInit.formationProperties;
            this.channelRegistry = formationInit.channelRegistry;
            this.executorService = formationInit.executorService;
            this.channelServer = channelServer;

            this.random = new Random();

            this.serverId = wdLink.getFeatures().newServerBuilder().build().getIdentifier();
            this.advChannel = new AdvChannel(ADV_CHANNEL_PORT);
            this.running = new AtomicBoolean(false);
            this.magicNumber = random.nextInt(1000);
            this.gos = new HashMap<>();
            this.networks = new HashMap<>();
        }

        private LinkListener<Collection<Device>> discover = (eventType, outcome) -> {
            if (outcome.isSuccessful()) {
                onWifiNetworks(outcome.getOutcome());
            }
        };

        private void onWifiNetworks(Collection<Device> devices) {
            for (Device device : devices) {
                networks.put(device.getName(), device);
            }
        }

        private Runnable publish = () -> {
            if (!running.get())
                return;

            String[] addrs = {"192.168.1.255"};

            // is already master
            if (wdLink.isAccepting(serverId)) {
                WifiDirectCredentials credentials = wdLink.getFeatures().getWifiP2pCredentials(serverId);
                if (credentials.getSSID().equals(Constants.UNDEFINED))
                    return;
                JSONObject obj = new JSONObject();
                Objects.requireNonNull(credentials);
                try {
                    obj.put("id", NetworkService.getLocalAddress().toString());
                    obj.put("type", "master");
                    obj.put("name", "direct2");
                    obj.put("magic", magicNumber);
                    obj.put("ssid", credentials.getSSID());
                    obj.put("pass", credentials.getPassword());
                    obj.put("port", channelServer.getPort());
                    obj.put("slaves", wdLink.getConnected().size());
                    advChannel.sendMessage(obj.toString(), addrs);

                } catch (JSONException e) {
                    e.printStackTrace();
                    NetLog.e("Publish", e.getMessage());
                }

            } else if (!isConnectedToWifiDirectGroup()) { // client
                JSONObject obj = new JSONObject();
                try {
                    obj.put("id", NetworkService.getLocalAddress().toString());
                    obj.put("type", "slave");
                    obj.put("name", "direct2");
                    obj.put("magic", magicNumber);
                    advChannel.sendMessage(obj.toString(), addrs);
                } catch (JSONException e) {
                    e.printStackTrace();
                    NetLog.e("Publish", e.getMessage());
                }
            }

        };

        private Map<String, Info> undef;
        private Map<String, Pair<Integer, Long>> goSlaveCount;

        private void listen() {
            undef = new HashMap<>();
            goSlaveCount = new HashMap<>();

            this.advChannel.addPacketListener((srcIpAddress, jsonMessage) -> {
                if (!running.get())
                    return;
                try {
                    synchronized (Topology.this) {
                        JSONObject obj = new JSONObject(jsonMessage);
                        if (obj.get("name").equals("direct2") && !obj.get("id").equals(NetworkService.getLocalAddress().toString())) {
                            int conns = wdLink.getConnected().size();
                            long diff = System.currentTimeMillis() - masterIdle;
                            if (isMaster.get() && conns == 0 && diff > formationProperties.getIdleTime()) {
                                wdLink.deny(serverId).done(result -> {
                                    NetLog.w("Master", "Deny");
                                    this.isMaster.set(false);
                                });
                            }

                            int count = filterUndef();
                            // if master received
                            if (obj.get("type").equals("master")) {
                                String ssid = obj.getString("ssid");
                                String pass = obj.getString("pass");
                                int port = obj.getInt("port");
                                int slaves = obj.getInt("slaves");

                                undef.remove(srcIpAddress);
                                gos.put(ssid, new Credential(ssid, pass, port));
                                if (slaves >= formationProperties.getLimitsConnections().max) {
                                    goSlaveCount.remove(ssid);
                                } else {
                                    goSlaveCount.put(ssid, new Pair<>(slaves, System.currentTimeMillis()));
                                    List<String> list = new ArrayList<>(goSlaveCount.keySet());
                                    Credential cred = gos.get(list.get(random.nextInt(list.size())));
                                    connect(cred);
                                }

                                /*double prob;
                                if (count > 15) prob = 1 / 3;
                                else if (count > 10) prob = 1 / 2;
                                else prob = 1;*/


                            } else { // otherwise - is client
                                masterDecide(srcIpAddress, obj.getInt("magic"));
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    NetLog.e(TAG, e.getMessage());
                }
            });
            advChannel.start();
        }

        private AtomicBoolean becoming = new AtomicBoolean(false);
        private AtomicBoolean isMaster = new AtomicBoolean(false);
        private int errorTimes = 0;
        private long masterIdle = System.currentTimeMillis();

        static class Info {
            private int magic;
            private final long firstTs;
            private long currTs;

            Info(int magic, long firstTs) {
                this.magic = magic;
                this.firstTs = firstTs;
                this.currTs = firstTs;
            }
        }

        private int filterUndef() {
            Iterator<Map.Entry<String, Info>> iter = undef.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, Info> entry = iter.next();
                long diff = System.currentTimeMillis() - entry.getValue().currTs;
                if (diff >= 5000)
                    iter.remove();
            }
            Iterator<Map.Entry<String, Pair<Integer, Long>>> it = goSlaveCount.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Pair<Integer, Long>> entry = it.next();
                long diff = System.currentTimeMillis() - entry.getValue().second;
                if (diff >= 4000 || entry.getValue().first >= formationProperties.getLimitsConnections().max)
                    it.remove();
            }
            return undef.size();
        }

        private void masterDecide(String ipAddress, int magic) {
            boolean gosAvailable = goSlaveCount.size() > 0;
            if (!gosAvailable && !isConnectedToWifiDirectGroup()) {
                if (!undef.containsKey(ipAddress))
                    undef.put(ipAddress, new Info(magic, System.currentTimeMillis()));
                else {
                    undef.get(ipAddress).magic = magic;
                    undef.get(ipAddress).currTs = System.currentTimeMillis();
                }
                boolean valid = false;
                boolean best = true;
                for (Map.Entry<String, Info> entry : undef.entrySet()) {
                    long diff1 = System.currentTimeMillis() - entry.getValue().firstTs;
                    if (diff1 > 1500) valid = true;
                    if (entry.getValue().magic > this.magicNumber) best = false;
                }

                if (valid && best && becoming.compareAndSet(false, true) && !isMaster.get() && !connecting.get()) {
                    NetLog.i("Master", "Is Master");

                    // become master
                    wdLink.accept(wdLink.getFeatures().newServerBuilder().build())
                            .done(result -> {
                                NetLog.w("Master", "Success");
                                isMaster.set(true);
                                becoming.set(false);
                                masterIdle = System.currentTimeMillis();
                            })
                            .fail(result -> {
                                NetLog.e("Master fail", result.getReason().name());
                                NetLog.e("Master fail", result.getMessage());
                                becoming.set(false);
                                errorTimes += 1;
                                if (errorTimes >= 3)
                                    magicNumber = 0;
                                else
                                    magicNumber = random.nextInt(100);
                                undef.clear();
                            });
                }
            }
        }

        private AtomicBoolean connecting = new AtomicBoolean(false);

        private void connect(Credential cred) {
            Device device = networks.get(cred.ssid);
            if (device == null) {
                wifiLink.discover(wifiLink.getFeatures().newDiscoveryBuilder().build());
            }

            if (!isConnectedToWifiDirectGroup() && device != null && connecting.compareAndSet(false, true) && !becoming.get()) {
                undef.clear();
                NetLog.d("Connect", "Connecting... to " + cred.ssid);
                ConnectionProperties<WifiConnectionSettings> connectionProperties =
                        wifiLink.getFeatures().newConnectionBuilder(device)
                                .removeWhenDisconnect(true)
                                .setTimeout(10000) // 10 seconds
                                .setPassword(cred.password)
                                .build();
                LibFormation.connect(wifiLink, device, connectionProperties)
                        .done(result -> {
                            NetLog.e("Connect", "success");
                            connecting.set(false);
                        })
                        .fail(result -> {
                            NetLog.e("Connect", "fail...");
                            connecting.set(false);
                            executorService.submit(() -> reconnectTomato());
                        });
            }
        }

        private synchronized void reconnectTomato() {
            Device device = networks.get("Tomato");
            if (device != null) {
                ConnectionProperties<WifiConnectionSettings> connectionProperties =
                        wifiLink.getFeatures().newConnectionBuilder(device)
                                .removeWhenDisconnect(false)
                                .setTimeout(10000) // 10 seconds
                                .build();
                LibFormation.connect(wifiLink, device, connectionProperties);
            }
        }

        public void start() {
            if (running.compareAndSet(false, true)) {
                wifiLink.listen(LinkEvent.LINK_DISCOVERY_FOUND, discover);
                executorService.scheduleAtFixedRate(publish, 100, 1000, TimeUnit.MILLISECONDS);
                listen();
            }
        }

        public void stop() {
            if (running.compareAndSet(true, false)) {
                wifiLink.removeListener(discover);
                this.advChannel.stop();
                executorService.shutdownNow();
                wdLink.deny(serverId);
            }
        }

        public boolean isRunning() {
            return running.get();
        }

        /**
         * Verifies if the current device is connected to the Wifi Direct group.
         *
         * @return <tt>true</tt> if connect the wifi direct group, <tt>false</tt> otherwise
         */
        private boolean isConnectedToWifiDirectGroup() {
            // if a group owner - then return true
            if (isMaster.get())
                return true;
            // otherwise is a client
            Collection<Device> devices = wifiLink.getConnected();
            for (Device dev : devices) {
                if (gos.containsKey(dev.getName()))
                    return true;
            }
            return false;
        }

        /**
         * Establish a socket to Group Owner if not already established.
         *
         * @param device device to connected to
         */
        private synchronized void connectToGo(@NonNull Device device) {
            Credential credential = gos.get(device.getName());
            if (credential == null) {
                NetLog.e(TAG, "Credentials are NULL");
            } else {
                // if no socket to group owner - then create one
                if (!channelRegistry.isConnected(org.hyrax.link.misc.Constants.DEFAULT_GO_IP)) {
                    channelRegistry.createSocketChannel(
                            org.hyrax.link.misc.Constants.DEFAULT_GO_IP,
                            credential.serverListenPort, Technology.WIFI_DIRECT)
                            .fail(result -> {
                                NetLog.e(TAG, "Error establishing the socket connection");
                                wifiLink.disconnect(device)
                                        .always((state, resolved, rejected) -> {
                                            executorService.submit(() -> reconnectTomato());
                                        });
                            });
                } else {
                    NetLog.w(TAG, "Socket already connect to GO");
                }
            }
        }
    }
}

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation.wifi;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.ScanData;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiScanRecord;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.NetworkService;
import org.hyrax.network.channel.ChannelRegistry;
import org.hyrax.network.formation.FormationInit;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.formation.MeshTopology;
import org.hyrax.network.misc.Channel;
import org.hyrax.network.misc.ChannelServer;
import org.hyrax.network.misc.Limits;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.DeferredManager;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DefaultDeferredManager;
import org.jdeferred2.impl.DeferredObject;
import org.jdeferred2.multiple.OneValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

/**
 * Wifi random mesh topology implementation.
 * <p/>
 * The algorithm implements a random mesh network. The devices alternate between a visible state
 * and discover state in order to create unique communications links. E.g A channel to B then
 * B does not need another channel to A since the channel are bidirectional links.
 * In this algorithm the device cannot be at visible and discover state at same time.
 * </p>
 * This works on top of Wifi, Wifi Direct and Wifi Direct Legacy and possibly on Mobile networks.
 */
public class WifiMeshRandom implements Formation {
    private static final String TAG = "WMeshRand";
    private static final String NAME = "WifiMeshRand";
    private static final String SERVICE_NAME = "WifiMeshAuto";
    private static final String LOGIC_ADDRESS_KEY = "logic";

    private FormationInit formationInit;
    private LinkPromise<WifiLink> wifiLink;
    @Nullable
    private Topology topology;

    // wifi direct connections listener
    private boolean connRegistered;
    private final LinkListener<Device> connectionListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        Device device = outcome.getOutcome();
        switch (eventType) {
            case LINK_CONNECTION_NEW:
                NetLog.i(TAG, "Connected to Wifi " + device.getName());
                start();
                break;
            case LINK_CONNECTION_LOST:
                lightStop();
                NetLog.w(TAG, "Disconnected from Wifi " + device.getName());
                break;
        }
    };


    // wifi direct visibility listener
    private final LinkListener<String> visibilityListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_VISIBILITY_ON:
                // do nothing
                NetLog.i(TAG, "Visible");
                break;
            case LINK_VISIBILITY_OFF:
                NetLog.i(TAG, "Not Visible");
                if (topology != null)
                    topology.wakeUpStateMachine();
                break;
        }
    };

    /**
     * Constructor.
     */
    public WifiMeshRandom() {
        this.wifiLink = LinkService.getLinkPromise(Technology.WIFI);
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.WIFI_MESH_RANDOM;
    }

    @Override
    public void setFormationInit(@NonNull FormationInit formationInit) {
        this.formationInit = formationInit;
    }

    @NonNull
    @Override
    public FormationProperties getDefaultFormationProperties() {
        return FormationProperties.newBuilder()
                .setDiscoveryTimeLimits(5000, 10000)
                .setVisibilityTimeLimits(15000, 20000)
                .setConnectionLimits(20, 50)
                .setAcceptProbability(0.5)
                .setIdleTime(30000)
                .setEnableHardwareOnStart(true)
                .build();
    }

    @Override
    public synchronized void start() {
        assert formationInit != null;
        if (!connRegistered) {
            wifiLink.listen(EventCategory.LINK_CONNECTION, connectionListener);
            connRegistered = true;
        }

        boolean toEnableHw = formationInit.formationProperties.isToEnableHardwareOnStart();
        if (toEnableHw && !wifiLink.isEnabled()) {
            wifiLink.enable()
                    .done(result -> NetLog.d(TAG, "Wifi hardware enabled"))
                    .fail(result -> NetLog.e(TAG, "Enable Wifi hardware failure"));
        } else if (wifiLink.isEnabled() && (topology == null || !topology.isRunning())) {
            formationInit.channelRegistry.createSocketChannelServerIfNotExists(0, Technology.WIFI)
                    .done(server -> {
                        wifiLink.listen(EventCategory.LINK_VISIBILITY, visibilityListener);

                        topology = new Topology(wifiLink, formationInit, server);
                        topology.start(MeshTopology.State.INIT);
                    })
                    .fail(error -> NetLog.e(TAG, "Server socket start failure. " + error.getMessage()));
        }
    }

    @Override
    public void resume() {
        if (topology != null)
            topology.resume();
    }

    @Override
    public void pause() {
        if (topology != null)
            topology.pause();
    }

    /**
     * Stops the network formation but keeps some service running.
     */
    private void lightStop() {
        wifiLink.removeListener(visibilityListener);
        if (topology != null)
            topology.stop();
        topology = null;
    }

    @Override
    public synchronized void stop() {
        lightStop();
        wifiLink.removeListener(connectionListener);
        connRegistered = false;
        if (LinkService.hasLink(Technology.WIFI)) {
            LinkService.shutdownLink(Technology.WIFI);
        }
    }

    @Override
    public boolean isRunning() {
        return topology != null && topology.isRunning();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof WifiMeshRandom && formationEquals((WifiMeshRandom) obj);
    }

    @Override
    public int hashCode() {
        return formationHash();
    }

    /**
     * Mesh topology implementation.
     */
    static class Topology extends MeshTopology {
        private final int EMPTY_DISCOVER = -1;
        private final int ALL_ALREADY_CONNECTED = -2;
        private final int DEFAULT_WAIT_AFTER_SCAN = 1000; // 1 second
        private final int WAIT_MAX = 10000; // 10 seconds
        private final Random random;
        private final LinkPromise<WifiLink> wifiLink;
        private final FormationProperties formationProperties;
        private final ChannelRegistry channelRegistry;
        private final ChannelServer server;

        private final String advertiserId;
        private final String scannerId;

        private int waitAfterScan;

        /**
         * Constructor.
         *
         * @param wifiLink      the wifi link object
         * @param formationInit formation initialization object
         * @param server        channel server object
         */
        private Topology(@NonNull LinkPromise<WifiLink> wifiLink,
                         @NonNull FormationInit formationInit,
                         @NonNull ChannelServer server) {
            super(formationInit.executorService);
            this.random = new Random();
            this.wifiLink = wifiLink;
            this.formationProperties = formationInit.formationProperties;
            this.channelRegistry = formationInit.channelRegistry;
            this.server = server;
            this.advertiserId = UUID.randomUUID().toString();
            this.scannerId = UUID.randomUUID().toString();
            this.waitAfterScan = DEFAULT_WAIT_AFTER_SCAN;
        }

        @Override
        public void stop() {
            super.stop();
            wifiLink.cancelVisible(advertiserId);
            wifiLink.cancelDiscover(scannerId);
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onInit() {
            DeferredObject<State, Exception, Void> defer = new DeferredObject<>();
            int port = server.getPort();
            // if server port is invalid then returns an error
            if (port <= 0) {
                defer.reject(new Exception("Invalid listen port " + port));
                // decision to go visible
            } else if (goVisible()) {
                defer.resolve(State.ADVERTISE);
                // or go scan
            } else {
                defer.resolve(State.SCAN_AND_CONNECT);
            }

            return defer.promise();
        }

        /**
         * Tells if the device goes visible with a certain probability.
         *
         * @return <tt>true</tt> if to go visible <tt>false</tt> otherwise
         */
        private boolean goVisible() {
            // generated a random value between [0, 1]
            double rand = random.nextDouble();
            return rand < formationProperties.getAcceptProbability();
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onAdvertise() {
            DeferredObject<State, Exception, Void> defer = new DeferredObject<>();
            int port = server.getPort();
            if (port <= 0) {
                defer.reject(new Exception("Advertise Invalid listen port " + port));
                // if not visible - then go visible
            } else if (!wifiLink.isVisible(advertiserId)) {
                Limits<Integer> visL = formationProperties.getLimitsTimeVisibility();
                int time = visL.min + new Random().nextInt((visL.max - visL.min) + 1);
                VisibilityProperties<WifiAdvertiseSettings, WifiAdvertiseData> properties =
                        wifiLink.getFeatures()
                                .newVisibilityBuilder()
                                .setAdvertiseId(advertiserId)
                                .setTimeout(time)
                                .stopAfterTimeoutExpiration(true)
                                .setAdvertiserSettings(WifiAdvertiseSettings
                                        .newBuilder(SERVICE_NAME)
                                        .setPort(port)
                                        .setProtocol(Bonjour.SERVICE_TCP)
                                        .build())
                                .setAdvertiserData(new WifiAdvertiseData() {{
                                    addData(LOGIC_ADDRESS_KEY, NetworkService.getLocalAddress().toString());
                                }})
                                .build();
                LibFormation.visible(wifiLink, properties)
                        .done(result -> defer.resolve(State.END))
                        .fail(defer::reject);
            } else {
                defer.resolve(State.END);
            }

            return defer.promise();
        }

        /**
         * Applies a filter on the discovered devices services.
         *
         * @param device device discovered
         * @return @return <tt>true</tt> if the device can be used for connection, <tt>false</tt> otherwise
         */
        private boolean applyFilter(@NonNull Device device) {
            String wifiLocalIp = wifiLink.getFeatures().getIpAddress();
            String wifiDirectLocalIp = LinkService.getWifiIpAddress("p2p-wlan", LinkService.ProtocolVersion.IPV4);
            String localhost = "127.0.0.1";
            for (ScanData scanData : device.getScanData()) {
                WifiScanRecord record = (WifiScanRecord) scanData.getOriginalObject();
                String rmHost = record.getHost();
                // ignore loopback service - both wifi and wifi p2p
                if (wifiLocalIp.equals(rmHost) || wifiDirectLocalIp.equals(rmHost) || localhost.equals(rmHost))
                    return false;
            }
            return true;
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onScanAndConnect() {
            DeferredObject<State, Exception, Void> defer = new DeferredObject<>();
            Limits<Integer> disL = formationProperties.getLimitsTimeDiscovery();
            int time = disL.min + new Random().nextInt((disL.max - disL.min) + 1);
            DiscoveryProperties<Void, WifiScanFilter> properties = wifiLink.getFeatures()
                    .newDiscoveryBuilder()
                    .setScanId(scannerId)
                    .setTimeout(time)
                    .stopAfterTimeoutExpiration(true)
                    .setScannerFilter(WifiScanFilter.newBuilder()
                            .setServiceName(SERVICE_NAME)
                            .setFilter(this::applyFilter)
                            .setProtocol(Bonjour.SERVICE_TCP)
                            .build())
                    .build();
            LibFormation.discover(wifiLink, properties)
                    .then(this::connectMultiple)
                    .done(connResult -> {
                        switch (connResult) {
                            /*
                              If the discovery is empty or all discovered device are connected -
                              then double the wait time after scan
                             */
                            case EMPTY_DISCOVER:
                            case ALL_ALREADY_CONNECTED:
                                waitAfterScan = waitAfterScan * 2;
                                break;

                            // means that some connections were performed - restart the wait time after scan
                            default:
                                waitAfterScan = DEFAULT_WAIT_AFTER_SCAN;
                        }
                        defer.resolve(State.END);
                    })
                    .fail(defer::reject);
            return defer.promise();
        }

        /**
         * Connects to multiple devices.
         *
         * @param devices a collection of devices to connect to
         * @return a promise object indicating how many devices were successfully connected.
         * <p>The negative values or zero represent error codes.</p>
         * <ul>
         * <li>0 : (All connections attempt failed)</li>
         * <li>-1: (The collection of devices is empty)</li>
         * <li>-2: (All discovered devices are already connected)</li>
         * </ul>
         */
        private Promise<Integer, Exception, Void> connectMultiple(@NonNull Collection<Device> devices) {
            // if discovery is empty return empty discover
            if (devices.isEmpty())
                return new DeferredObject<Integer, Exception, Void>().resolve(EMPTY_DISCOVER);

            List<Promise<Channel, Exception, Void>> list = new ArrayList<>();
            DeferredManager dm = new DefaultDeferredManager();

            for (Device dev : devices) {
                for (ScanData scanData : dev.getScanData()) {
                    WifiScanRecord record = (WifiScanRecord) scanData.getOriginalObject();
                    boolean connected = channelRegistry.isConnected(record.getHost());
                    NetLog.i("Found", String.format(Locale.ENGLISH,
                            "Dev: %s Host: %s Port: %d || Connected: %s",
                            dev.getName(), record.getHost(), record.getPort(),
                            Boolean.toString(connected)));

                    if (!connected) {
                        list.add(channelRegistry.createSocketChannel(
                                record.getHost(), record.getPort(), Technology.WIFI));
                    }
                }
            }
            // if list is empty means the all discovered devices are already connected.
            if (list.isEmpty())
                return new DeferredObject<Integer, Exception, Void>().resolve(ALL_ALREADY_CONNECTED);

            // else go connect the devices that are not connected yet
            DeferredObject<Integer, Exception, Void> defer = new DeferredObject<>();
            dm.settle(list)
                    .done(connResults -> {
                        int connected = 0;
                        for (OneValue value : connResults) {
                            if (value.getValue() instanceof Channel)
                                connected++;
                        }
                        if (connected == 0)
                            defer.reject(new Exception("All connections attempts have failed"));
                        else
                            defer.resolve(connected);
                    })
                    .fail(result -> defer.reject(new Exception(result)));

            return defer.promise();
        }

        @Override
        protected long getWaitTime() {
            switch (getCurrState()) {
                case ADVERTISE:
                    waitAfterScan = DEFAULT_WAIT_AFTER_SCAN;
                    return WAIT_MAX;
                case SCAN_AND_CONNECT:
                    return (waitAfterScan > WAIT_MAX) ? WAIT_MAX : waitAfterScan;
                default:
                    throw new NetworkRuntimeException("Unexpected state " + getCurrState());
            }
        }
    }
}

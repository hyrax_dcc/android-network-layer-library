/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation.wifi;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.misc.Bonjour;
import org.hyrax.link.misc.ScanData;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.link.wifi.WifiDirectLink;
import org.hyrax.link.wifi.WifiLink;
import org.hyrax.link.wifi.direct.WifiDirectCredentials;
import org.hyrax.link.wifi.types.ScanDataWifi;
import org.hyrax.link.wifi.types.WifiAdvertiseData;
import org.hyrax.link.wifi.types.WifiAdvertiseSettings;
import org.hyrax.link.wifi.types.WifiConnectionSettings;
import org.hyrax.link.wifi.types.WifiScanFilter;
import org.hyrax.link.wifi.types.WifiScanRecord;
import org.hyrax.link.wifi.types.WifiServerSettings;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.channel.ChannelRegistry;
import org.hyrax.network.formation.FormationInit;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.formation.StarTopology;
import org.hyrax.network.misc.ChannelServer;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.Limits;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.DonePipe;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Wifi Direct Legacy star topology implementation.
 * </p>
 * Only the group owner is able to participate on a normal wifi network and wifi p2p group,
 * the remaining devices (clients) can only be at the group.
 * <p/>
 * The algorithm implements a simple star network, where one device will dynamically act as
 * a master(group owner) of the network and the other will connect to it.
 * <p/>
 * Note, this algorithm only creates isolated groups, then communication is
 * just performed inside the groups.
 */
public class WifiDirectLegacyStar implements Formation {
    private static final String TAG = "WDLegStar";
    private static final String NAME = "WifiDirectLegacyStar";

    private FormationInit formationInit;
    // wifi direct link
    private LinkPromise<WifiDirectLink> wdLink;
    // wifi link
    private final LinkPromise<WifiLink> wifiLink;
    @Nullable
    private Topology topology;

    // wifi hardware listener
    private boolean hwRegistered;
    private final LinkListener<Boolean> hwListener = ((eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_HARDWARE_ON:
                start();
                break;
            case LINK_HARDWARE_OFF:
                lightStop();
                break;
        }
    });

    // wifi direct connections listener
    private final LinkListener<Device> connectionListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        Device device = outcome.getOutcome();
        switch (eventType) {
            case LINK_CONNECTION_NEW:
                if (topology == null)
                    return;
                Log.w("Connect", "Toto " + device);
                boolean isOnGroup = topology.isConnectedToWifiDirectGroup();
                boolean isGo = wdLink.isAccepting(topology.serverId);
                NetLog.i(TAG, String.format(Locale.ENGLISH,
                        "New Connection %s -> %s || OnGroup: %s || isGo: %s", device.getName(),
                        device.getUniqueAddress(), Boolean.toString(isOnGroup), Boolean.toString(isGo)));
                // if is a group peer - then establish a socket to the group owner
                if (isOnGroup && !isGo) {
                    // connects socket to group owner
                    topology.connectToGo(device);
                }
                break;
            case LINK_CONNECTION_LOST:
                if (topology != null) {
                    topology.masterIdleTime = System.currentTimeMillis();
                    topology.wakeUpStateMachine();
                }

                NetLog.w(TAG, "Lost Connection " + device.getUniqueAddress());
                break;
        }
    };


    // wifi direct visibility listener
    private final LinkListener<String> visibilityListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_VISIBILITY_ON:
                // do nothing
                break;
            case LINK_VISIBILITY_OFF:
                if (topology != null)
                    topology.wakeUpStateMachine();
                break;
        }
    };

    public WifiDirectLegacyStar() {
        this.wdLink = LinkService.getLinkPromise(Technology.WIFI_DIRECT);
        this.wifiLink = LinkService.getLinkPromise(Technology.WIFI);
        LibFormation.listenAndRecordEvents(wdLink);
        LibFormation.listenAndRecordEvents(wifiLink);
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.WIFI_DIRECT_LEGACY_STAR;
    }

    @Override
    public void setFormationInit(@NonNull FormationInit formationInit) {
        this.formationInit = formationInit;
    }

    @NonNull
    @Override
    public FormationProperties getDefaultFormationProperties() {
        return FormationProperties.newBuilder()
                .setDiscoveryTimeLimits(4000, 6000)
                .setVisibilityTimeLimits(15000, 20000)
                .setConnectionLimits(3, 5)
                .setAcceptProbability((double) 1 / 4)
                .setIdleTime(40000)
                .setEnableHardwareOnStart(true)
                .build();
    }

    @Override
    public synchronized void start() {
        assert formationInit != null;
        if (!hwRegistered) {
            wdLink.listen(EventCategory.LINK_HARDWARE, hwListener);
            hwRegistered = true;
        }

        boolean toEnableHw = formationInit.formationProperties.isToEnableHardwareOnStart();
        if (toEnableHw && !wdLink.isEnabled()) {
            wdLink.enable()
                    .done(result -> NetLog.d(TAG, "Wifi hardware enabled"))
                    .fail(result -> NetLog.e(TAG, "Enable Wifi hardware failure"));
        } else if (wdLink.isEnabled() && (topology == null || !topology.isRunning())) {
            formationInit.channelRegistry.createSocketChannelServerIfNotExists(0, Technology.WIFI_DIRECT)
                    .done(server -> {
                        wdLink.listen(EventCategory.LINK_VISIBILITY, visibilityListener);
                        wdLink.listen(EventCategory.LINK_CONNECTION, connectionListener);
                        wifiLink.listen(EventCategory.LINK_CONNECTION, connectionListener);

                        topology = new Topology(wdLink, wifiLink, formationInit, server);
                        topology.start(StarTopology.State.INIT);
                    })
                    .fail(error -> NetLog.e(TAG, "Network Start failure. " + error.getMessage()));
        }
    }

    /**
     * Stops the network formation but keeps some service running.
     */
    private void lightStop() {
        wdLink.removeListener(visibilityListener);
        wdLink.removeListener(connectionListener);
        wifiLink.removeListener(connectionListener);

        if (topology != null)
            topology.stop();
        topology = null;
    }

    @Override
    public void resume() {
        if (topology != null)
            topology.resume();
    }

    @Override
    public void pause() {
        if (topology != null)
            topology.pause();
    }

    @Override
    public synchronized void stop() {
        lightStop();
        wdLink.removeListener(hwListener);
        hwRegistered = false;
        if (formationInit.formationProperties.isToEnableHardwareOnStart())
            wdLink.disable();
        if (LinkService.hasLink(Technology.WIFI_DIRECT)) {
            LinkService.shutdownLink(Technology.WIFI_DIRECT);
        }
        LibFormation.stopListenEvents(wdLink);
        LibFormation.stopListenEvents(wifiLink);
    }

    @Override
    public boolean isRunning() {
        return topology != null && topology.isRunning();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof WifiDirectLegacyStar && formationEquals((WifiDirectLegacyStar) obj);
    }

    @Override
    public int hashCode() {
        return formationHash();
    }

    private static class Topology extends StarTopology {
        private static final String SERVICE_NAME = "WifiDirectLegacyAuto";
        private static final String NETWORK_SSID_KEY = "k_ssid";
        private static final String NETWORK_PASS_KEY = "k_pass";
        private static final String SERVER_LISTEN_PORT_KEY = "k_port";
        private static final int WAIT_MAX = 20000; // 20 seconds

        private final LinkPromise<WifiDirectLink> wdLink;
        private final LinkPromise<WifiLink> wifiLink;
        private final FormationProperties formationProperties;
        private final ChannelRegistry channelRegistry;
        private final ChannelServer channelServer;

        private final Random random;
        private int discoverAttempts;
        private long masterIdleTime;

        private final String serverId;
        private final String advertiserId;
        private final String scannerId;

        private final Map<Device, Credential> discoveredDevices;

        /**
         * Constructor.
         *
         * @param wdLink        wifi direct link object
         * @param wifiLink      wifi link object
         * @param formationInit initialization object
         * @param channelServer socket server object
         */
        private Topology(@NonNull LinkPromise<WifiDirectLink> wdLink,
                         @NonNull LinkPromise<WifiLink> wifiLink,
                         @NonNull FormationInit formationInit,
                         @NonNull ChannelServer channelServer) {
            super(formationInit.executorService);
            this.wdLink = wdLink;
            this.wifiLink = wifiLink;
            this.formationProperties = formationInit.formationProperties;
            this.channelRegistry = formationInit.channelRegistry;
            this.channelServer = channelServer;

            this.random = new Random();
            this.discoverAttempts = 0;
            this.masterIdleTime = System.currentTimeMillis();

            this.serverId = wdLink.getFeatures().newServerBuilder().build().getIdentifier();
            this.advertiserId = UUID.randomUUID().toString();
            this.scannerId = UUID.randomUUID().toString();
            this.discoveredDevices = new HashMap<>();
        }

        @Override
        public void stop() {
            super.stop();
            wdLink.deny(serverId);
            wdLink.cancelVisible(advertiserId);
            wdLink.cancelDiscover(scannerId);
            discoveredDevices.clear();
        }

        /**
         * Returns the visibility properties configurations for the current server.
         *
         * @return visibility properties object
         */
        @NonNull
        private VisibilityProperties getVisibilityProperties() {
            WifiDirectCredentials credentials = wdLink.getFeatures().getWifiP2pCredentials(serverId);
            if (org.hyrax.link.misc.Constants.UNDEFINED.equals(credentials.getSSID())) {
                throw new NetworkRuntimeException("Undefined server info");
            }
            Limits<Integer> visL = formationProperties.getLimitsTimeVisibility();
            int time = visL.min + new Random().nextInt((visL.max - visL.min) + 1);
            return wdLink.getFeatures().newVisibilityBuilder()
                    .setAdvertiseId(advertiserId)
                    .stopAfterTimeoutExpiration(true)
                    .setTimeout(time)
                    .setAdvertiserSettings(WifiAdvertiseSettings.newBuilder(SERVICE_NAME)
                            .setProtocol(Bonjour.PRESENCE_TCP)
                            .forceToGoDiscovery(false)
                            .build())
                    .setAdvertiserData(new WifiAdvertiseData() {{
                        addData(NETWORK_SSID_KEY, credentials.getSSID());
                        addData(NETWORK_PASS_KEY, credentials.getPassword());
                        addData(SERVER_LISTEN_PORT_KEY, Integer.toString(channelServer.getPort()));
                    }})
                    .build();
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onRoleMaster() {
            int port = channelServer.getPort();
            if (port <= 0) {
                return new DeferredObject<State, Exception, Void>()
                        .reject(new Exception("Advertise Invalid listen port " + port));
            }
            DeferredObject<StarTopology.State, Exception, Void> defer = new DeferredObject<>();
            ServerProperties<WifiServerSettings> serverProperties = wdLink.getFeatures()
                    .newServerBuilder()
                    .build();
            Limits<Integer> connL = formationProperties.getLimitsConnections();
            boolean isAccepting = wdLink.isAccepting(serverId);
            boolean isVisible = wdLink.isVisible(advertiserId);
            int connSize = wdLink.getConnected().size();

            NetLog.w("Clients", "" + connSize);
            // if not accepting then becomes master
            if (!isAccepting) {
                masterIdleTime = System.currentTimeMillis();
                LibFormation.acceptConnections(wdLink, serverProperties)
                        .then((DonePipe<Boolean, Boolean, Exception, Void>) result ->
                                LibFormation.visible(wdLink, getVisibilityProperties()))
                        .done(result -> defer.resolve(State.END))
                        .fail(defer::reject);
                // if the master supports more clients - then continue to be visible
            } else if (!isVisible && connSize < connL.max) {
                LibFormation.visible(wdLink, getVisibilityProperties())
                        .done(result -> defer.resolve(State.END))
                        .fail(defer::reject);
            } else {
                defer.resolve(State.END);
            }
            return defer.promise();
        }

        /**
         * Verifies if the discovered device has the network pin set.
         *
         * @param device remote device
         * @return <tt>true</tt> if the device can be used for connection, <tt>false</tt> otherwise
         */
        private boolean isNetworkValid(@NonNull Device device) {
            String ssid = getScanDataKey(device.getScanData(), NETWORK_SSID_KEY);
            String pass = getScanDataKey(device.getScanData(), NETWORK_PASS_KEY);
            String serverPort = getScanDataKey(device.getScanData(), SERVER_LISTEN_PORT_KEY);

            boolean valid = !Constants.UNDEFINED.equals(ssid) && !Constants.UNDEFINED.equals(pass) &&
                    !Constants.UNDEFINED.equals(serverPort);
            if (valid)
                discoveredDevices.put(device, new Credential(ssid, pass, Integer.valueOf(serverPort)));
            return valid;
        }

        /**
         * Returns the scan value for a specific key.
         *
         * @param scanData scan data collection
         * @param key      data key
         * @return returns the value of the key, or {@value Constants#UNDEFINED} if not found
         */
        @NonNull
        private String getScanDataKey
        (@NonNull Collection<ScanData> scanData, @NonNull String key) {
            for (ScanData data : scanData) {
                WifiScanRecord record = ((ScanDataWifi) data).getOriginalObject();
                if (record.getExtraData().containsKey(key))
                    return record.getExtraData().get(key);
            }
            return Constants.UNDEFINED;
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onRoleClient() {
            DeferredObject<StarTopology.State, Exception, Void> defer = new DeferredObject<>();
            // if client already connect go to end state
            if (isConnectedToWifiDirectGroup()) {
                defer.resolve(State.END);
                // checks GO socket connect and connects to it if needed
                connectToGo(wifiLink.getConnected().iterator().next());
            } else {
                Limits<Integer> disL = formationProperties.getLimitsTimeDiscovery();
                int time = disL.min + new Random().nextInt((disL.max - disL.min) + 1);
                DiscoveryProperties<Void, WifiScanFilter> discoveryProperties = wdLink.getFeatures()
                        .newDiscoveryBuilder()
                        .setScanId(scannerId)
                        .setTimeout(time)
                        .setScannerFilter(WifiScanFilter.newBuilder()
                                .setServiceName(SERVICE_NAME)
                                .setProtocol(Bonjour.PRESENCE_TCP)
                                .setFilter(this::isNetworkValid)
                                .build())
                        .setStopRule(devices -> devices.size() > 0)
                        .stopAfterTimeoutExpiration(true)
                        .build();

                discoverAttempts++;
                LibFormation.discover(wdLink, discoveryProperties)
                        .then((DonePipe<Collection<Device>, Device, Exception, Void>) devices -> {
                            Device wdDevice = null;
                            Credential credential = null;
                            for (Device dev : devices) {
                                wdDevice = dev;
                                credential = discoveredDevices.get(dev);
                                if (credential != null) break;
                            }
                            if (credential != null) {
                                Device wifiDevice = wifiLink.getFeatures().toWifiNode(wdDevice, credential.ssid);
                                ConnectionProperties<WifiConnectionSettings> connectionProperties =
                                        wifiLink.getFeatures().newConnectionBuilder(wifiDevice)
                                                .removeWhenDisconnect(true)
                                                .setTimeout(10000) // 10 seconds
                                                .setPassword(credential.password)
                                                .build();

                                return LibFormation.connect(wifiLink, wifiDevice, connectionProperties);
                            }
                            return new DeferredObject<Device, Exception, Void>()
                                    .reject(new Exception("No devices discovered"));
                        })
                        .done(result -> {
                            discoverAttempts = 0;
                            defer.resolve(State.END);
                        })
                        .fail(defer::reject);
            }
            return defer.promise();
        }

        @NonNull
        @Override
        protected Promise<Role, Exception, Void> onWhichRole() {
            DeferredObject<Role, Exception, Void> defer = new DeferredObject<>();
            switch (getCurrRole()) {
                // if neither client or master a decision must be made
                case UNDEFINED:
                    discoverAttempts = 0;
                    // destroy the active server, advertiser and scanner
                    wdLink.deny(serverId)
                            .always((state, resolved, rejected) -> {
                                return wdLink.cancelVisible(advertiserId);
                            })
                            /*.always((state, resolved, rejected) -> {
                                return wdLink.cancelDiscover(scannerId);
                            })*/
                            .always((state, resolved, rejected) -> {
                                defer.resolve(getNextRole());
                            });
                    break;

                case CLIENT:
                    // if connect to wifi direct group - then do nothing
                    if (isConnectedToWifiDirectGroup()) defer.resolve(Role.CLIENT);
                    else defer.resolve(getNextRole());
                    break;

                case MASTER:
                    discoverAttempts = 0;
                    int connSize = wdLink.getConnected().size();
                    long timeDiff = System.currentTimeMillis() - masterIdleTime;
                    if (connSize == 0 && timeDiff > formationProperties.getIdleTime()) {
                        LibFormation.destroyMaster(wdLink, serverId, advertiserId)
                                .done(result -> defer.resolve(getNextRole()));
                    } else {
                        defer.resolve(Role.MASTER);
                    }
                    break;
            }
            return defer.promise();
        }

        /**
         * Turns the visibility off when reach clients capacity.
         */
        void turnVisibleOff() {
            int lm = formationProperties.getLimitsConnections().max;
            if (wdLink.getConnected().size() >= lm && wdLink.isVisible(advertiserId))
                wdLink.cancelVisible(advertiserId);
        }

        @Override
        protected long getWaitTime() {
            switch (getCurrRole()) {
                case MASTER:
                    return WAIT_MAX;
                case CLIENT:
                    return WAIT_MAX;
                default:
                    throw new NetworkRuntimeException("Undefined role " + getCurrRole());
            }
        }

        /**
         * Verifies if the current device is connected to the Wifi Direct group.
         *
         * @return <tt>true</tt> if connect the wifi direct group, <tt>false</tt> otherwise
         */
        private boolean isConnectedToWifiDirectGroup() {
            // if a group owner - then return true
            if (wdLink.isAccepting(serverId))
                return true;
            // otherwise is a client
            Collection<Device> devices = wifiLink.getConnected();
            for (Device dev : devices) {
                if (getCredentialBySsid(dev) != null)
                    return true;
            }
            return false;
        }

        /**
         * Establish a socket to Group Owner if not already established.
         *
         * @param device device to connected to
         */
        private void connectToGo(@NonNull Device device) {
            Credential credential = getCredentialBySsid(device);
            if (credential == null) {
                NetLog.e(TAG, "Credentials are NULL");
            } else {
                // if no socket to group owner - then create one
                if (!channelRegistry.isConnected(org.hyrax.link.misc.Constants.DEFAULT_GO_IP)) {
                    channelRegistry.createSocketChannel(
                            org.hyrax.link.misc.Constants.DEFAULT_GO_IP,
                            credential.serverListenPort, Technology.WIFI_DIRECT)
                            .fail(result -> NetLog.e(TAG, "Error establishing the socket connection"));
                } else {
                    NetLog.w(TAG, "Socket already connect to GO");
                }
            }
        }

        /**
         * Looks for a credential object given a device. Searches by device name (SSID).
         *
         * @param device wifi device - WifiNode
         * @return the wifi credential object or NULL if credentials not found
         */
        @Nullable
        private Credential getCredentialBySsid(@NonNull Device device) {
            for (Credential cred : discoveredDevices.values()) {
                if (cred.ssid.equals(device.getName()))
                    return cred;
            }
            return null;
        }

        /**
         * Returns the next role.
         *
         * @return next role
         */
        @NonNull
        private StarTopology.Role getNextRole() {
            // generated a random value between [0, 1]
            double rand = random.nextDouble();
            // threshold to become a master - the first time the device will go always to become client
            double threshold = formationProperties.getAcceptProbability() * discoverAttempts;
            return (rand > threshold) ? Role.CLIENT : Role.MASTER;
        }
    }
}

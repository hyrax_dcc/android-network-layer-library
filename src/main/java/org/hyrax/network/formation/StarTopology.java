/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.Promise;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Generic Star topology class.
 */
public abstract class StarTopology {
    private static final String TAG = "StarTop";
    private static final int FAIL_DELAY = 3000; // 3 seconds

    private final ScheduledExecutorService executorService;
    private final AtomicBoolean running;
    private final AtomicBoolean paused;
    private Role currRole;
    private State currState;
    @Nullable
    private ScheduledFuture schFuture;

    /**
     * Enum class to identify the device role.
     */
    public enum Role {
        UNDEFINED,
        MASTER,
        CLIENT
    }

    /**
     * Enum class to represent the device state.
     */
    public enum State {
        INIT,
        ROLE_MASTER,
        ROLE_CLIENT,
        END,
    }

    /**
     * Constructor.
     *
     * @param executorService thread pool executor
     */
    protected StarTopology(@NonNull ScheduledExecutorService executorService) {
        this.executorService = executorService;
        this.running = new AtomicBoolean(false);
        this.paused = new AtomicBoolean(false);
        this.currRole = Role.UNDEFINED;
    }

    /**
     * Starts the network formation.
     *
     * @param state initial state
     */
    public void start(@NonNull State state) {
        if (running.compareAndSet(false, true)) {
            executorService.execute(() -> execute(state));
        }
    }

    /**
     * Resumes the network formation.
     */
    public void resume() {
        if (paused.compareAndSet(true, false)) {
            wakeUpStateMachine();
        }
    }

    /**
     * Pauses the network formation.
     */
    public void pause() {
        if (!paused.compareAndSet(false, true))
            NetLog.d(TAG, "Already paused");
    }


    /**
     * Stops the network formation.
     */
    public void stop() {
        running.set(false);
    }

    /**
     * Returns if the algorithm is running
     *
     * @return <tt>true</tt> if running <tt>false</tt> otherwise
     */
    public boolean isRunning() {
        return running.get();
    }

    /**
     * Returns the current role.
     *
     * @return enum role object
     */
    @NonNull
    protected Role getCurrRole() {
        return currRole;
    }

    /**
     * Executes the next state.
     *
     * @param state state to be executed
     */
    private void execute(@NonNull State state) {
        if (running.get()) {
            //If the algorithm has been paused and the current state is END - then interrupts the execution
            if (paused.get() && currState == State.END)
                return;

            switch (state) {
                case INIT:
                    execWhichRole();
                    break;
                case ROLE_MASTER:
                    execRoleMaster();
                    currRole = Role.MASTER;
                    break;
                case ROLE_CLIENT:
                    execRoleClient();
                    currRole = Role.CLIENT;
                    break;
                case END:
                    execEnd();
                    break;
                default:
                    throw new NetworkRuntimeException("Undefined state " + state);
            }
        }
        currState = state;
    }

    /**
     * Executes the master role.
     */
    private void execRoleMaster() {
        StarTopology that = this;
        onRoleMaster()
                .done(state -> that.executorService.execute(() -> that.execute(state)))
                .fail(error -> {
                    NetLog.e(TAG, error.getMessage());
                    currRole = Role.UNDEFINED;
                    that.executorService.schedule(() -> that.execute(State.INIT),
                            FAIL_DELAY, TimeUnit.MILLISECONDS);
                });
    }

    /**
     * Master role implementation.
     *
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    protected abstract Promise<State, Exception, Void> onRoleMaster();

    /**
     * Executes the client role.
     */
    private void execRoleClient() {
        StarTopology that = this;
        onRoleClient()
                .done(state -> that.executorService.execute(() -> that.execute(state)))
                .fail(error -> {
                    NetLog.e(TAG, error.getMessage());
                    that.executorService.schedule(() -> that.execute(State.INIT),
                            FAIL_DELAY, TimeUnit.MILLISECONDS);
                });
    }

    /**
     * Client role implementation.
     *
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    protected abstract Promise<State, Exception, Void> onRoleClient();

    /**
     * Decides which role will be played.
     */
    private void execWhichRole() {
        StarTopology that = this;
        onWhichRole()
                // call if success
                .done(role -> {
                    switch (role) {
                        case MASTER:
                            that.executorService.execute(() -> that.execute(State.ROLE_MASTER));
                            break;
                        case CLIENT:
                            that.executorService.execute(() -> that.execute(State.ROLE_CLIENT));
                            break;
                        case UNDEFINED:
                            that.executorService.execute(() -> that.execute(State.INIT));
                            break;
                        default:
                            throw new NetworkRuntimeException("Undefined role " + role);
                    }
                    NetLog.d(TAG, "Role " + role);
                })
                // call if error
                .fail(result -> {
                    NetLog.e(TAG, result.getMessage());
                    that.executorService.schedule(
                            () -> that.execute(State.INIT),
                            FAIL_DELAY, TimeUnit.MILLISECONDS);
                });
    }

    /**
     * Which role implementation.
     *
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    protected abstract Promise<Role, Exception, Void> onWhichRole();

    /**
     * Executes the final state.
     */
    private void execEnd() {
        long time = getWaitTime();
        if (time > 0) {
            if (schFuture != null && !schFuture.isDone()) {
                schFuture.cancel(false);
            }
            schFuture = executorService.schedule(
                    this::wakeUpStateMachine, time, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * Returns the amount of time the device must be on end state
     * or a negative number to wait forever.
     *
     * @return long time in milliseconds
     */
    protected abstract long getWaitTime();

    /**
     * Wakes up the state machine.
     */
    public void wakeUpStateMachine() {
        if (currState == State.END) {
            executorService.execute(() -> execute(State.INIT));
        }
    }
}

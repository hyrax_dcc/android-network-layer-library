/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation;

import android.support.annotation.NonNull;

import org.hyrax.network.FormationProperties;
import org.hyrax.network.channel.ChannelRegistry;

import java.util.concurrent.ScheduledExecutorService;

/**
 * Formation initialization object
 */
public class FormationInit {
    @NonNull
    public final FormationProperties formationProperties;
    @NonNull
    public final ChannelRegistry channelRegistry;
    @NonNull
    public final ScheduledExecutorService executorService;

    /**
     * Constructor.
     *
     * @param formationProperties formation properties object
     * @param channelRegistry     channel registry object
     * @param executorService     executor service object
     */
    public FormationInit(@NonNull FormationProperties formationProperties,
                         @NonNull ChannelRegistry channelRegistry,
                         @NonNull ScheduledExecutorService executorService) {
        this.formationProperties = formationProperties;
        this.channelRegistry = channelRegistry;
        this.executorService = executorService;
    }
}

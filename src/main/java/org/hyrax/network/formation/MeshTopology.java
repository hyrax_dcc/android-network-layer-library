/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.Promise;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Generic mesh topology class.
 */
public abstract class MeshTopology {
    private static final String TAG = "MeshTop";
    private static final int FAIL_DELAY = 2000; // 2 second

    private final ScheduledExecutorService executorService;
    private final AtomicBoolean running;
    private final AtomicBoolean paused;
    private State currState;
    @Nullable
    private ScheduledFuture schFuture;

    /**
     * Enum class to represent the device state.
     */
    public enum State {
        INIT,
        ADVERTISE,
        SCAN_AND_CONNECT,
        END,
    }

    /**
     * Constructor.
     *
     * @param executorService thread pool executor
     */
    protected MeshTopology(@NonNull ScheduledExecutorService executorService) {
        this.executorService = executorService;
        this.running = new AtomicBoolean(false);
        this.paused = new AtomicBoolean(false);
    }

    /**
     * Starts the network formation.
     *
     * @param state initial state
     */
    public void start(@NonNull State state) {
        if (running.compareAndSet(false, true)) {
            executorService.execute(() -> execute(state));
        }
    }

    /**
     * Resumes the network formation.
     */
    public void resume() {
        if (paused.compareAndSet(true, false)) {
            wakeUpStateMachine();
        }
    }

    /**
     * Pauses the network formation.
     */
    public void pause() {
        if (!paused.compareAndSet(false, true))
            NetLog.d(TAG, "Already paused");
    }

    /**
     * Stops the network formation.
     */
    public void stop() {
        running.set(false);
    }

    /**
     * Returns if the algorithm is running
     *
     * @return <tt>true</tt> if running <tt>false</tt> otherwise
     */
    public boolean isRunning() {
        return running.get();
    }

    /**
     * Return the current state.
     *
     * @return state enum object
     */
    @NonNull
    protected State getCurrState() {
        return currState;
    }

    /**
     * Executes the next state.
     *
     * @param state state to be executed
     */
    private void execute(@NonNull State state) {
        if (running.get()) {
            //If the algorithm has been paused and the current state is END - then interrupts the execution
            if (paused.get() && currState == State.END)
                return;

            switch (state) {
                case INIT:
                    execInit();
                    break;
                case ADVERTISE:
                    execAdvertise();
                    NetLog.d(TAG, "Advertising ...");
                    break;
                case SCAN_AND_CONNECT:
                    execScanAndConnect();
                    NetLog.d(TAG, "Scanning ...");
                    break;
                case END:
                    execEnd();
                    break;
                default:
                    throw new NetworkRuntimeException("Undefined state " + state);
            }
        }
        currState = state;
    }

    /**
     * Executes the initial state of state machine.
     * </p>
     * Goes to advertise state.
     */
    private void execInit() {
        MeshTopology that = this;
        onInit()
                .done(state -> that.executorService.execute(() -> that.execute(state)))
                .fail(error -> {
                    NetLog.e(TAG, error.getMessage());
                    that.executorService.schedule(() -> that.execute(State.INIT),
                            FAIL_DELAY, TimeUnit.MILLISECONDS);
                });
    }

    /**
     * Init implementation
     *
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    protected abstract Promise<State, Exception, Void> onInit();

    /**
     * Executes the advertise state.
     */
    private void execAdvertise() {
        MeshTopology that = this;
        onAdvertise()
                .done(state -> that.executorService.execute(() -> that.execute(state)))
                .fail(error -> {
                    NetLog.e(TAG, error.getMessage());
                    that.executorService.schedule(() -> that.execute(State.INIT),
                            FAIL_DELAY, TimeUnit.MILLISECONDS);
                });
    }

    /**
     * Advertise implementation.
     *
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    protected abstract Promise<State, Exception, Void> onAdvertise();

    /**
     * Executes the scan and connect state.
     */
    private void execScanAndConnect() {
        MeshTopology that = this;
        onScanAndConnect()
                .done(state -> that.executorService.execute(() -> that.execute(state)))
                .fail(error -> {
                    NetLog.e(TAG, error.getMessage());
                    that.executorService.schedule(() -> that.execute(State.INIT),
                            FAIL_DELAY, TimeUnit.MILLISECONDS);
                });
    }

    /**
     * Finds and connects to new peers.
     *
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    protected abstract Promise<State, Exception, Void> onScanAndConnect();

    /**
     * Executes the end state.
     */
    private void execEnd() {
        long time = getWaitTime();
        if (time > 0) {
            if (schFuture != null && !schFuture.isDone()) {
                schFuture.cancel(false);
            }
            schFuture = executorService.schedule(
                    this::wakeUpStateMachine, time, TimeUnit.MILLISECONDS);
        }
    }

    /**
     * Returns the amount of time the device must be on end state
     * or a negative number to wait forever.
     *
     * @return long time in milliseconds
     */
    protected abstract long getWaitTime();

    /**
     * Wakes up the state machine.
     */
    public void wakeUpStateMachine() {
        if (currState == State.END) {
            executorService.execute(() -> execute(State.INIT));
        }
    }
}

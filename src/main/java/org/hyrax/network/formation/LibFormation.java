/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation;

import android.support.annotation.NonNull;

import org.hyrax.link.Device;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.misc.exception.LinkException;
import org.hyrax.link.misc.link.Link;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.network.Formation;
import org.hyrax.network.formation.bluetooth.BluetoothMeshRandom;
import org.hyrax.network.formation.bluetooth.BluetoothStar;
import org.hyrax.network.formation.wifi.WifiDirectLegacyStar;
import org.hyrax.network.formation.wifi.WifiDirectLegacyStar2;
import org.hyrax.network.formation.wifi.WifiDirectStar;
import org.hyrax.network.formation.wifi.WifiMeshRandom;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.AlwaysPipe;
import org.jdeferred2.DoneCallback;
import org.jdeferred2.FailCallback;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Helper class responsible for executing some generic formation actions.
 */
public class LibFormation {

    /**
     * Constructs a network topology algorithm from a type.
     *
     * @param type formation enum type
     * @return a routing object
     * @throws NetworkRuntimeException if type is not taken care
     */
    @NonNull
    public static Formation factory(@NonNull Formation.Type type) {
        switch (type) {
            case BLUETOOTH_STAR:
                return new BluetoothStar();
            case BLUETOOTH_MESH_RANDOM:
                return new BluetoothMeshRandom();
            case WIFI_DIRECT_STAR:
                return new WifiDirectStar();
            case WIFI_DIRECT_LEGACY_STAR:
                return new WifiDirectLegacyStar();
            case WIFI_DIRECT_LEGACY_STAR_2:
                return new WifiDirectLegacyStar2();
            case WIFI_MESH_RANDOM:
                return new WifiMeshRandom();
            default:
                throw new NetworkRuntimeException("Undefined routing type " + type);
        }
    }

    /**
     * Disables the device visibility and the device connections acceptance.
     *
     * @param linkPromise a link object
     * @param serverId    server identifier
     * @param visibleId   advertiser identifier
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    public static Promise<Boolean, Exception, Void> destroyMaster(@NonNull LinkPromise linkPromise,
                                                                  @NonNull String serverId,
                                                                  @NonNull String visibleId) {
        NetLog.d("LibForm", "Destroy Master");
        DeferredObject<Boolean, Exception, Void> defer = new DeferredObject<>();

        linkPromise.cancelVisible(visibleId)
                .always((state, resolved, rejected) -> {
                    return linkPromise.deny(serverId);
                })
                .always((state, resolved, rejected) -> {
                    defer.resolve(true);
                });
        return defer.promise();
    }

    /**
     * Tells the device to accept connections.
     *
     * @param linkPromise      a link object
     * @param serverProperties server properties
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    public static Promise<Boolean, Exception, Void> acceptConnections(@NonNull LinkPromise linkPromise,
                                                                      @NonNull ServerProperties serverProperties) {
        DeferredObject<Boolean, Exception, Void> defer = new DeferredObject<>();
        linkPromise.accept(serverProperties)
                .done((DoneCallback<String>) id -> defer.resolve(true))
                .fail((FailCallback<LinkException>) defer::reject);
        return defer.promise();
    }

    /**
     * Turns the  device visible to other.
     *
     * @param linkPromise          a link object
     * @param visibilityProperties visibility properties
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    public static Promise<Boolean, Exception, Void> visible(@NonNull LinkPromise linkPromise,
                                                            @NonNull VisibilityProperties visibilityProperties) {
        DeferredObject<Boolean, Exception, Void> defer = new DeferredObject<>();
        linkPromise.setVisible(visibilityProperties, true)
                .done((DoneCallback<String>) id -> defer.resolve(true))
                .fail((FailCallback<LinkException>) defer::reject);
        return defer.promise();
    }

    /**
     * Performs a discovery and returns a promise with the devices discovered.
     *
     * @param linkPromise a link object
     * @param properties  discovery properties
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    public static Promise<Collection<Device>, Exception, Void> discover(@NonNull LinkPromise linkPromise,
                                                                        @NonNull DiscoveryProperties properties) {
        DeferredObject<Collection<Device>, Exception, Void> defer = new DeferredObject<>();
        linkPromise.cancelDiscover(properties.getIdentifier())
                .always((AlwaysPipe<String, LinkException, Collection<Device>, LinkException, Void>)
                        (state, resolved, rejected) -> linkPromise.discover(properties))
                .done((DoneCallback<Collection<Device>>) defer::resolve)
                .fail((FailCallback<LinkException>) error -> defer.reject(new Exception(error)));
        return defer.promise();
    }

    /**
     * Performs a connection to a remote device.
     *
     * @param linkPromise a link object
     * @param device      device to connect to
     * @param properties  connection properties
     * @return a promise object meaning the success or failure of the operation
     */
    @NonNull
    public static Promise<Device, Exception, Void> connect(@NonNull LinkPromise linkPromise,
                                                           @NonNull Device device,
                                                           @NonNull ConnectionProperties properties) {
        NetLog.log(NetLog.LogTag.CONNECTION_ATTEMPT, linkPromise.getTechnology(), device.getUniqueAddress());
        return linkPromise.connect(device, properties);
    }

    private static Map<Link, LinkListener> listenMap;

    /**
     * Listen and logs the link layer events.
     *
     * @param link a link object
     */
    public static void listenAndRecordEvents(@NonNull Link link) {
        if (listenMap == null)
            listenMap = new HashMap<>();
        if (!listenMap.containsKey(link)) {
            listenMap.put(link, (eventType, outcome) -> {
                boolean success = outcome.isSuccessful();
                switch (eventType) {
                    case LINK_HARDWARE_ON:
                    case LINK_HARDWARE_OFF:
                    case LINK_DISCOVERY_FOUND:
                    case LINK_DISCOVERY_DONE:
                    case LINK_VISIBILITY_DONE:
                    case LINK_CONNECTION_SERVER_INFO:
                    case LINK_CONNECTION_SERVER_NEW_CLIENT:
                    case LINK_DETACH:
                        break; // do nothing
                    case LINK_DISCOVERY_ON:
                        NetLog.log(NetLog.LogTag.DISCOVERY_ON, link.getTechnology(), success);
                        break;
                    case LINK_DISCOVERY_OFF:
                        NetLog.log(NetLog.LogTag.DISCOVERY_OFF, link.getTechnology(), success);
                        break;
                    case LINK_VISIBILITY_ON:
                        NetLog.log(NetLog.LogTag.VISIBLE_ON, link.getTechnology(), success);
                        break;
                    case LINK_VISIBILITY_OFF:
                        NetLog.log(NetLog.LogTag.VISIBLE_OFF, link.getTechnology(), success);
                        break;
                    case LINK_CONNECTION_SERVER_ON:
                        NetLog.log(NetLog.LogTag.SERVER_ON, link.getTechnology(), success);
                        break;
                    case LINK_CONNECTION_SERVER_OFF:
                        NetLog.log(NetLog.LogTag.SERVER_OFF, link.getTechnology(), success);
                        break;
                    case LINK_CONNECTION_NEW:
                        Device device = (Device) outcome.getOutcome();
                        NetLog.log(NetLog.LogTag.CONNECTION_NEW, link.getTechnology(), success, device.getUniqueAddress());
                        break;
                    case LINK_CONNECTION_LOST:
                        device = (Device) outcome.getOutcome();
                        NetLog.log(NetLog.LogTag.CONNECTION_LOST, link.getTechnology(), success, device.getUniqueAddress());
                        break;
                }
            });
            link.listen(listenMap.get(link));
        } else
            throw new NetworkRuntimeException("Link already mapped " + link.getTechnology());
    }

    /**
     * Unregisters the listeners
     *
     * @param link a link object
     */
    public static void stopListenEvents(@NonNull Link link) {
        if (listenMap != null) {
            LinkListener listener = listenMap.remove(link);
            if (listener != null)
                link.removeListener(listener);
            if (listenMap.size() == 0)
                listenMap = null;
        }
    }
}

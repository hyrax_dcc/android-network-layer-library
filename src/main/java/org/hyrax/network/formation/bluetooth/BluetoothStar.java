/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation.bluetooth;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.bluetooth.BluetoothLink;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.formation.FormationInit;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.formation.StarTopology;
import org.hyrax.network.misc.Limits;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.DonePipe;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DeferredObject;

import java.util.Collection;
import java.util.Random;

/**
 * Bluetooth star topology implementation.
 * <p/>
 * The algorithm implements a simple star network, where one device will dynamically act as
 * a master of the network and the other will connect to it.
 * <p/>
 * Note, this algorithm only creates isolated groups, then communication is
 * just performed inside the groups.
 */
public class BluetoothStar implements Formation {
    private static final String TAG = "BthStar";
    private static final String NAME = "BluetoothStar";

    private FormationInit formationInit;
    private LinkPromise<BluetoothLink> bluetoothLink;
    @Nullable
    private Topology topology;

    // Bluetooth hardware listener
    private boolean hwRegistered;
    private final LinkListener<Boolean> hwListener = ((eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_HARDWARE_ON:
                start();
                break;
            case LINK_HARDWARE_OFF:
                lightStop();
                break;
        }
    });

    // Bluetooth connections listener
    private final LinkListener<Device> connectionListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        Device device = outcome.getOutcome();
        switch (eventType) {
            case LINK_CONNECTION_NEW:
                BluetoothComm comm = bluetoothLink.getFeatures().getCommLink(device);
                NetLog.assertNotNull(comm);
                if (comm != null) {
                    formationInit.channelRegistry.createBluetoothChannel(device, comm)
                            .done(result -> topology.turnVisibleOff())
                            .fail(error -> NetLog.e(TAG, "Error creating bluetooth channel " + error.getMessage()));
                }
                break;
            case LINK_CONNECTION_LOST:
                if (topology != null) {
                    topology.masterIdleTime = System.currentTimeMillis();
                    topology.wakeUpStateMachine();
                }
                break;
        }
    };


    // Bluetooth visibility listener
    private final LinkListener<String> visibilityListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_VISIBILITY_ON:
                // do nothing
                break;
            case LINK_VISIBILITY_OFF:
                if (topology != null)
                    topology.wakeUpStateMachine();
                break;
        }
    };

    /**
     * Constructor.
     */
    public BluetoothStar() {
        this.bluetoothLink = LinkService.getLinkPromise(Technology.BLUETOOTH);
        LibFormation.listenAndRecordEvents(bluetoothLink);
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.BLUETOOTH_STAR;
    }

    @Override
    public void setFormationInit(@NonNull FormationInit formationInit) {
        this.formationInit = formationInit;
    }

    @NonNull
    @Override
    public FormationProperties getDefaultFormationProperties() {
        return FormationProperties.newBuilder()
                .setDiscoveryTimeLimits(4000, 6000)
                .setVisibilityTimeLimits(10000, 15000)
                .setConnectionLimits(3, 7)
                .setAcceptProbability((double) 1 / 7)
                .setIdleTime(30000)
                .setEnableHardwareOnStart(true)
                .build();
    }

    @Override
    public synchronized void start() {
        assert formationInit != null;

        if (!hwRegistered) {
            bluetoothLink.listen(EventCategory.LINK_HARDWARE, hwListener);
            hwRegistered = true;
        }

        boolean toEnableHw = formationInit.formationProperties.isToEnableHardwareOnStart();
        if (toEnableHw && !bluetoothLink.isEnabled()) {
            bluetoothLink.enable()
                    .done(result -> NetLog.d(TAG, "Bluetooth hardware enabled"))
                    .fail(result -> NetLog.e(TAG, "Enable Bluetooth hardware failure"));
        } else if (bluetoothLink.isEnabled() && (topology == null || !topology.isRunning())) {
            bluetoothLink.listen(EventCategory.LINK_CONNECTION, connectionListener);
            bluetoothLink.listen(EventCategory.LINK_VISIBILITY, visibilityListener);

            topology = new Topology(bluetoothLink, formationInit);
            topology.start(StarTopology.State.INIT);
        }
    }

    @Override
    public void resume() {
        if (topology != null)
            topology.resume();
    }

    @Override
    public void pause() {
        if (topology != null)
            topology.pause();
    }

    /**
     * Stops the network formation but keeps some service running.
     */
    private void lightStop() {
        bluetoothLink.removeListener(connectionListener);
        bluetoothLink.removeListener(visibilityListener);

        if (topology != null) {
            topology.resetPrefix();
            topology.stop();
        }
        topology = null;
    }

    @Override
    public synchronized void stop() {
        lightStop();
        bluetoothLink.removeListener(hwListener);
        hwRegistered = false;
        if (formationInit.formationProperties.isToEnableHardwareOnStart())
            bluetoothLink.disable();
        if (LinkService.hasLink(Technology.BLUETOOTH)) {
            LinkService.shutdownLink(Technology.BLUETOOTH);
        }
        LibFormation.stopListenEvents(bluetoothLink);
    }

    @Override
    public boolean isRunning() {
        return topology != null && topology.isRunning();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof BluetoothStar && formationEquals((BluetoothStar) obj);
    }

    @Override
    public int hashCode() {
        return formationHash();
    }

    /**
     * Star topology.
     */
    private static class Topology extends StarTopology {
        private static final String PREFIX_MASTER = "HYRAX_M_";
        private static final int WAIT_MAX = 20000; // 20 seconds

        private final LinkPromise<BluetoothLink> bthLink;
        private final FormationProperties formationProperties;
        private final Random random;
        private int discoverAttempts;
        private long masterIdleTime;

        private final String serverId;
        private final String advertiserId;
        private final String scannerId;

        /**
         * Constructor.
         *
         * @param bthLink       bluetooth link object
         * @param formationInit formation initialization object
         */
        private Topology(@NonNull LinkPromise<BluetoothLink> bthLink,
                         @NonNull FormationInit formationInit) {
            super(formationInit.executorService);
            this.bthLink = bthLink;
            this.formationProperties = formationInit.formationProperties;
            this.random = new Random();
            this.discoverAttempts = 0;
            this.masterIdleTime = System.currentTimeMillis();

            this.serverId = bthLink.getFeatures().newServerBuilder().build().getIdentifier();
            this.advertiserId = bthLink.getFeatures().newVisibilityBuilder().build().getIdentifier();
            this.scannerId = bthLink.getFeatures().newDiscoveryBuilder().build().getIdentifier();
        }

        @Override
        public void stop() {
            super.stop();
            bthLink.deny(serverId);
            bthLink.cancelVisible(advertiserId);
            bthLink.cancelDiscover(scannerId);
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onRoleMaster() {
            ServerProperties serverProperties = bthLink.getFeatures()
                    .newServerBuilder().build();

            Limits<Integer> visL = formationProperties.getLimitsTimeVisibility();
            int time = visL.min + new Random().nextInt((visL.max - visL.min) + 1);
            VisibilityProperties visibilityProperties = bthLink.getFeatures()
                    .newVisibilityBuilder()
                    .setTimeout(time)
                    .stopAfterTimeoutExpiration(true)
                    .build();

            DeferredObject<StarTopology.State, Exception, Void> defer = new DeferredObject<>();
            Limits<Integer> connL = formationProperties.getLimitsConnections();
            boolean isAccepting = bthLink.isAccepting(serverId);
            boolean isVisible = bthLink.isVisible(advertiserId);
            int connSize = bthLink.getConnected().size();

            long timeDiff = System.currentTimeMillis() - masterIdleTime;
            // if not accepting then becomes master
            if (!isAccepting) {
                if (setPrefix(PREFIX_MASTER)) {
                    masterIdleTime = System.currentTimeMillis();
                    LibFormation.acceptConnections(bthLink, serverProperties)
                            .then((DonePipe<Boolean, Boolean, Exception, Void>) result ->
                                    LibFormation.visible(bthLink, visibilityProperties))
                            .done(result -> defer.resolve(State.END))
                            .fail(defer::reject);
                } else {
                    defer.reject(new Exception("Error setting the prefix"));
                }

                // if master has been idle for long time - then restart the algorithm
            } else if (connSize == 0 && timeDiff > formationProperties.getIdleTime()) {
                LibFormation.destroyMaster(bthLink, serverId, advertiserId)
                        .done(result -> defer.resolve(State.INIT));
                // if the master supports more client - then continue to be visible
            } else if (!isVisible && connSize < connL.max) {
                LibFormation.visible(bthLink, visibilityProperties)
                        .done(result -> defer.resolve(State.END))
                        .fail(defer::reject);
            } else {
                defer.resolve(State.END);
            }
            return defer.promise();
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onRoleClient() {
            //resets the name prefix
            resetPrefix();

            DeferredObject<StarTopology.State, Exception, Void> defer = new DeferredObject<>();
            int connections = bthLink.getConnected().size();
            // if client already connect go to end state
            if (connections > 0) {
                defer.resolve(State.END);
            } else {
                Limits<Integer> disL = formationProperties.getLimitsTimeDiscovery();
                int time = disL.min + new Random().nextInt((disL.max - disL.min) + 1);
                DiscoveryProperties<Void, Filter<Device>> discoveryProperties = bthLink.getFeatures()
                        .newDiscoveryBuilder()
                        .setTimeout(time)
                        .setScannerFilter(device -> device.getName().startsWith(PREFIX_MASTER))
                        .setStopRule(devices -> devices.size() > 0)
                        .stopAfterTimeoutExpiration(true).build();

                discoverAttempts++;

                LibFormation.discover(bthLink, discoveryProperties)
                        .then((DonePipe<Collection<Device>, Device, Exception, Void>) devices -> {

                            if (devices.size() > 0) {
                                Device device = devices.iterator().next();
                                ConnectionProperties connectionProperties = bthLink.getFeatures()
                                        .newConnectionBuilder(device)
                                        .setTimeout(10000) // 10 seconds
                                        .build();
                                return LibFormation.connect(bthLink, device, connectionProperties);
                            }
                            return new DeferredObject<Device, Exception, Void>()
                                    .reject(new Exception("No devices discovered"));
                        })
                        .done(result -> {
                            discoverAttempts = 0;
                            defer.resolve(State.END);
                        })
                        .fail(defer::reject);
            }
            return defer.promise();
        }

        @NonNull
        @Override
        protected Promise<StarTopology.Role, Exception, Void> onWhichRole() {
            DeferredObject<StarTopology.Role, Exception, Void> defer = new DeferredObject<>();
            switch (getCurrRole()) {
                // if neither client or master a decision must be made
                case UNDEFINED:
                    discoverAttempts = 0;
                    // destroy the active server, advertiser and scanner
                    bthLink.deny(serverId)
                            .always((state, resolved, rejected) -> {
                                return bthLink.cancelVisible(advertiserId);
                            })
                            .always((state, resolved, rejected) -> {
                                return bthLink.cancelDiscover(scannerId);
                            })
                            .always((state, resolved, rejected) -> {
                                defer.resolve(getNextRole());
                            });
                    break;

                case CLIENT:
                    int connections = bthLink.getConnected().size();
                    // if connected continue as CLIENT
                    if (connections > 0)
                        defer.resolve(Role.CLIENT);
                    else
                        defer.resolve(getNextRole());
                    break;

                case MASTER:
                    discoverAttempts = 0;
                    int connSize = bthLink.getConnected().size();
                    long timeDiff = System.currentTimeMillis() - masterIdleTime;
                    if (connSize == 0 && timeDiff > formationProperties.getIdleTime()) {
                        LibFormation.destroyMaster(bthLink, serverId, advertiserId)
                                .done(result -> defer.resolve(getNextRole()));
                    } else {
                        defer.resolve(Role.MASTER);
                    }

                    break;
            }
            return defer.promise();
        }

        /**
         * Turns the visibility off when reach clients capacity.
         */
        void turnVisibleOff() {
            int lm = formationProperties.getLimitsConnections().max;
            if (bthLink.getConnected().size() >= (lm - 1) && bthLink.isVisible(advertiserId))
                bthLink.cancelVisible(advertiserId);
        }

        @Override
        protected long getWaitTime() {
            switch (getCurrRole()) {
                case MASTER:
                    return WAIT_MAX;
                case CLIENT:
                    return WAIT_MAX;
                default:
                    throw new NetworkRuntimeException("Undefined role " + getCurrRole());
            }
        }

        /**
         * Returns the next role.
         *
         * @return next role
         */
        @NonNull
        private StarTopology.Role getNextRole() {
            // generated a random value between [0, 1]
            double rand = random.nextDouble();
            // threshold to become a master - the first time the device will go always to become client
            double threshold = formationProperties.getAcceptProbability() * discoverAttempts;
            return (rand > threshold) ? Role.CLIENT : Role.MASTER;
        }

        /**
         * Verifies if the prefix name is valid.
         *
         * @param name prefix name
         * @return -1 if no valid prefix or prefix length
         */
        private int hasValidPrefix(@NonNull String name) {
            if (name.startsWith(PREFIX_MASTER))
                return PREFIX_MASTER.length();
            return -1;
        }

        /**
         * Changes the current bluetooth device name.
         *
         * @param prefix prefix name to be added
         * @return <tt>true</tt> if changed, <tt>false</tt> otherwise
         */
        private boolean setPrefix(@NonNull String prefix) {
            String name = bthLink.getFeatures().getName();
            if (!Constants.UNDEFINED.equals(name)) {
                int idx = hasValidPrefix(name);
                // if prefix already set
                if (idx > 0 && PREFIX_MASTER.equals(prefix)) {
                    return true;
                }
                if (idx > 0) {
                    name = name.substring(idx);
                }
                int tries = 3;
                boolean set;
                do {
                    set = bthLink.getFeatures().setName(prefix + name);
                } while (!set && --tries > 0);

                return set;
            }
            return false;
        }

        /**
         * Resets the bluetooth name prefix.
         */
        private void resetPrefix() {
            setPrefix("");
        }
    }
}

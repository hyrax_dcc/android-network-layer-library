/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.formation.bluetooth;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.hyrax.link.Device;
import org.hyrax.link.EventCategory;
import org.hyrax.link.LinkListener;
import org.hyrax.link.LinkPromise;
import org.hyrax.link.LinkService;
import org.hyrax.link.Technology;
import org.hyrax.link.bluetooth.BluetoothLink;
import org.hyrax.link.bluetooth.legacy.comm.BluetoothComm;
import org.hyrax.link.misc.Constants;
import org.hyrax.link.misc.Filter;
import org.hyrax.link.misc.properties.ConnectionProperties;
import org.hyrax.link.misc.properties.DiscoveryProperties;
import org.hyrax.link.misc.properties.ServerProperties;
import org.hyrax.link.misc.properties.VisibilityProperties;
import org.hyrax.network.Formation;
import org.hyrax.network.FormationProperties;
import org.hyrax.network.channel.ChannelRegistry;
import org.hyrax.network.formation.FormationInit;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.formation.MeshTopology;
import org.hyrax.network.misc.Limits;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.jdeferred2.DeferredManager;
import org.jdeferred2.DonePipe;
import org.jdeferred2.Promise;
import org.jdeferred2.impl.DefaultDeferredManager;
import org.jdeferred2.impl.DeferredObject;
import org.jdeferred2.multiple.OneValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Bluetooth random mesh topology implementation.
 * <p/>
 * The algorithm implements a random mesh network. The devices alternate between a visible state
 * and discover state in order to create unique communications links. E.g A channel to B then
 * B does not need another channel to A since the channel are bidirectional links.
 * In this algorithm the device cannot be at visible and discover state at same time.
 */
public class BluetoothMeshRandom implements Formation {
    private static final String TAG = "BthMeshRand";
    private static final String NAME = "BluetoothMeshRand";

    private FormationInit formationInit;
    private LinkPromise<BluetoothLink> bluetoothLink;
    @Nullable
    private Topology topology;

    // Bluetooth hardware listener
    private boolean hwRegistered;
    private final LinkListener<Boolean> hwListener = ((eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_HARDWARE_ON:
                start();
                break;
            case LINK_HARDWARE_OFF:
                lightStop();
                break;
        }
    });

    // Bluetooth connections listener
    private final LinkListener<Device> connectionListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        Device device = outcome.getOutcome();
        switch (eventType) {
            case LINK_CONNECTION_NEW:
                BluetoothComm comm = bluetoothLink.getFeatures().getCommLink(device);
                if (comm != null) {
                    formationInit.channelRegistry.createBluetoothChannel(device, comm)
                            .fail(error -> NetLog.e(TAG, "Error creating bluetooth channel " + error.getMessage()));
                } else {
                    NetLog.w(TAG, "" + bluetoothLink.getConnected());
                    NetLog.e(TAG, "Bluetooth Comm NULL " + device);
                }
                break;
            case LINK_CONNECTION_LOST:
                if (topology != null) {
                    topology.idleTime = System.currentTimeMillis();
                    topology.wakeUpStateMachine();
                }
                break;
        }
    };


    // Bluetooth visibility listener
    private final LinkListener<String> visibilityListener = (eventType, outcome) -> {
        if (!outcome.isSuccessful()) {
            NetLog.e(TAG, outcome.getError().getMessage());
            return;
        }
        switch (eventType) {
            case LINK_VISIBILITY_ON:
                // do nothing
                break;
            case LINK_VISIBILITY_OFF:
                /*if (topology != null)
                    topology.wakeUpStateMachine();*/
                break;
        }
    };

    /**
     * Constructor.
     */
    public BluetoothMeshRandom() {
        this.bluetoothLink = LinkService.getLinkPromise(Technology.BLUETOOTH);
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.BLUETOOTH_MESH_RANDOM;
    }

    @Override
    public void setFormationInit(@NonNull FormationInit formationInit) {
        this.formationInit = formationInit;
    }

    @NonNull
    @Override
    public FormationProperties getDefaultFormationProperties() {
        return FormationProperties.newBuilder()
                .setDiscoveryTimeLimits(4000, 6000)
                .setVisibilityTimeLimits(10000, 15000)
                .setConnectionLimits(2, 5) // 2,5
                .setAcceptProbability(0.3)
                .setIdleTime(15000) // 30000
                .setEnableHardwareOnStart(true)
                .build();
    }

    @Override
    public synchronized void start() {
        assert formationInit != null;

        if (!hwRegistered) {
            bluetoothLink.listen(EventCategory.LINK_HARDWARE, hwListener);
            hwRegistered = true;
        }

        boolean toEnableHw = formationInit.formationProperties.isToEnableHardwareOnStart();
        if (toEnableHw && !bluetoothLink.isEnabled()) {
            bluetoothLink.enable()
                    .done(result -> NetLog.d(TAG, "Bluetooth hardware enabled"))
                    .fail(result -> NetLog.e(TAG, "Enable Bluetooth hardware failure"));
        } else if (bluetoothLink.isEnabled() && (topology == null || !topology.isRunning())) {
            bluetoothLink.listen(EventCategory.LINK_CONNECTION, connectionListener);
            bluetoothLink.listen(EventCategory.LINK_VISIBILITY, visibilityListener);

            topology = new Topology(bluetoothLink, formationInit);
            topology.start(MeshTopology.State.INIT);
        }
    }

    /**
     * Stops the network formation but keeps some service running.
     */
    private void lightStop() {
        bluetoothLink.removeListener(connectionListener);
        bluetoothLink.removeListener(visibilityListener);

        if (topology != null) {
            topology.stop();
        }
        topology = null;
    }

    @Override
    public synchronized void stop() {
        lightStop();
        bluetoothLink.removeListener(hwListener);
        hwRegistered = false;
        if (LinkService.hasLink(Technology.BLUETOOTH)) {
            LinkService.shutdownLink(Technology.BLUETOOTH);
        }
    }

    @Override
    public void resume() {
        if (topology != null)
            topology.resume();
    }

    @Override
    public void pause() {
        if (topology != null)
            topology.pause();
    }

    @Override
    public boolean isRunning() {
        return topology != null && topology.isRunning();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof BluetoothMeshRandom && formationEquals((BluetoothMeshRandom) obj);
    }

    @Override
    public int hashCode() {
        return formationHash();
    }

    /**
     * Mesh topology implementation.
     */
    private static class Topology extends MeshTopology {
        private static final String PREFIX_ADVERTISER = "HYRAX_ADV_";
        private static final int EMPTY_DISCOVER = -1;
        private static final int ALL_ALREADY_CONNECTED = -2;
        private static final int DEFAULT_WAIT_TIME = 1000; // 1 second
        private static final int WAIT_MAX = 20000; // 20 seconds

        private final Random random;
        private final LinkPromise<BluetoothLink> bthLink;
        private final FormationProperties formationProperties;
        private final ChannelRegistry channelRegistry;

        private final String serverId;
        private final String advertiserId;
        private final String scannerId;

        private long idleTime;
        private int waitTime;

        private Topology(@NonNull LinkPromise<BluetoothLink> bthLink,
                         @NonNull FormationInit formationInit) {
            super(formationInit.executorService);
            this.random = new Random();
            this.bthLink = bthLink;
            this.formationProperties = formationInit.formationProperties;
            this.channelRegistry = formationInit.channelRegistry;

            this.serverId = bthLink.getFeatures().newServerBuilder().build().getIdentifier();
            this.advertiserId = bthLink.getFeatures().newVisibilityBuilder().build().getIdentifier();
            this.scannerId = bthLink.getFeatures().newDiscoveryBuilder().build().getIdentifier();

            this.idleTime = System.currentTimeMillis();
            this.waitTime = DEFAULT_WAIT_TIME;

            bthLink.listen(EventCategory.LINK_CONNECTION_SERVER, (LinkListener<String>) (eventType, outcome) -> {
                switch (eventType) {
                    case LINK_CONNECTION_SERVER_ON:
                        NetLog.d(TAG, "Server on ...");
                        break;

                    case LINK_CONNECTION_SERVER_OFF:
                        NetLog.w(TAG, "Server OFF");
                        break;
                }
            });
        }

        @Override
        public void stop() {
            super.stop();
            resetPrefix();
            bthLink.deny(serverId);
            bthLink.cancelVisible(advertiserId);
            bthLink.cancelDiscover(scannerId);
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onInit() {
            DeferredObject<State, Exception, Void> defer = new DeferredObject<>();
            int connections = bthLink.getConnected().size();
            Limits<Integer> connL = formationProperties.getLimitsConnections();

            // if the number of connections reach the limit - then do nothing
            if (connections >= connL.max) {
                defer.resolve(State.END);
                /*If the number connection has reach the minimum then go visible
                or if decide to go visible.*/
            } else if (connections >= connL.min || goVisible()) {
                defer.resolve(State.ADVERTISE);
                // otherwise go scan for peers
            } else {
                defer.resolve(State.SCAN_AND_CONNECT);
            }
            return defer.promise();
        }

        /**
         * Tells if the device goes visible with a certain probability.
         *
         * @return <tt>true</tt> if to go visible <tt>false</tt> otherwise
         */
        private boolean goVisible() {
            // generated a random value between [0, 1]
            double rand = random.nextDouble();
            return rand < formationProperties.getAcceptProbability();
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onAdvertise() {
            DeferredObject<State, Exception, Void> defer = new DeferredObject<>();
            ServerProperties serverProperties = bthLink.getFeatures().newServerBuilder().build();

            Limits<Integer> visL = formationProperties.getLimitsTimeVisibility();
            int time = visL.min + new Random().nextInt((visL.max - visL.min) + 1);
            VisibilityProperties visibilityProperties = bthLink.getFeatures().newVisibilityBuilder()
                    .setTimeout(time)
                    .stopAfterTimeoutExpiration(true)
                    .build();

            if (!bthLink.isAccepting(serverId)) {
                if (setPrefix(PREFIX_ADVERTISER)) {
                    idleTime = System.currentTimeMillis();
                    LibFormation.acceptConnections(bthLink, serverProperties)
                            .then((DonePipe<Boolean, Boolean, Exception, Void>) result ->
                                    LibFormation.visible(bthLink, visibilityProperties))
                            .done(result -> defer.resolve(State.END))
                            .fail(defer::reject);
                } else {
                    defer.reject(new Exception("Error setting the prefix"));
                }
            } else if (!bthLink.isVisible(advertiserId)) {
                LibFormation.visible(bthLink, visibilityProperties)
                        .done(result -> defer.resolve(State.END))
                        .fail(defer::reject);
            } else {
                defer.resolve(State.END);
            }
            return defer.promise();
        }

        @NonNull
        @Override
        protected Promise<State, Exception, Void> onScanAndConnect() {
            DeferredObject<State, Exception, Void> defer = new DeferredObject<>();

            Limits<Integer> disL = formationProperties.getLimitsTimeDiscovery();
            int time = disL.min + new Random().nextInt((disL.max - disL.min) + 1);
            DiscoveryProperties<Void, Filter<Device>> discoveryProperties = bthLink.getFeatures()
                    .newDiscoveryBuilder()
                    .setTimeout(time)
                    .setScannerFilter(device -> device.getName().startsWith(PREFIX_ADVERTISER) &&
                            !channelRegistry.isConnected(device.getUniqueAddress()))
                    .setStopRule(devices -> devices.size() > 0)
                    .stopAfterTimeoutExpiration(true)
                    .build();

            LibFormation.discover(bthLink, discoveryProperties)
                    .then(this::connect)
                    .done(connResult -> {
                        switch (connResult) {
                            /*
                              If the discovery is empty or all discovered device are connected -
                              then double the wait time after scan
                             */
                            case EMPTY_DISCOVER:
                            case ALL_ALREADY_CONNECTED:
                                waitTime = waitTime * 2;
                                break;

                            // means that some connections were performed - restart the wait time after scan
                            default:
                                waitTime = DEFAULT_WAIT_TIME;
                        }
                        defer.resolve(State.END);
                    })
                    .fail(defer::reject);
            return defer.promise();
        }

        /**
         * Connects to one of the device in the collection.
         *
         * @param devices a collection of devices to connect to
         * @return a promise object indicating how many devices were successfully connected.
         * <p>The negative values or zero represent error codes.</p>
         * <ul>
         * <li>0 : (All connections attempt failed)</li>
         * <li>-1: (The collection of devices is empty)</li>
         * <li>-2: (All discovered devices are already connected)</li>
         * </ul>
         */
        @NonNull
        private Promise<Integer, Exception, Void> connect(@NonNull Collection<Device> devices) {
            // if discovery is empty return empty discover
            if (devices.isEmpty())
                return new DeferredObject<Integer, Exception, Void>().resolve(EMPTY_DISCOVER);

            List<Promise<Device, Exception, Void>> list = new ArrayList<>();
            DeferredManager dm = new DefaultDeferredManager();

            for (Device dev : devices) {
                boolean connected = channelRegistry.isConnected(dev.getUniqueAddress());
                NetLog.i("Found", String.format(Locale.ENGLISH,
                        "Device: %s || Connected: %s",
                        dev.getUniqueAddress(),
                        Boolean.toString(connected)));
                if (!connected) {
                    ConnectionProperties connectionProperties = bthLink.getFeatures()
                            .newConnectionBuilder(dev)
                            .setTimeout(10000) // 10 seconds
                            .build();
                    list.add(LibFormation.connect(bthLink, dev, connectionProperties));
                }
            }
            // if list is empty means the all discovered devices are already connected.
            if (list.isEmpty())
                return new DeferredObject<Integer, Exception, Void>().resolve(ALL_ALREADY_CONNECTED);

            // else go connect the devices that are not connected yet
            DeferredObject<Integer, Exception, Void> defer = new DeferredObject<>();
            dm.settle(list)
                    .done(connResults -> {
                        int connected = 0;
                        for (OneValue value : connResults) {
                            if (value.getValue() instanceof Device)
                                connected++;
                        }
                        if (connected == 0)
                            defer.reject(new Exception("All connections attempts have failed"));
                        else
                            defer.resolve(connected);
                    })
                    .fail(result -> defer.reject(new Exception(result)));

            return defer.promise();
        }

        @Override
        protected long getWaitTime() {
            switch (getCurrState()) {
                case INIT:
                    return WAIT_MAX;
                case ADVERTISE:
                    waitTime = DEFAULT_WAIT_TIME;
                    return WAIT_MAX;
                case SCAN_AND_CONNECT:
                    return (waitTime > WAIT_MAX) ? WAIT_MAX : waitTime;
                default:
                    throw new NetworkRuntimeException("Unexpected state " + getCurrState());
            }
        }

        /**
         * Verifies if the prefix name is valid.
         *
         * @param name prefix name
         * @return -1 if no valid prefix or prefix length
         */
        private int hasValidPrefix(@NonNull String name) {
            if (name.startsWith(PREFIX_ADVERTISER))
                return PREFIX_ADVERTISER.length();
            return -1;
        }

        /**
         * Changes the current bluetooth device name.
         *
         * @param prefix prefix name to be added
         * @return <tt>true</tt> if changed, <tt>false</tt> otherwise
         */
        private boolean setPrefix(@NonNull String prefix) {
            String name = bthLink.getFeatures().getName();
            if (!Constants.UNDEFINED.equals(name)) {
                int idx = hasValidPrefix(name);
                // if prefix already set
                if (idx > 0 && PREFIX_ADVERTISER.equals(prefix)) {
                    return true;
                }
                if (idx > 0) {
                    name = name.substring(idx);
                }
                int tries = 3;
                boolean set;
                do {
                    set = bthLink.getFeatures().setName(prefix + name);
                } while (!set && --tries > 0);

                return set;
            }
            return false;
        }

        /**
         * Resets the bluetooth name prefix.
         */
        private void resetPrefix() {
            setPrefix("");
        }
    }
}

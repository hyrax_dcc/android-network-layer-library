/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.NonNull;

import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.Limits;
import org.hyrax.network.misc.NetLog;

import java.util.Locale;

/**
 * Network formation properties object.
 */
public final class FormationProperties {
    private final Limits<Integer> limitsTimeDiscovery;
    private final Limits<Integer> limitsTimeVisibility;
    private final Limits<Integer> limitsConnections;
    private final double acceptProbability;
    private final int idleTime;
    private final boolean enableHardwareOnStart;

    /**
     * Constructor.
     *
     * @param builder formation properties builder
     */
    FormationProperties(@NonNull Builder builder) {
        this.limitsTimeDiscovery = builder.limitsTimeDiscovery;
        this.limitsTimeVisibility = builder.limitsTimeVisibility;
        this.limitsConnections = builder.limitsConnections;
        this.acceptProbability = builder.acceptProbability;
        this.idleTime = builder.idleTime;
        this.enableHardwareOnStart = builder.enableHardwareOnStart;
    }

    /**
     * Returns the time discovery limits (min, max).
     *
     * @return limits object
     */
    public Limits<Integer> getLimitsTimeDiscovery() {
        return limitsTimeDiscovery;
    }

    /**
     * Returns the time visibility limits (min, max).
     *
     * @return limits object
     */
    public Limits<Integer> getLimitsTimeVisibility() {
        return limitsTimeVisibility;
    }

    /**
     * Returns the connections limits (min, max).
     *
     * @return limits object
     */
    public Limits<Integer> getLimitsConnections() {
        return limitsConnections;
    }

    /**
     * Returns the client acceptance probability.
     *
     * @return double value between [0,1]
     */
    public double getAcceptProbability() {
        return acceptProbability;
    }

    /**
     * Returns the device idle time.
     * <p/>
     * This means the amount of time that a node can be doing nothing. Usually after this time
     * the device changes roles.
     *
     * @return int value
     */
    public int getIdleTime() {
        return idleTime;
    }

    /**
     * Returns a boolean value for the formation algorithm to knew if must enable the hardware
     * in case its off.
     *
     * @return <tt>true</tt> if to enable, <tt>false</tt> otherwise
     */
    public boolean isToEnableHardwareOnStart() {
        return enableHardwareOnStart;
    }

    /**
     * Instantiates a builder object in order to defined read-only parameters.
     *
     * @return a {@link Builder} object
     */
    public static Builder newBuilder() {
        return new Builder();
    }

    /**
     * Instantiates a builder object in order to defined read-only parameters.
     *
     * @param properties formation properties already set.
     * @return a {@link Builder} object
     */
    public static Builder newBuilder(@NonNull FormationProperties properties) {
        return new Builder(properties);
    }

    /**
     * Class responsible for building and defining the {@link FormationProperties} parameters
     */
    public static final class Builder {
        private Limits<Integer> limitsTimeDiscovery;
        private Limits<Integer> limitsTimeVisibility;
        private Limits<Integer> limitsConnections;
        private double acceptProbability;
        private int idleTime;
        private boolean enableHardwareOnStart;

        /**
         * Constructor.
         */
        Builder() {
            this.limitsTimeDiscovery = new Limits<>(
                    org.hyrax.link.misc.Constants.DEFAULT_DISCOVERY_TIME,
                    org.hyrax.link.misc.Constants.DEFAULT_DISCOVERY_TIME);
            this.limitsTimeVisibility = new Limits<>(
                    org.hyrax.link.misc.Constants.DEFAULT_VISIBILITY_TIME,
                    org.hyrax.link.misc.Constants.DEFAULT_VISIBILITY_TIME);
            this.limitsConnections = new Limits<>(
                    Constants.DEFAULT_MIN_CONNECTIONS_DEVICE,
                    Constants.DEFAULT_MAX_CONNECTIONS_DEVICE);
            this.acceptProbability = (double) 1 / Constants.DEFAULT_MAX_CONNECTIONS_DEVICE;
            this.idleTime = Constants.DEFAULT_IDLE_TIME;
        }

        /**
         * Constructor.
         *
         * @param properties formation properties already set.
         */
        Builder(@NonNull FormationProperties properties) {
            this.limitsTimeDiscovery = properties.limitsTimeDiscovery;
            this.limitsTimeVisibility = properties.limitsTimeVisibility;
            this.limitsConnections = properties.limitsConnections;
            this.acceptProbability = properties.acceptProbability;
            this.idleTime = properties.idleTime;
            this.enableHardwareOnStart = properties.enableHardwareOnStart;
        }

        /**
         * Asserts if a min variable is not greater than max.
         *
         * @param min value min
         * @param max value max
         */
        private void assertLimits(int min, int max) {
            NetLog.assertEquals(max >= min, true,
                    String.format(Locale.ENGLISH,
                            "Var max: %d must be greater than Var min: %d", min, max));
        }

        /**
         * Changes the discovery time interval.
         *
         * @param min time min
         * @param max time max
         * @return the object builder
         */
        public Builder setDiscoveryTimeLimits(int min, int max) {
            assertLimits(min, max);
            this.limitsTimeDiscovery = new Limits<>(min, max);
            return this;
        }

        /**
         * Changes the visibility time interval.
         *
         * @param min time min
         * @param max time max
         * @return the object builder
         */
        public Builder setVisibilityTimeLimits(int min, int max) {
            assertLimits(min, max);
            this.limitsTimeVisibility = new Limits<>(min, max);
            return this;
        }

        /**
         * Changes the connections limits
         *
         * @param min minimum active connections
         * @param max maximum active connections
         * @return the object builder
         */
        public Builder setConnectionLimits(int min, int max) {
            assertLimits(min, max);
            this.limitsConnections = new Limits<>(min, max);
            return this;
        }

        /**
         * Changes the probability of accept connections.
         *
         * @param prob double probability
         * @return the object builder
         */
        public Builder setAcceptProbability(double prob) {
            NetLog.assertEquals(prob >= 0 && prob <= 1, true,
                    "Prob must be between [0,1]");
            this.acceptProbability = prob;
            return this;
        }

        /**
         * Changes the node idle time.
         * <p/>
         * This means the amount of time that a node can be doing nothing. Usually after this time
         * the device changes roles.
         *
         * @param time time in milliseconds
         * @return the object builder
         */
        public Builder setIdleTime(int time) {
            this.idleTime = time;
            return this;
        }

        /**
         * Changes a flag to the formation algorithm to enable the hardware when starts.
         *
         * @param enable <tt>true</tt> if to automatically enable the hardware on start,
         *               <tt>false</tt> otherwise
         * @return the device changes roles.
         */
        public Builder setEnableHardwareOnStart(boolean enable) {
            this.enableHardwareOnStart = enable;
            return this;
        }

        /**
         * Returns a {@link FormationProperties} object.
         *
         * @return {@link FormationProperties} object
         */
        @NonNull
        public FormationProperties build() {
            return new FormationProperties(this);
        }
    }
}





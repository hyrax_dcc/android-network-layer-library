/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.NonNull;

import org.hyrax.network.formation.FormationInit;

import java.util.Objects;

/**
 * Base interface that defined the network formation topology.
 * </p>
 * Use the class {@link org.hyrax.network.channel.ChannelRegistry} to create channels in order to
 * inform the routing algorithm of new connections.
 */
public interface Formation {

    /**
     * Enum class that represents the formation algorithm topology.
     */
    enum Type {
        /**
         * Star formation using Bluetooth
         */
        BLUETOOTH_STAR,

        /**
         * Bluetooth mesh random formation
         */
        BLUETOOTH_MESH_RANDOM,

        /**
         * Star formation using just Wifi Direct.
         */
        WIFI_DIRECT_STAR,

        /**
         * Star formation using Wifi Direct + Wifi
         */
        WIFI_DIRECT_LEGACY_STAR,

        /**
         * Star formation using Wifi Direct + Wifi.
         * The devices need to be connected to Wifi first.
         */
        WIFI_DIRECT_LEGACY_STAR_2,

        /**
         * Mesh random formation, using Sockets links.
         * It can be used on top of Wifi or Wifi Direct technologies.
         */
        WIFI_MESH_RANDOM,

        /**
         * Custom formation algorithm.
         */
        CUSTOM,
    }

    /**
     * Returns the network formation name.
     *
     * @return string name
     */
    @NonNull
    String getName();

    /**
     * Returns the network formation topology.
     *
     * @return enum formation type
     */
    @NonNull
    Type getType();

    /**
     * Changes the required initialization objects.
     * <p/>
     * Method called by the {@link Network} class.
     *
     * @param formationInit formation init object
     */
    void setFormationInit(@NonNull FormationInit formationInit);

    /**
     * Returns the default formation properties.
     *
     * @return formation properties object
     */
    @NonNull
    FormationProperties getDefaultFormationProperties();

    /**
     * Starts the network formation.
     */
    void start();

    /**
     * Resumes the network formation.
     */
    void resume();

    /**
     * Pauses the network formation.
     */
    void pause();

    /**
     * Stops the network formation.
     */
    void stop();

    /**
     * Checks if the network formation is running.
     *
     * @return <tt>true</tt> if running <tt>false</tt> otherwise
     */
    boolean isRunning();

    /**
     * Check if a formation algorithm is the same as the current.
     *
     * @param formation network formation to compare with
     * @return <tt>true</tt> if two formation objects are the same, <tt>false</tt> otherwise
     */
    default boolean formationEquals(@NonNull Formation formation) {
        return (this.getType() == formation.getType()) &&
                (this.getName().equals(formation.getName()));
    }

    /**
     * Returns an hash to the current formation object
     *
     * @return integer representing an hash
     */
    default int formationHash() {
        return Objects.hash(getType(), getName());
    }
}

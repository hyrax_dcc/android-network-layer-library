/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.routing;

import android.support.annotation.NonNull;

import org.hyrax.network.Address;
import org.hyrax.network.Routing;
import org.hyrax.network.misc.NetworkRuntimeException;
import org.hyrax.network.routing.packet.Packet;

import java.util.UUID;

/**
 * Helper class responsible for executing some generic routing actions.
 */
public class LibRouting {
    // packet containing the remove logic address
    public static final int LOGIC_ADDRESS = 0;
    public static final int NEIGHBOR_TABLE = 1;

    /**
     * Converts logic network address to protocol buffer address.
     *
     * @param address address to be converted
     * @return protocol buffer message address
     */
    public static Packet.Address addressToProtoAddress(@NonNull Address address) {
        UUID uuid = address.getLogicAddress();
        return Packet.Address.newBuilder()
                .setMostSigBits(uuid.getMostSignificantBits())
                .setLeastSigBits(uuid.getLeastSignificantBits())
                .build();
    }

    /**
     * Converts the protocol buffer address to logical network address.
     *
     * @param protoAddress protocol buffer message address
     * @return network logical address
     */
    public static Address protoAddressToAddress(@NonNull Packet.Address protoAddress) {
        return Address.AddressFromUUID(
                new UUID(protoAddress.getMostSigBits(), protoAddress.getLeastSigBits()));
    }

    /**
     * Constructs a routing algorithm from a type.
     *
     * @param type routing enum type
     * @return a routing object
     * @throws NetworkRuntimeException if type is not taken care
     */
    public static Routing factory(@NonNull Routing.Type type) {
        switch (type) {
            case FLOOD:
                return new Flood();
            case FLOOD_CONTROL:
                return new FloodControl();
            default:
                throw new NetworkRuntimeException("Undefined routing type " + type);
        }
    }
}

#!/bin/bash

OUTPUT=../../../../../

echo "Compiling hyrax message routing  ...."
protoc -I=./ --java_out=$OUTPUT packet.proto
if [ $? -ne 0 ]
then
    echo "Message request failure. Please check the syntax."
    exit 1;
fi

echo "Compilation successfully completed!!"

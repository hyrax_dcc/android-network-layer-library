/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.routing;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.protobuf.InvalidProtocolBufferException;

import org.hyrax.network.Address;
import org.hyrax.network.NetworkService;
import org.hyrax.network.Routing;
import org.hyrax.network.RoutingProperties;
import org.hyrax.network.misc.Channel;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkRegistry;
import org.hyrax.network.routing.packet.Packet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Flooding algorithm implementation.
 * Infinite message forward avoided using message TTL.
 * <p/>
 * Algorithm Unicast:
 * - If a peers is directly connected sent to it
 * - Otherwise floods to all neighbors, excepting the one which sent the message
 * Algorithm Broadcast:
 * - Send to all neighbors, excepting the one which sent the message
 * <p>
 * In both strategies the packets will be dropped when the number of hops reach zero.
 */
class Flood implements Routing {
    //private static final int MAX_QUEUE_SIZE = 7500;
    private static final String NAME = "flood";
    //private final ExecutorService receiverWorker;
    private final ThreadPoolExecutor worker;
    private final AtomicInteger packetId;
    private NetworkRegistry networkRegistry;
    private final RoutingTable routingTable;
    @Nullable
    private AddressInfo addressInfo;

    private int rejected;

    private class AddressInfo {
        private final Address local;
        private final Address flood;
        private final Packet.Address srcAddress;
        private final Packet.Address floodAddress;

        private AddressInfo() {
            this.local = NetworkService.getLocalAddress();
            this.flood = NetworkService.getFloodAddress();
            this.srcAddress = LibRouting.addressToProtoAddress(local);
            this.floodAddress = LibRouting.addressToProtoAddress(flood);
        }

        private boolean isLoopback(@NonNull Address adr) {
            return local.equals(adr);
        }

        private boolean isFlood(@NonNull Address adr) {
            return flood.equals(adr);
        }
    }

    /**
     * Constructor.
     */
    Flood() {
        this.rejected = 0;
        // bound queu
        LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
        //this.receiverWorker = Executors.newSingleThreadExecutor();
        this.worker = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, queue,
                (r, executor) -> {
                    try {
                        // WAIT_MAX, TimeUnit.MILLISECONDS
                        if (!executor.isShutdown())
                            executor.getQueue().put(r);
                        return;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    rejected++;
                    Log.e("TH", "Rejected .... " + rejected);
                });
        this.packetId = new AtomicInteger(0);
        this.routingTable = new RoutingTable();
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.FLOOD;
    }

    @Override
    public void setNetworkRegistry(@NonNull NetworkRegistry networkRegistry) {
        this.networkRegistry = networkRegistry;
    }

    @Override
    public void onPeerJoin(@NonNull Address address, @NonNull Channel channel) {
        routingTable.addPeer(address, channel);
        // logs new peer notification
        NetLog.logPeerNew(address.toString(), channel.getTechnology());
    }

    @Override
    public void onPeerLeave(@NonNull Address address, @NonNull Channel channel) {
        routingTable.removePeer(address, channel);
        // logs lost peer notification
        NetLog.logPeerLost(address.toString(), channel.getTechnology());
    }

    @Override
    public void onPeerPacket(@NonNull Address address, @NonNull Channel channel,
                             @NonNull Packet.Header header, @NonNull byte[] body) {
        Address srcAdr = LibRouting.protoAddressToAddress(header.getSource());
        int pktId = header.getId();
        int hops = header.getMaxHops();

        if (header.getTag() == LibRouting.NEIGHBOR_TABLE) {
            try {
                byte[] copyBody = new byte[header.getBodyLength()];
                System.arraycopy(body, 0, copyBody, 0, header.getBodyLength());
                routingTable.onNeighborTableRcv(srcAdr, Packet.Table.parseFrom(copyBody));
            } catch (InvalidProtocolBufferException e) {
                e.printStackTrace();
                NetLog.e("Invalid", e.getMessage());
            }
        } else {
            // log a received packet
            NetLog.logPacketRcv(srcAdr.toString(), pktId, address.toString(), channel.getTechnology().name());
            if (isValidPacket(srcAdr, pktId, hops)) {

                assert addressInfo != null;
                Address destAdr = LibRouting.protoAddressToAddress(header.getDestination());
                boolean isLoopback = addressInfo.isLoopback(destAdr);
                boolean isFlood = addressInfo.isFlood(destAdr);

                if (isLoopback || isFlood) {
                    // notify a received message
                    networkRegistry.notifyPeerPacket(body, header.getBodyLength(), srcAdr, header.getTag());
                }
                int peers = routingTable.getConnectedPeers().size();
                if (hops > 0 && peers > 1 && (!isLoopback || isFlood)) {
                    //decrement the message hops
                    Packet.Header newHeader = Packet.Header.newBuilder(header)
                            .setMaxHops(hops - 1).build();
                    routePacket(newHeader, body, srcAdr, destAdr, address);
                }
                NetLog.logPacketValid(srcAdr.toString(), pktId);

            } else {
                // logs invalid packet - dropped
                NetLog.logPacketDrop(srcAdr.toString(), pktId);
                /*NetLog.w(getName(), String.format(Locale.ENGLISH,
                        "Packet dropped from: %s with id: %d -> tag: %d",
                        srcAdr, header.getId(), header.getTag()));*/
            }
        }
    }

    /**
     * Verifies if the current packets is valid.
     *
     * @param srcAddress source packet address
     * @param pktId      packet identifier
     * @param hops       number of hops left
     * @return <tt>true</tt> if valid <tt>false</tt> otherwise
     */
    boolean isValidPacket(@NonNull Address srcAddress, int pktId, int hops) {
        return hops >= 0;
    }

    @Override
    public void send(@NonNull byte[] bytes, @NonNull Address destination,
                     @NonNull RoutingProperties routingProperties) {
        assert addressInfo != null;
        Packet.Header header = buildNewHeader(bytes.length,
                LibRouting.addressToProtoAddress(destination), routingProperties);

        routePacket(header, bytes, addressInfo.local, destination, addressInfo.local);
        // logs message send
        NetLog.logPacketSend(destination.toString(), header.getId(), header.getTag(), bytes.length);
    }

    @Override
    public void sendToAll(@NonNull byte[] bytes, @NonNull RoutingProperties routingProperties) {
        assert addressInfo != null;
        //assert addressInfo != null;
        Packet.Header header = buildNewHeader(bytes.length, addressInfo.floodAddress, routingProperties);

        routePacket(header, bytes, addressInfo.local, addressInfo.flood, addressInfo.local);
        // logs message before going to the queue
        NetLog.logPacketSend(addressInfo.flood.toString(), header.getId(), header.getTag(), bytes.length);
    }

    @Override
    public void start() {
        if (addressInfo == null)
            addressInfo = new AddressInfo();
    }

    /**
     * Builds and returns a packet header
     *
     * @param bodyLen     length of the packet body
     * @param destination packet destination address
     * @param properties  routing properties object
     * @return packet header object
     */
    @NonNull
    private Packet.Header buildNewHeader(int bodyLen, @NonNull Packet.Address destination,
                                         @NonNull RoutingProperties properties) {
        assert addressInfo != null;
        return Packet.Header.newBuilder()
                .setTag(properties.getTag())
                .setId(packetId.getAndIncrement())
                .setBodyLength(bodyLen)
                .setSource(addressInfo.srcAddress)
                .setDestination(destination)
                .setMaxHops(properties.getMaxNumberOfHops() - 1)
                .build();

    }

    @Override
    public void destroy() {
        worker.shutdownNow();
        routingTable.clean();
        //receiverWorker.shutdownNow();
    }

    /**
     * Route a packet to a destination.
     * If the destination id not directly reached then sends to everyone.
     *
     * @param header      packet header object
     * @param body        packet body
     * @param srcAddress  source address
     * @param destination destination address
     * @param rcvAddress  receive address
     */
    private void routePacket(@NonNull Packet.Header header, @NonNull byte[] body,
                             @NonNull Address srcAddress, @NonNull Address destination,
                             @NonNull Address rcvAddress) {
        byte[] copyBody = new byte[header.getBodyLength()];
        System.arraycopy(body, 0, copyBody, 0, header.getBodyLength());
        //Log.d("Size", "" + worker.getQueue().size());
        worker.submit(() -> {
            //Log.w("SEND Size", " " + ((ThreadPoolExecutor) senderWorker).getQueue().size());
            List<Channel> channels = routingTable.getChannels(destination);
            // if destination is directly connected - then send just to it
            if (!route(header, copyBody, srcAddress, destination, channels)) {
                Set<Address> peers = routingTable.getConnectedPeers();
                // if not - send to every one
                for (Address address : peers) {
                    if (!rcvAddress.equals(address))
                        route(header, copyBody, srcAddress, address, routingTable.getChannels(address));
                }
            }
        });
    }

    /**
     * Sends a packet to one of the channel list.
     *
     * @param header     packet header object
     * @param body       packet body
     * @param srcAddress source address
     * @param nextHop    next address to send
     * @param channels   list of channels
     * @return <tt>true</tt> if a packet was sent, <tt>false</tt> otherwise
     */
    private boolean route(@NonNull Packet.Header header, @NonNull byte[] body,
                          @NonNull Address srcAddress, @NonNull Address nextHop,
                          @NonNull List<Channel> channels) {
        for (Channel c : channels) {
            boolean success = c.writePacket(header, body);
            // log a datagram route
            NetLog.logDatagramRoute(srcAddress.toString(), header.getId(), 0,
                    nextHop.toString(), c.getTechnology().name(), success);
            if (success)
                return true;
            c.stop();
        }
        return false;
    }

    private class RoutingTable {
        //private final int ROUTES_TO_KEEP = 3;
        // TODO - ord channel by priority
        final Map<Address, List<Channel>> dirConnected;
        final Map<Address, Address> neighbors;

        private RoutingTable() {
            this.dirConnected = new HashMap<>();
            this.neighbors = new HashMap<>();
        }

        private synchronized Set<Address> getConnectedPeers() {
            return dirConnected.keySet();
        }

        private synchronized List<Channel> getChannels(@NonNull Address destination) {
            if (dirConnected.containsKey(destination)) {
                return dirConnected.get(destination);
            }
            Address nextHop = neighbors.get(destination);
            if (nextHop != null && dirConnected.containsKey(nextHop)) {
                return dirConnected.get(nextHop);
            }
            return new ArrayList<>();
        }

        private synchronized void addPeer(@NonNull Address address, @NonNull Channel channel) {
            dirConnected.put(address, new ArrayList<Channel>() {{
                add(channel);
            }});
            if (dirConnected.size() > 1)
                onNeighborTableSend();
        }

        private synchronized void removePeer(@NonNull Address address, @NonNull Channel channel) {
            dirConnected.remove(address);
        }

        private synchronized void onNeighborTableRcv(@NonNull Address from, @NonNull Packet.Table neighbors) {
            for (Packet.Address neigh : neighbors.getNeighborsList()) {
                this.neighbors.put(LibRouting.protoAddressToAddress(neigh), from);
            }
        }

        private void onNeighborTableSend() {
            Packet.Table.Builder builder = Packet.Table.newBuilder();
            for (Address a : dirConnected.keySet())
                builder.addNeighbors(LibRouting.addressToProtoAddress(a));
            assert addressInfo != null;
            byte[] body = builder.build().toByteArray();
            Packet.Header header = Packet.Header.newBuilder()
                    .setTag(LibRouting.NEIGHBOR_TABLE)
                    .setBodyLength(body.length)
                    .setSource(addressInfo.srcAddress)
                    .build();

            for (Map.Entry<Address, List<Channel>> entry : dirConnected.entrySet()) {
                for (Channel c : entry.getValue()) {
                    if (c.writePacket(header, body))
                        break;
                }
            }
        }

        private void clean() {
            this.dirConnected.clear();
            this.neighbors.clear();
        }
    }
}
/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network.routing;

import android.support.annotation.NonNull;
import android.util.SparseIntArray;

import org.hyrax.network.Address;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Controlled flooding algorithm implementation.
 * Infinite message forward avoided using message TTL and message ids cache.
 */
public class FloodControl extends Flood {
    private static final String NAME = "controlled_flood";
    private final HashMap<Address, Cache> cacheMessages;
    private static final Lock lock = new ReentrantLock();

    /**
     * Constructor.
     */
    FloodControl() {
        super();
        this.cacheMessages = new HashMap<>();
    }

    @NonNull
    @Override
    public String getName() {
        return NAME;
    }

    @NonNull
    @Override
    public Type getType() {
        return Type.FLOOD_CONTROL;
    }

    @Override
    public void destroy() {
        super.destroy();
        cacheMessages.clear();
    }

    @Override
    boolean isValidPacket(@NonNull Address srcAddress, int pktId, int hops) {
        if (hops < 0)
            return false;
        lock.lock();
        try {
            Cache cache = cacheMessages.get(srcAddress);
            if (cache == null) {
                cache = new Cache();
                cacheMessages.put(srcAddress, cache);
            }
            return cache.markNewMessage(pktId);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Cache class that saves the received messages ids.
     */
    private static class Cache {
        private static final long CACHE_TIMEOUT = 2000; // 2 seconds
        // collects the garbage every 1000 messages
        private static final int GARBAGE_COLLECT_COUNT = 1000;
        private final SparseIntArray msgCache;
        private int msgCount;

        /**
         * Constructor.
         */
        private Cache() {
            this.msgCache = new SparseIntArray();
            this.msgCount = 0;
        }

        /**
         * Marks a new received message.
         *
         * @param msgId message identifies
         * @return <tt>true</tt> if it's a new message or <tt>false</tt> if it's a replicated message
         */
        private boolean markNewMessage(int msgId) {
            msgCount++;
            if (msgCache.get(msgId) == 0) {
                msgCache.put(msgId, 1);
                return true;
            }
            return false;
            //Long ts = msgCache.get(msgId);
            //long nowTs = System.currentTimeMillis();
            // if ts is null or the timeout has expired - then is a new message
            //if (ts == 0 || (nowTs - ts) > CACHE_TIMEOUT) {
                /*If has received a certain amount of messages - then clean old cache values.
                The idea is to keep the low memory usage.
                */
            //if (msgCount % GARBAGE_COLLECT_COUNT == 0)
            //    collectGarbage(nowTs);
            //   msgCache.put(msgId, nowTs);
            //    return true;
            //}
            //return false;
        }

        /**
         * Cleans old cache entries
         *
         * @param currTs the current timestamp
         */
        private void collectGarbage(long currTs) {
            msgCount = 0;
            for (int i = 0; i < msgCache.size(); i++) {
                if (currTs - msgCache.valueAt(i) > CACHE_TIMEOUT)
                    msgCache.removeAt(i);
            }
        }
    }
}

/*
 * Hyrax Middleware: a middleware for mobile edge clouds.
 *
 * Copyright (C) 2019 INESC TEC.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * A commercial license is also available for use in industrial projects and collaborations that do not wish to use the GPL 3 license.
 */

package org.hyrax.network;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.Log;

import org.hyrax.network.channel.ChannelRegistry;
import org.hyrax.network.formation.FormationInit;
import org.hyrax.network.formation.LibFormation;
import org.hyrax.network.misc.Constants;
import org.hyrax.network.misc.NetLog;
import org.hyrax.network.misc.NetworkListener;
import org.hyrax.network.misc.NetworkRegistry;
import org.hyrax.network.misc.NetworkRuntimeException;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Singleton class that represents the network communication interface (API).
 */
public final class Network {
    private static final Lock NETWORK_LOCK = new ReentrantLock();
    private static final String TAG = "Network";
    private final NetworkRegistry networkRegistry;
    private final Routing routing;
    private final ChannelRegistry channelRegistry;
    private final ScheduledExecutorService executorService;
    public final Set<Formation> topologies;
    private Address localAddress;

    /**
     * Constructor.
     *
     * @param routing the routing algorithm
     */
    protected Network(@NonNull Routing routing) {
        this.networkRegistry = new NetworkRegistry();
        this.routing = routing;
        routing.setNetworkRegistry(networkRegistry);
        //boots channel registry
        this.channelRegistry = ChannelRegistry.boot(networkRegistry, routing);
        this.executorService = Executors.newSingleThreadScheduledExecutor();
        this.topologies = new HashSet<>();
    }

    /**
     * Starts the network
     */
    protected void start() {
        localAddress = NetworkService.getLocalAddress();
        NetLog.i("Local Address", localAddress.toString());
        NetLog.i("Flood Address", NetworkService.getFloodAddress().toString());

        routing.start();
        // logs online notification
        NetLog.logPeerOnline(localAddress.toString(), routing.getName());
    }

    /**
     * Stops the network
     */
    protected void stop() {
        //stop all active network interfaces
        Log.e("Stop", "net1");
        NETWORK_LOCK.lock();
        try {
            Log.e("Stop", "net2");
            // logs offline notification
            NetLog.logPeerOffline(localAddress.toString(), routing.getName());

            Log.e("Stop", "net3");
            for (Formation f : topologies)
                f.stop();
            Log.e("Stop", "net4");
            topologies.clear();
            Log.e("Stop", "net5");
            //cleans the network registry
            networkRegistry.destroy();
            Log.e("Stop", "net6");
            //destroy the channel service
            channelRegistry.shutdown();
            Log.e("Stop", "net7");
            //destroy and cleans the routing
            routing.destroy();
            Log.e("Stop", "net8");
            //stops executor service
            executorService.shutdownNow();
            Log.e("Stop", "net9");
        } finally {
            NETWORK_LOCK.unlock();
        }
    }

    /**
     * Enables a network topology algorithm with default formation properties.
     *
     * @param type formation type
     */
    public void enableFormation(@NonNull Formation.Type type) {
        Formation f = LibFormation.factory(type);
        enableFormation(f, f.getDefaultFormationProperties());
    }

    /**
     * Enables a network topology algorithm.
     *
     * @param type                topology type
     * @param formationProperties formation properties object
     */
    public void enableFormation(@NonNull Formation.Type type,
                                @NonNull FormationProperties formationProperties) {
        enableFormation(LibFormation.factory(type), formationProperties);
    }

    /**
     * Enables a network topology algorithm.
     *
     * @param formation           formation algorithm object
     * @param formationProperties formation properties object
     */
    public void enableFormation(@NonNull Formation formation,
                                @NonNull FormationProperties formationProperties) {
        NETWORK_LOCK.lock();
        try {
            if (topologies.add(formation)) {
                formation.setFormationInit(new FormationInit(
                        formationProperties, channelRegistry, executorService
                ));
                formation.start();
                NetLog.i(TAG, String.format(Locale.ENGLISH, "Network topology: %s enabled -> %s",
                        formation.getType(), formation.getName()));
                return;
            }
            throw new NetworkRuntimeException(String.format(Locale.ENGLISH,
                    "Formation already active %s -> %s",
                    formation.getName(), formation.getType()));
        } finally {
            NETWORK_LOCK.unlock();
        }
    }

    /**
     * Disables a network topology algorithm.
     *
     * @param type network topology type
     */
    public void disableFormation(@NonNull Formation.Type type) {
        NETWORK_LOCK.lock();
        try {
            Formation f = null;
            for (Formation formation : topologies) {
                if (type == formation.getType()) {
                    f = formation;
                    break;
                }
            }
            if (f != null) {
                disableFormation(f);
            } else {
                NetLog.w(TAG, "Formation type not active " + type);
            }
        } finally {
            NETWORK_LOCK.unlock();
        }
    }

    /**
     * Disables a network topology
     *
     * @param formation formation algorithm object
     */
    public void disableFormation(@NonNull Formation formation) {
        NETWORK_LOCK.lock();
        try {
            if (topologies.remove(formation)) {
                formation.stop();
                NetLog.w(TAG, String.format(Locale.ENGLISH,
                        "Network topology: %s disabled -> %s",
                        formation.getType(), formation.getName()));
            } else {
                NetLog.w(TAG, "Formation not active " + formation.getName());
            }
        } finally {
            NETWORK_LOCK.unlock();
        }
    }

    /**
     * Verifies if a particular network interface is active.
     *
     * @param type network topology name
     * @return <tt>true</tt> if active, <tt>false</tt> otherwise
     */
    public boolean isTopologyEnabled(Formation.Type type) {
        NETWORK_LOCK.lock();
        boolean val = false;
        try {
            for (Formation formation : topologies) {
                if (type == formation.getType()) {
                    val = true;
                    break;
                }
            }
        } finally {
            NETWORK_LOCK.unlock();
        }
        return val;
    }

    /**
     * Verifies if the network is online.
     * <br/>
     * This issues the network in order to find out if there are any network interface
     * already online. By online we mean that a network must be connected somewhere.
     *
     * @return <tt>true</tt> if online, <tt>false</tt> otherwise
     */
    public boolean isOnline() {
        NETWORK_LOCK.lock();
        boolean val = false;
        try {
            for (Formation f : topologies) {
                if (f.isRunning()) {
                    val = true;
                    break;
                }
            }
        } finally {
            NETWORK_LOCK.unlock();
        }
        return val;
    }

    /**
     * Sends an array of message bytes to a destination.
     *
     * @param destination destination logical address
     * @param bytes       message bytes
     * @param tag         message tag
     */
    public void send(@NonNull byte[] bytes, @NonNull Address destination,
                     @IntRange(from = Constants.ROUTING_TAG_MIN) int tag) {
        send(bytes, destination, RoutingProperties.newBuilder(tag).build());
    }

    /**
     * Send an array of message bytes to a destination with specific send properties.
     *
     * @param bytes             message bytes
     * @param destination       destination logical address
     * @param routingProperties send properties object definition
     */
    public void send(@NonNull byte[] bytes, @NonNull Address destination,
                     @NonNull RoutingProperties routingProperties) {
        routing.send(bytes, destination, routingProperties);
    }

    /**
     * Sends an array of message bytes to unspecific destination, following default send properties rules.
     * <br/>
     * Every node in the way will receive the message sent.
     *
     * @param bytes message bytes
     * @param tag   message tag
     */
    public void sendToAll(@NonNull byte[] bytes,
                          @IntRange(from = Constants.ROUTING_TAG_MIN) int tag) {
        sendToAll(bytes, RoutingProperties.newBuilder(tag).build());
    }

    /**
     * Sends an array of message bytes to unspecific destination, following defined send properties
     *
     * @param bytes             message bytes
     * @param routingProperties send properties object definition
     */
    public void sendToAll(@NonNull byte[] bytes, @NonNull RoutingProperties routingProperties) {
        routing.sendToAll(bytes, routingProperties);
    }

    /**
     * Adds a network listener instance.
     *
     * @param networkListener listener object
     */
    public void addNetworkListener(@NonNull NetworkPeerListener networkListener) {
        networkRegistry.addPeerListener(networkListener);
    }

    /**
     * Listens all routing messages produced by the network, excepting the internal control messages.
     *
     * @param networkPacketListener network packet listener object
     */
    public void addPacketListener(@NonNull NetworkPacketListener networkPacketListener) {
        networkRegistry.addPacketListener(Constants.ROUTING_ALL_TAG, networkPacketListener);
    }

    /**
     * Listen the routing messages filtered by a tag.
     *
     * @param tag                   message tag
     * @param networkPacketListener network packet listener object
     */
    public void addPacketListener(@IntRange(from = Constants.ROUTING_TAG_MIN) int tag,
                                  @NonNull NetworkPacketListener networkPacketListener) {
        networkRegistry.addPacketListener(tag, networkPacketListener);
    }

    /**
     * Removes a network events listeners.
     * <br/>
     * All network event listener implements the interface {@link NetworkListener}
     *
     * @param networkEventListener network event listener object
     */
    public void removeNetworkEventListener(@NonNull NetworkListener networkEventListener) {
        networkRegistry.removeNetworkListener(networkEventListener);
    }
}
